package by.reghor.task5.logic;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import by.reghor.task5.entity.ColdWeapon;

public class DomParser {
	
    private static final DomParser instance=new DomParser();
    
    private static Logger logger = Logger.getLogger(DomParser.class);
    
    private DomParser(){
    	
    }
	
    
    public static DomParser getInstance() {
    	return instance;
    }
    
    public List<ColdWeapon> parse(File xmlFile) throws ParserException{
    	Document document = null;
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory
				.newInstance();
		try {
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			document = dBuilder.parse(xmlFile);
		} catch (IOException | ParserConfigurationException | SAXException exception) {
			logger.error(exception);
			throw new ParserException(exception);
		}
		List<ColdWeapon> coldWeaponList=new ArrayList<>();
		document.getDocumentElement().normalize();

		NodeList nList = document.getElementsByTagName("knife");

		for (int temp = 0; temp < nList.getLength(); temp++) {

			Node nNode = nList.item(temp);



			if (nNode.getNodeType() == Node.ELEMENT_NODE) {

				Element eElement = (Element) nNode;
				ColdWeapon currentWeapon=new ColdWeapon();
				currentWeapon.setId(eElement.getAttribute("ID"));
				currentWeapon.setType(eElement.getElementsByTagName("Type").item(0).getTextContent());
				currentWeapon.setHandy(eElement.getElementsByTagName("Handy").item(0).getTextContent());
				currentWeapon.setOrigin(eElement.getElementsByTagName("Origin").item(0).getTextContent());
				if (eElement.getElementsByTagName("bladeLength").item(0)!=null){
					currentWeapon.setBladeLength(eElement.getElementsByTagName("bladeLength").item(0).getTextContent());}
				if (eElement.getElementsByTagName("bladeWidth").item(0)!=null){	currentWeapon.setBladeWidth(eElement.getElementsByTagName("bladeWidth").item(0).getTextContent());}
				if (eElement.getElementsByTagName("bladeMaterial").item(0)!=null){	currentWeapon.setBladeMaterial(eElement.getElementsByTagName("bladeMaterial").item(0).getTextContent());}
				Node handleMaterial = eElement.getElementsByTagName("handleMaterial").item(0);
				if (handleMaterial!=null){
					NodeList handleChildNodes = handleMaterial.getChildNodes();
					for (int j = 0; j < handleChildNodes.getLength(); j++) {
						if (handleChildNodes.item(j).getNodeType() == Node.ELEMENT_NODE) {
							Element e = (Element) (handleChildNodes.item(j));
							currentWeapon.setHandleMaterial(e.getTextContent());
						}
					}
				}
				if (eElement.getElementsByTagName("cannelure").item(0)!=null){ currentWeapon.setCannelure(eElement.getElementsByTagName("cannelure").item(0).getTextContent());}
				if (eElement.getElementsByTagName("Value").item(0)!=null){ currentWeapon.setValue(eElement.getElementsByTagName("Value").item(0).getTextContent());}

				coldWeaponList.add(currentWeapon);
			}
		}
		return coldWeaponList;
    }

}
