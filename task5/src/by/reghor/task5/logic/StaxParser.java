package by.reghor.task5.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.apache.log4j.Logger;

import by.reghor.task5.entity.ColdWeapon;

public class StaxParser {

	private static final StaxParser instance=new StaxParser();
	
	private static Logger logger = Logger.getLogger(StaxParser.class);

	static final String KNIFE = "knife";
	static final String ID = "ID";
	static final String TYPE = "Type";
	static final String HANDY = "Handy";
	static final String ORIGIN = "Origin";
	static final String BLADE_LENGTH = "bladeLength";
	static final String BLADE_WIDTH = "bladeWidth";
	static final String BLADE_MATERIAL = "bladeMaterial";
	static final String WOODEN = "wooden";
	static final String NOT_WOODEN = "notWooden";
	static final String CANNELURE = "cannelure";
	static final String VALUE = "Value";

	private StaxParser(){

	}

	public static StaxParser getInstance() {
		return instance;
	}
	@SuppressWarnings("unchecked")
	public List<ColdWeapon> parse(File xmlFile) throws ParserException{
		List<ColdWeapon> items = new ArrayList<>();
		try {
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			InputStream in = new FileInputStream(xmlFile);
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
			ColdWeapon item = null;

			while (eventReader.hasNext()) {
				XMLEvent event = eventReader.nextEvent();

				if (event.isStartElement()) {
					StartElement startElement = event.asStartElement();
					if (startElement.getName().getLocalPart().equals(KNIFE)) {
						item = new ColdWeapon();
						Iterator<Attribute> attributes = startElement
								.getAttributes();
						while (attributes.hasNext()) {
							Attribute attribute = attributes.next();
							if (attribute.getName().toString().equals(ID)) {
								item.setId(attribute.getValue());
							}

						}
					}

					if (event.isStartElement()) {
						if (event.asStartElement().getName().getLocalPart()
								.equals(TYPE)) {
							event = eventReader.nextEvent();
							item.setType(event.asCharacters().getData());
							continue;
						}
					}
					if (event.asStartElement().getName().getLocalPart()
							.equals(HANDY)) {
						event = eventReader.nextEvent();
						item.setHandy(event.asCharacters().getData());
						continue;
					}

					if (event.asStartElement().getName().getLocalPart()
							.equals(ORIGIN)) {
						event = eventReader.nextEvent();
						item.setOrigin(event.asCharacters().getData());
						continue;
					}

					if (event.asStartElement().getName().getLocalPart()
							.equals(BLADE_LENGTH)) {
						event = eventReader.nextEvent();
						item.setBladeLength(event.asCharacters().getData());
						continue;

					}
					if (event.asStartElement().getName().getLocalPart()
							.equals(BLADE_WIDTH)) {
						event = eventReader.nextEvent();
						item.setBladeWidth(event.asCharacters().getData());
						continue;

					}
					if (event.asStartElement().getName().getLocalPart()
							.equals(BLADE_MATERIAL)) {
						event = eventReader.nextEvent();
						item.setBladeMaterial(event.asCharacters().getData());
						continue;

					}
					if (event.asStartElement().getName().getLocalPart()
							.equals(WOODEN)||event.asStartElement().getName().getLocalPart()
							.equals(NOT_WOODEN)) {
						event = eventReader.nextEvent();
						item.setHandleMaterial(event.asCharacters().getData());
						continue;

					}
					if (event.asStartElement().getName().getLocalPart()
							.equals(CANNELURE)) {
						event = eventReader.nextEvent();
						item.setCannelure(event.asCharacters().getData());
						continue;

					}
					if (event.asStartElement().getName().getLocalPart()
							.equals(VALUE)) {
						event = eventReader.nextEvent();
						item.setValue(event.asCharacters().getData());
						continue;

					}
				}

				if (event.isEndElement()) {
					EndElement endElement = event.asEndElement();
					if (endElement.getName().getLocalPart().equals(KNIFE)) {
						items.add(item);

					}

				}
			}
		} catch (FileNotFoundException|XMLStreamException e) {
			logger.error(e);
			throw new ParserException(e);
		}
		return items;
	}
}


