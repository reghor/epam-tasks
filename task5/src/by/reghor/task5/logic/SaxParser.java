package by.reghor.task5.logic;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import by.reghor.task5.entity.ColdWeapon;

public class SaxParser extends DefaultHandler {

	private static volatile SaxParser instance=new SaxParser();

	List<ColdWeapon> weaponList;
	File xmlFile;
	String tmpValue;
	ColdWeapon weaponTmp;


	private SaxParser (){

	}

	public static SaxParser getInstance() {
		return instance;
	}

	public  List<ColdWeapon>   parse(File xmlFile) throws ParserException{
		this.xmlFile = xmlFile;
		weaponList = new ArrayList<ColdWeapon>();


		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser;
		
		try {
			saxParser = factory.newSAXParser();
			saxParser.parse(xmlFile, this);
		} catch (ParserConfigurationException | SAXException|IOException e) {
			throw new ParserException(e);
		}
		
		return weaponList;

	}


	@Override
	public void startElement(String s, String s1, String elementName, Attributes attributes) throws SAXException {

		if (elementName.equalsIgnoreCase("knife")) {
			weaponTmp = new ColdWeapon();
			weaponTmp.setId(attributes.getValue("ID"));
		}
	}
	@Override
	public void endElement(String s, String s1, String element) throws SAXException {
		if (element.equals("knife")) {
			weaponList.add(weaponTmp);
		}else if (element.equalsIgnoreCase("Type")) {
			weaponTmp.setType(tmpValue);}
		else if (element.equalsIgnoreCase("Handy")) {
			weaponTmp.setHandy(tmpValue);
		}else if (element.equalsIgnoreCase("Origin")) {
			weaponTmp.setOrigin(tmpValue);
		}else if (element.equalsIgnoreCase("bladeLength")) {
			weaponTmp.setBladeLength(tmpValue);
		}else if (element.equalsIgnoreCase("bladeWidth")) {
			weaponTmp.setBladeWidth(tmpValue);
		}else if (element.equalsIgnoreCase("bladeMaterial")) {
			weaponTmp.setBladeMaterial(tmpValue);
		}else if (element.equalsIgnoreCase("wooden")) {
			weaponTmp.setHandleMaterial(tmpValue);
		}else if (element.equalsIgnoreCase("notWooden")) {
			weaponTmp.setHandleMaterial(tmpValue);
		}else if (element.equalsIgnoreCase("cannelure")) {
			weaponTmp.setCannelure(tmpValue);
		}else if (element.equalsIgnoreCase("Value")) {
			weaponTmp.setValue(tmpValue);
		}

	}
	@Override
	public void characters(char[] ac, int i, int j) throws SAXException {
		tmpValue = new String(ac, i, j);
	}
}
