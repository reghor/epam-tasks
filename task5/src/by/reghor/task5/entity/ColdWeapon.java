package by.reghor.task5.entity;

import java.io.Serializable;

public class ColdWeapon implements Serializable {

	private static final long serialVersionUID = 1L;
	
		private String id;
		private String type;
		private String handy;
		private  String bladeLength;
		private  String origin;
		private  String bladeWidth;
		private String bladeMaterial;
		private String handleMaterial;
		private String cannelure;
		private String value;
		
		
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getHandy() {
			return handy;
		}
		public void setHandy(String handy) {
			this.handy = handy;
		}
		public String getBladeLength() {
			return bladeLength;
		}
		public void setBladeLength(String bladeLength) {
			this.bladeLength = bladeLength;
		}
		public String getOrigin() {
			return origin;
		}
		public void setOrigin(String origin) {
			this.origin = origin;
		}
		public String getBladeWidth() {
			return bladeWidth;
		}
		public void setBladeWidth(String bladeWidth) {
			this.bladeWidth = bladeWidth;
		}
		public String getBladeMaterial() {
			return bladeMaterial;
		}
		public void setBladeMaterial(String bladeMaterial) {
			this.bladeMaterial = bladeMaterial;
		}
		public String getHandleMaterial() {
			return handleMaterial;
		}
		public void setHandleMaterial(String handleMaterial) {
			this.handleMaterial = handleMaterial;
		}
		
		
		public String getCannelure() {
			return cannelure;
		}
		public void setCannelure(String cannelure) {
			this.cannelure = cannelure;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
		public static long getSerialversionuid() {
			return serialVersionUID;
		}
		
	

}
