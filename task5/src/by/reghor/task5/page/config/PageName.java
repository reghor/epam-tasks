package by.reghor.task5.page.config;

public final  class PageName {

	private PageName() {
	}
	public static final String MAIN_PAGE = "main";
	public static final String OUTPUT_PAGE = "parsed.xml";
	
}