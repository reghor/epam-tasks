package by.reghor.task5.page.config;

import java.util.ResourceBundle;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class PageManager {
	private static Lock lock = new ReentrantLock();
    private static volatile PageManager instance;
    private ResourceBundle bundle = ResourceBundle.getBundle("page_name");
    
    private PageManager(){
    	
    }

    
    public static PageManager getInstance() {
        if (instance==null){
        	lock.lock();
        	if (instance==null){
        		instance=new PageManager();
        	}
        	lock.unlock();
        }
    	return instance;
    }

    public String getValue(String key) {
        return bundle.getString(key);
    }
}
