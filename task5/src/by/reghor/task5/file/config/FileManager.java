package by.reghor.task5.file.config;

import java.util.ResourceBundle;

public class FileManager {
    private static final FileManager instance=new FileManager();
    private ResourceBundle bundle = ResourceBundle.getBundle("file_name");
    
    private FileManager(){
    	
    }

    
    public static FileManager getInstance() {
       
    	return instance;
    }

    public String getValue(String key) {
        return bundle.getString(key);
    }
}
