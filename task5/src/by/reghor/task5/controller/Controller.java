package by.reghor.task5.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import by.reghor.task5.command.CommandException;
import by.reghor.task5.command.CommandRetriever;
import by.reghor.task5.command.ICommand;
import by.reghor.task5.page.config.PageManager;
import by.reghor.task5.page.config.PageName;

public class Controller extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final String COMMAND_NAME="command";
	private static Logger logger = Logger.getLogger(Controller.class);
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);


	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

		String commandName= request.getParameter(COMMAND_NAME);
		ICommand command=null;
		command=CommandRetriever.getInstance().getCommand(commandName);
		String pageName=null;
		try{
			pageName=command.execute(request);
			

		} catch(CommandException e){
			logger.error(e);
			pageName=PageManager.getInstance().getValue(PageName.MAIN_PAGE);
			response.sendRedirect(request.getContextPath() + pageName);

		}
		
		RequestDispatcher requestDispatcher=request.getRequestDispatcher(pageName);
		if (requestDispatcher!=null){
			
			requestDispatcher.forward(request, response);
			
		}

	}

}
