package by.reghor.task5.command.impl;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.reghor.task5.command.CommandException;
import by.reghor.task5.command.ICommand;
import by.reghor.task5.entity.ColdWeapon;
import by.reghor.task5.file.config.FileManager;
import by.reghor.task5.file.config.FileName;
import by.reghor.task5.logic.DomParser;
import by.reghor.task5.logic.ParserException;
import by.reghor.task5.logic.SaxParser;
import by.reghor.task5.logic.StaxParser;
import by.reghor.task5.page.config.PageManager;
import by.reghor.task5.page.config.PageName;

public class Parse implements ICommand {
	private static final Logger logger = Logger.getLogger(Parse.class);


	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		String parserName = (String)request.getParameter("parser_type");
		if (parserName != null) {
			List<ColdWeapon> coldWeaponList=null;
			String xmlFileName = FileManager.getInstance().getValue(
					FileName.XML_INPUT);
			File xmlFile = new File(getClass().getClassLoader().getResource(xmlFileName).getFile());
			
			if(parserName.equalsIgnoreCase(ParserType.DOM.toString())) {
				try {
					coldWeaponList=DomParser.getInstance().parse(xmlFile);
				} catch (ParserException e) {
					logger.error(e);
					throw new CommandException(e);
				}
				request.setAttribute("weapons_list", coldWeaponList);
				return PageManager.getInstance().getValue(PageName.OUTPUT_PAGE);
			} else if(parserName.equalsIgnoreCase(ParserType.SAX.toString())){
				try{
					coldWeaponList=SaxParser.getInstance().parse(xmlFile);
					request.setAttribute("weapons_list", coldWeaponList);
				} catch (ParserException e) {
					logger.error(e);
					throw new CommandException(e);
				}
				return PageManager.getInstance().getValue(PageName.OUTPUT_PAGE);
			}else if(parserName.equalsIgnoreCase(ParserType.STAX.toString())){
				
				try{
					coldWeaponList=StaxParser.getInstance().parse(xmlFile);
					request.setAttribute("weapons_list", coldWeaponList);
				} catch (ParserException e) {
					logger.error(e);
					throw new CommandException(e);
				}
				return PageManager.getInstance().getValue(PageName.OUTPUT_PAGE);
			} else{
				return PageManager.getInstance().getValue(PageName.MAIN_PAGE);
			}
		}
		return  PageManager.getInstance().getValue(PageName.MAIN_PAGE);


	}
}
