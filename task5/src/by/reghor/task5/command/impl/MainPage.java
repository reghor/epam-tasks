package by.reghor.task5.command.impl;

import javax.servlet.http.HttpServletRequest;

import by.reghor.task5.command.CommandException;
import by.reghor.task5.command.ICommand;
import by.reghor.task5.page.config.PageManager;
import by.reghor.task5.page.config.PageName;

public class MainPage implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		return PageManager.getInstance().getValue(PageName.MAIN_PAGE);
	}

}
