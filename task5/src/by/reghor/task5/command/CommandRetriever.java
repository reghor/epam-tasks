package by.reghor.task5.command;

import java.util.HashMap;
import java.util.Map;

import by.reghor.task5.command.impl.Empty;
import by.reghor.task5.command.impl.MainPage;
import by.reghor.task5.command.impl.Parse;

public class CommandRetriever {
	private static final CommandRetriever instance = new CommandRetriever();

	private Map<CommandName, ICommand> commands = new HashMap<>();

	private CommandRetriever() {
		commands.put(CommandName.PARSE, new Parse());
		commands.put(CommandName.MAIN_PAGE, new MainPage());
		commands.put(CommandName.EMPTY_COMMAND, new Empty());
	}

	public static CommandRetriever getInstance() {
		return instance;
	}

	public ICommand getCommand(String commandName) {
		CommandName name = null;
		ICommand command = null;
		if (commandName != null) {
			name = CommandName.valueOf(commandName.toUpperCase());
			command = commands.get(name);

		} else {
			command = commands.get(CommandName.EMPTY_COMMAND);
		}
		return command;

	}

}
