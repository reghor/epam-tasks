package by.reghor.task5.command;

public enum CommandName {
	PARSE, EMPTY_COMMAND, MAIN_PAGE;
}
