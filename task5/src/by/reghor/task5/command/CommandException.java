package by.reghor.task5.command;

import by.reghor.task5.exception.CommonException;

public class CommandException extends CommonException {

	private static final long serialVersionUID = 1L;

	public CommandException() {
		
    }

    public CommandException(String message) {
        super(message);
    }

    public CommandException(String message, Throwable exception) {
        super(message, exception);
    }

    public CommandException(Throwable exception) {
        super(exception);
    }
}
