<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>List of cold weapons</title>
</head>
<body>
<form action="controller" method="post">
<input type="hidden" name="command" value="main_page">
<input type="submit" value="на главную">
</form>
	<h1>Table of cold weapons</h1>
	<table border="1">
		<tr>
			<th>ID</th>
			<th>Type</th>
			<th>Handy</th>
			<th>Orgigin</th>
			<th>Blade length</th>
			<th>Blade width</th>
			<th>Blade material</th>
			<th>Handle material</th>
			<th>Cancellure</th>
			<th>Value</th>
		</tr>
		<c:forEach items="${weapons_list }" var="weapon">
		<tr>
			<th>${weapon.id}</th>
			<th>${weapon.type}</th>
			<th>${weapon.handy}</th>
			<th>${weapon.origin}</th>
			<th>${weapon.bladeLength}</th>
			<th>${weapon.bladeWidth}</th>
			<th>${weapon.bladeMaterial}</th>
			<th>${weapon.handleMaterial}</th>
			<th>${weapon.cannelure}</th>
			<th>${weapon.value}</th>
		</tr>
		</c:forEach>
	</table>
</body>
</html>