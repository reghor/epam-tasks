package by.epam.task1.logic;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;

import by.epam.task1.entity.LocalInternetPlan;
import by.epam.task1.entity.LocalPlan;
import by.epam.task1.entity.LogicException;
import by.epam.task1.entity.RoamingPlan;
import by.epam.task1.entity.ServicePlan;

//Factory class for creating different service plans with different parameters

public class ServicePlanFactory {

	private static final org.apache.log4j.Logger logger = LogManager.getLogger(ServicePlan.class);
	static {
		logger.setLevel(Level.INFO);
	}

	public LocalInternetPlan createLocalInternetPlan(int subscribersCount, float monthlyFee, float smsCharge, String planName,
			float incomingCallCharge, float outgoingCallCharge, float internetSpeed, float internetTraffic) {
		try {
			if (monthlyFee < 0 || monthlyFee > 1000) {
				throw new LogicException("Wrong monthly Fee");
			}
		} catch (LogicException le) {
			logger.error("Wrong input for constructor", le);
			return null;
		}
		return new LocalInternetPlan(subscribersCount, monthlyFee, smsCharge, planName, incomingCallCharge, outgoingCallCharge, internetSpeed,
				internetTraffic);
	}

	public LocalPlan createLocalPlan(int subscribersCount, float monthlyFee, float smsCharge, String planName, float incomingCallCharge,
			float outgoingCallCharge) {
		try {
			if (monthlyFee < 0 || monthlyFee > 1000) {
				throw new LogicException("Wrong monthly Fee");
			}
		} catch (LogicException le) {
			logger.error("Wrong input for constructor", le);
			return null;
		}
		return new LocalPlan(subscribersCount, monthlyFee, smsCharge, planName, incomingCallCharge, outgoingCallCharge);
	}

	public RoamingPlan createRoamingPlan(int subscribersCount, float monthlyFee, float smsCharge, String planName, float internetSpeed,
			float internetTraffic, float outgoingCallCharge, float incomingCallCharge) {
		try {
			if (monthlyFee < 0 || monthlyFee > 1000) {
				throw new LogicException("Wrong monthly Fee");
			}
		} catch (LogicException le) {
			logger.error("Wrong input for constructor", le);
			return null;
		}
		return new RoamingPlan(subscribersCount, monthlyFee, smsCharge, planName, internetSpeed, internetTraffic, outgoingCallCharge,
				incomingCallCharge);
	}

}
