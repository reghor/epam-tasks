package by.epam.task1.logic;

import java.util.List;
import by.epam.task1.entity.LocalInternetPlan;
import by.epam.task1.entity.LocalPlan;
import by.epam.task1.entity.RoamingPlan;
import by.epam.task1.entity.ServicePlan;

//Class for printing the whole info about service plan line.

public class ContainerInfoPrinter {
	private ServicePlanContatiner container;

	public ContainerInfoPrinter(ServicePlanContatiner container) {
		this.container = container;
	}

	public void findSuitingLocalInternetPlansByInternetTraffic(float min, float max) {
		List<LocalInternetPlan> tempList = container.findSuitingLocalInternetPlansByInternetTraffic(min, max);
		if (tempList.isEmpty()) {
			System.out.print("No suitable plans");
			return;
		}
		System.out.printf("Here are your suitable plans where internet traffic is min %1.2f and max %2.2f:\n", min, max);
		for (LocalPlan plan : tempList) {
			System.out.print(plan);
		}
	}

	public void findSuitingLocalPlansByLocalCallCharge(float min, float max) {
		List<LocalPlan> tempList = container.findSuitingLocalPlansByLocalCallCharge(min, max);
		if (tempList.isEmpty()) {
			System.out.print("No suitable plans");
			return;
		}
		System.out.printf("Here are your suitable plans where local charge is min %1f and max %2f:\n", min, max);
		for (LocalPlan plan : tempList) {
			System.out.print(plan);
		}
	}

	public void findSuitingRoamingPlanPlansByOutgoingCallCharge(float min, float max) {
		List<RoamingPlan> tempList = container.findSuitingRoamingPlanPlansByOutgoingCallCharge(min, max);
		if (tempList.isEmpty()) {
			System.out.print("No suitable plans");
			return;
		}
		System.out.printf("Here are your suitable plans where outgoing call charge is min %1f and max %2f:\n", min, max);
		for (RoamingPlan plan : tempList) {
			System.out.print(plan);
		}
	}

	public void getServicePlans() {
		System.out.println("We have such service Plans as: ");
		for (ServicePlan plan : (List<ServicePlan>) container.getServicePlans()) {
			System.out.print(plan);
		}
	}

	public void getSubscribersCount() {
		System.out.print("Total number of subcribers is: " + container.getSubscribersCount() + "\n");
	}
}
