package by.epam.task1.logic;

import java.util.Comparator;
import by.epam.task1.entity.ServicePlan;

//Comparator class for sorting service plans by monthly fee.

public class MonthlyFeeComparator implements Comparator<ServicePlan> {

	private static MonthlyFeeComparator instance;

	private MonthlyFeeComparator() {

	}

	public static MonthlyFeeComparator getInstance() {
		if (instance == null) {
			instance = new MonthlyFeeComparator();
		}
		return instance;
	}

	@Override
	public int compare(ServicePlan o1, ServicePlan o2) {
		float mf1 = o1.getMonthlyFee();
		float mf2 = o2.getMonthlyFee();
		if (mf1 < mf2) {
			return -1;
		} else if (mf1 > mf2) {
			return 1;
		}
		return 0;

	}

}
