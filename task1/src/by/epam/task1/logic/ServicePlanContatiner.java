package by.epam.task1.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import by.epam.task1.entity.LocalInternetPlan;
import by.epam.task1.entity.LocalPlan;
import by.epam.task1.entity.RoamingPlan;
import by.epam.task1.entity.ServicePlan;

//Class for keeping service plans and performing operations on them.

public class ServicePlanContatiner {

	private List<ServicePlan> listOfServicePlans;

	public ServicePlanContatiner() {
		listOfServicePlans = new ArrayList<ServicePlan>();
	}

	public void addServicePlan(ServicePlan sp) {
		if (sp != null)
			listOfServicePlans.add(sp);
	}

	public List<LocalInternetPlan> findSuitingLocalInternetPlansByInternetTraffic(float min, float max) {
		List<LocalInternetPlan> suitingPlans = new ArrayList<LocalInternetPlan>();
		for (ServicePlan plan : listOfServicePlans) {
			if (isPlanLocalInternet(plan)) {
				if (isNumberWithinRange(((LocalInternetPlan) plan).getInternetTraffic(), min, max)) {
					suitingPlans.add((LocalInternetPlan) plan);
				}
			}
		}
		return suitingPlans;
	}

	public List<LocalPlan> findSuitingLocalPlansByLocalCallCharge(float min, float max) {
		List<LocalPlan> suitingPlans = new ArrayList<LocalPlan>();
		for (ServicePlan plan : listOfServicePlans) {
			if (isPlanLocal(plan)) {
				if (isNumberWithinRange(((LocalPlan) plan).getLocalCallCharge(), min, max)) {
					suitingPlans.add((LocalPlan) plan);
				}
			}
		}
		return suitingPlans;

	}

	public List<RoamingPlan> findSuitingRoamingPlanPlansByOutgoingCallCharge(float min, float max) {
		List<RoamingPlan> suitingPlans = new ArrayList<RoamingPlan>();
		for (ServicePlan plan : listOfServicePlans) {
			if (isPlanRoaming(plan)) {
				if (isNumberWithinRange(((RoamingPlan) plan).getOutgoingCallCharge(), min, max)) {
					suitingPlans.add((RoamingPlan) plan);
				}
			}
		}
		return suitingPlans;

	}

	public List<ServicePlan> getServicePlans() {
		return (List<ServicePlan>) listOfServicePlans;
	}

	public int getSubscribersCount() {
		int numberOfSubcribers = 0;
		for (ServicePlan plan : listOfServicePlans) {
			numberOfSubcribers += plan.getSubscribersCount();
		}
		return numberOfSubcribers;
	}

	public void initialize() {
		ServicePlanFactory planFactory = new ServicePlanFactory();
		addServicePlan(planFactory.createLocalInternetPlan(1200, 100f, 20f, "Cool local Plan", 0f, 5f, 1000f, 500f));
		addServicePlan(planFactory.createLocalInternetPlan(1200, -1, 20f, "Cool local Plan", 0f, 5f, 1000f, 500f));
		addServicePlan(planFactory.createLocalInternetPlan(11000, 150f, 30f, "Just another cool Plan", 0f, 50f, 1500f, 700f));
		addServicePlan(planFactory.createLocalPlan(13000, 80f, 20f, "Pretty good local Plan", 0f, 70f));
		addServicePlan(planFactory.createRoamingPlan(11100, 300f, 100f, "Expensive roaming Plan", 500f, 100f, 500f, 300f));
	}

	public void sortByMonthlyFee() {
		Collections.sort(listOfServicePlans, MonthlyFeeComparator.getInstance());
	}

	private boolean isNumberWithinRange(float number, float min, float max) {
		return (number > min && number < max);
	}

	private boolean isPlanLocal(ServicePlan plan) {
		return (plan.getClass() == LocalPlan.class);
	}

	private boolean isPlanLocalInternet(ServicePlan plan) {
		return (plan.getClass() == LocalInternetPlan.class);
	}

	private boolean isPlanRoaming(ServicePlan plan) {
		return (plan.getClass() == RoamingPlan.class);
	}

}
