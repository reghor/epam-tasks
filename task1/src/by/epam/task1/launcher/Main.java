package by.epam.task1.launcher;

import by.epam.task1.logic.ContainerInfoPrinter;
import by.epam.task1.logic.ServicePlanContatiner;

//Class for launching the project

public class Main {

	public static void main(String[] args) {
		ServicePlanContatiner container=new ServicePlanContatiner();
		container.initialize();
		ContainerInfoPrinter cPrinter=new ContainerInfoPrinter(container);
		cPrinter.findSuitingLocalInternetPlansByInternetTraffic(0, 5000);
		cPrinter.getSubscribersCount();
		container.sortByMonthlyFee();
		cPrinter.getServicePlans();
	}

}
