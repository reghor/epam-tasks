package by.epam.task1.entity;

//Entity Class that describes LocalInternetPlan.

public class LocalInternetPlan extends LocalPlan {
	private float internetSpeed;
	private float internetTraffic;
	
	public LocalInternetPlan(int subscribersCount, float monthlyFee, float smsCharge, String planName, float incomingCallCharge, float outgoingCallCharge, float internetSpeed, float internetTraffic) {
		super(subscribersCount, monthlyFee, smsCharge, planName, incomingCallCharge, outgoingCallCharge);
		this.internetSpeed = internetSpeed;
		this.internetTraffic = internetTraffic;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		LocalInternetPlan other = (LocalInternetPlan) obj;
		if (Float.floatToIntBits(internetSpeed) != Float
				.floatToIntBits(other.internetSpeed))
			return false;
		if (Float.floatToIntBits(internetTraffic) != Float
				.floatToIntBits(other.internetTraffic))
			return false;
		return true;
	}
	public float getInternetSpeed() {
		return internetSpeed;
	}
	public float getInternetTraffic() {
		return internetTraffic;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Float.floatToIntBits(internetSpeed);
		result = prime * result + Float.floatToIntBits(internetTraffic);
		return result;
	}
	public void setInternetSpeed(float internetSpeed) {
		this.internetSpeed = internetSpeed;
	}
	public void setInternetTraffic(float internetTraffic) {
		this.internetTraffic = internetTraffic;
	}
	public String toString() {
		StringBuilder sb = new StringBuilder();
        String newLine = System.getProperty("line.separator");

        sb.append(">Service plan<")
                .append(newLine)
                .append("\tPlan name: ")
                .append(getPlanName())
                .append(newLine)
                .append("\tNumber of subscribers: ")
                .append(getSubscribersCount())
                .append(newLine)
                .append("\tMonthly fee: ")
                .append(getMonthlyFee())
                .append(newLine)
                .append("\tSms charge	: ")
                .append(getsmsCharge())
                .append(newLine)
		        .append("\tLocalCallCharge: ")
		        .append(getLocalCallCharge())
		        .append(newLine)
		        .append("\tExternalCallCharge: ")
		        .append(getExternalCallCharge())
		        .append(newLine)
		        .append("\tInternetSpeed ")
		        .append(getInternetSpeed())
		        .append(newLine)
		        .append("\tInternetTraffic ")
		        .append(getInternetTraffic())
        		.append(newLine);
        return sb.toString();
	}

}
