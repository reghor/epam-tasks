package by.epam.task1.entity;

//Entity Class that describes LocalPlan

public class LocalPlan extends ServicePlan {
	private float externalCallCharge;

	private float localCallCharge;

	public LocalPlan(int subscribersCount, float monthlyFee, float smsCharge, String planName, float incomingCallCharge, float outgoingCallCharge) {
		super(subscribersCount, monthlyFee, smsCharge, planName);
		this.localCallCharge = incomingCallCharge;
		this.externalCallCharge = outgoingCallCharge;
	}

	public float getExternalCallCharge() {
		return externalCallCharge;
	}

	public float getLocalCallCharge() {
		return localCallCharge;
	}

	public void setExternalCallCharge(float externalCallCharge) {
		this.externalCallCharge = externalCallCharge;
	}

	public void setLocalCallCharge(float localCallCharge) {
		this.localCallCharge = localCallCharge;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		String newLine = System.getProperty("line.separator");

		sb.append(">Service plan<").append(newLine).append("\tPlan name: ").append(getPlanName()).append(newLine).append("\tNumber of subscribers: ")
				.append(getSubscribersCount()).append(newLine).append("\tMonthly fee: ").append(getMonthlyFee()).append(newLine)
				.append("\tSms charge	: ").append(getsmsCharge()).append(newLine).append("\tLocalCallCharge: ").append(getLocalCallCharge())
				.append(newLine).append("\tExternalCallCharge: ").append(getExternalCallCharge()).append(newLine);
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Float.floatToIntBits(localCallCharge);
		result = prime * result + Float.floatToIntBits(externalCallCharge);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		LocalPlan other = (LocalPlan) obj;
		if (Float.floatToIntBits(localCallCharge) != Float.floatToIntBits(other.localCallCharge))
			return false;
		if (Float.floatToIntBits(externalCallCharge) != Float.floatToIntBits(other.externalCallCharge))
			return false;
		return true;
	}
}
