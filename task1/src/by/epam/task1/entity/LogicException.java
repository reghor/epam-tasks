package by.epam.task1.entity;

//Class Exception that can be thrown when logic of the application is violated. 

public class LogicException extends Exception{
	private static final long serialVersionUID = 1L;
	public LogicException(String message){
		super(message);
	}
	public LogicException(String message, Throwable throwable){
		super(message,throwable);
	}
	public LogicException(Throwable throwable){
		super(throwable);
	}
}
