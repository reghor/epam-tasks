package by.epam.task1.entity;

//Entity Class that describes RoamingPlan

public class RoamingPlan extends ServicePlan {
	private float incomingCallCharge;
	private float internetSpeed;
	private float internetTraffic;
	private float outgoingCallCharge;

	public RoamingPlan(int subscribersCount, float monthlyFee, float smsCharge, String planName, float internetSpeed, float internetTraffic,
			float outgoingCallCharge, float incomingCallCharge) {
		super(subscribersCount, monthlyFee, smsCharge, planName);
		this.internetTraffic = internetTraffic;
		this.internetSpeed = internetSpeed;
		this.outgoingCallCharge = outgoingCallCharge;
		this.incomingCallCharge = incomingCallCharge;
	}

	public float getIncomingCallCharge() {
		return incomingCallCharge;
	}

	public float getInternetSpeed() {
		return internetSpeed;
	}

	public float getInternetTraffic() {
		return internetTraffic;
	}

	public float getOutgoingCallCharge() {
		return outgoingCallCharge;
	}

	public void setIncomingCallCharge(float incomingCallCharge) {
		this.incomingCallCharge = incomingCallCharge;
	}

	public void setInternetSpeed(float internetSpeed) {
		this.internetSpeed = internetSpeed;
	}

	public void setInternetTraffic(float internetTraffic) {
		this.internetTraffic = internetTraffic;
	}

	public void setOutgoingCallCharge(float outgoingCallCharge) {
		this.outgoingCallCharge = outgoingCallCharge;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		String newLine = System.getProperty("line.separator");

		sb.append(">Service plan<").append(newLine).append("\tPlan name: ").append(getPlanName()).append(newLine).append("\tNumber of subscribers: ")
				.append(getSubscribersCount()).append(newLine).append("\tMonthly fee: ").append(getMonthlyFee()).append(newLine)
				.append("\tSms charge	: ").append(getsmsCharge()).append(newLine).append("\tIncomingCallCharge: ").append(getIncomingCallCharge())
				.append(newLine).append("\tOutgoingCallCharge: ").append(getOutgoingCallCharge()).append(newLine).append("\tInternetSpeed ")
				.append(getInternetSpeed()).append(newLine).append("\tInternetTraffic ").append(getInternetTraffic()).append(newLine);
		return sb.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoamingPlan other = (RoamingPlan) obj;
		if (Float.floatToIntBits(incomingCallCharge) != Float.floatToIntBits(other.incomingCallCharge))
			return false;
		if (Float.floatToIntBits(internetSpeed) != Float.floatToIntBits(other.internetSpeed))
			return false;
		if (Float.floatToIntBits(internetTraffic) != Float.floatToIntBits(other.internetTraffic))
			return false;
		if (Float.floatToIntBits(outgoingCallCharge) != Float.floatToIntBits(other.outgoingCallCharge))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Float.floatToIntBits(incomingCallCharge);
		result = prime * result + Float.floatToIntBits(internetSpeed);
		result = prime * result + Float.floatToIntBits(internetTraffic);
		result = prime * result + Float.floatToIntBits(outgoingCallCharge);
		return result;
	}

}
