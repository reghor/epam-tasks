package by.epam.task1.entity;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;

//Abstract Entity Class describes common features of different service plans and usually is extended by them.

public abstract class ServicePlan {

	private static final org.apache.log4j.Logger logger = LogManager.getLogger(ServicePlan.class);
	static {
		logger.setLevel(Level.INFO);
	}

	private float monthlyFee;

	private String planName;

	private float smsCharge;

	private int subscribersCount;

	public ServicePlan(int subscribersCount, float monthlyFee, float smsCharge, String planName) {
		try {
			if (monthlyFee < 0 || monthlyFee > 1000) {
				throw new LogicException("Wrong monthly Fee");
			}
		} catch (LogicException le) {
			logger.error("Wrong input for constructor", le);
			return;
		}
		this.subscribersCount = subscribersCount;
		this.monthlyFee = monthlyFee;
		this.smsCharge = smsCharge;
		this.planName = planName;
	}

	public float getMonthlyFee() {
		return monthlyFee;
	}

	public String getPlanName() {
		return planName;
	}

	public float getsmsCharge() {
		return smsCharge;
	}

	public int getSubscribersCount() {
		return subscribersCount;
	}

	public void setMonthlyFee(float monthlyFee) {
		this.monthlyFee = monthlyFee;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public void setsmsCharge(float smsCharge) {
		this.smsCharge = smsCharge;
	}

	public void setSubscribersCount(int subscribersCount) {
		this.subscribersCount = subscribersCount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(monthlyFee);
		result = prime * result + subscribersCount;
		result = prime * result + Float.floatToIntBits(smsCharge);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServicePlan other = (ServicePlan) obj;
		if (Float.floatToIntBits(monthlyFee) != Float.floatToIntBits(other.monthlyFee))
			return false;
		if (subscribersCount != other.subscribersCount)
			return false;
		if (Float.floatToIntBits(smsCharge) != Float.floatToIntBits(other.smsCharge))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		String newLine = System.getProperty("line.separator");

		sb.append(">Service plan<").append(newLine).append("\tPlan name: ").append(planName).append(newLine).append("\tNumber of subscribers: ")
				.append(subscribersCount).append(newLine).append("\tMonthly fee: ").append(monthlyFee).append(newLine).append("\tSms charge	: ")
				.append(smsCharge);
		return sb.toString();
	}

}
