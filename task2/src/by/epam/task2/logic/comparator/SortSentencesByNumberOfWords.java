package by.epam.task2.logic.comparator;

import java.util.Comparator;

import by.epam.task2.common.constant.PropertyLoader;
import by.epam.task2.entity.ElementType;
import by.epam.task2.entity.TextElement;

public class SortSentencesByNumberOfWords implements Comparator<TextElement> {

	@Override
	public int compare(TextElement sentence1, TextElement sentence2) {
		if (sentence1.getType()==ElementType.SENTENCE&&sentence2.getType()==ElementType.SENTENCE){
			int sentence1WordCount=0, sentence2WordCount=0;
			for (TextElement lexeme:sentence1.getChildElements()){
				if (isLexemeWord(lexeme.getTextContent())){
					sentence1WordCount++;
				}
			}
			for (TextElement lexeme:sentence2.getChildElements()){
				if (isLexemeWord(lexeme.getTextContent())){
					sentence2WordCount++;
				}
			}
			if(sentence1WordCount>sentence2WordCount){
				return 1;
			} else if(sentence1WordCount<sentence2WordCount){
				return -1;
			} else {
				return 0;
			}
		}
		return 0;
	}
	
	private boolean isLexemeWord(String lexeme){
		return lexeme.matches(PropertyLoader.WORD_PATTERN);
		
	}

}
