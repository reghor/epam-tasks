package by.epam.task2.logic.parser;

import java.util.ArrayList;
import java.util.List;

import by.epam.task2.common.constant.PropertyLoader;
import by.epam.task2.entity.ComplexTextElement;
import by.epam.task2.entity.ElementType;
import by.epam.task2.entity.TextElement;

public class ParagraphParser implements ElementParser {

	private ElementParser nextHandler;

	private static ParagraphParser instance;

	private ParagraphParser(){

	}

	public static ParagraphParser getInstance(){
		if(instance == null) {
			instance = new ParagraphParser();
		}

		return instance;
	}

	private static List<TextElement> allSentences=new ArrayList<TextElement>(); 

	public static  List<TextElement> getAllSentences(){
		return allSentences;
	}

	public static void clearAllSentencesList(){
			allSentences.clear();
	}

	@Override
	public void setNext(ElementParser nextHandler) {
		this.nextHandler=nextHandler;
	}

	@Override
	public List<TextElement> parseText(String text, ElementType type) {
		if (type==ElementType.PARAGRAPH){
			String[] processedText=text.split(PropertyLoader.SENTENCE_PATTERN);
			List<TextElement> list=new ArrayList<TextElement>();
			for (String s:processedText){
				list.add(new ComplexTextElement(ElementType.SENTENCE, s));
			}
			allSentences.addAll(list);
			return list;
		} else{
			return nextHandler.parseText(text, type);
		}
	}

	@Override
	public String assembleText(List<String> list, ElementType type) {
		if (type==ElementType.PARAGRAPH){
			StringBuilder sb=new StringBuilder();
			for (String strElement:list){
				sb.append(strElement);
			}
			return sb.toString();
		} else{
			return nextHandler.assembleText(list, type);
		}
	}
}
