package by.epam.task2.logic.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import by.epam.task2.common.constant.PropertyLoader;
import by.epam.task2.entity.ElementType;
import by.epam.task2.entity.SimpleTextElement;
import by.epam.task2.entity.TextElement;

public class SentenceParser implements ElementParser {
	
	private ElementParser nextHandler;

	private static SentenceParser instance;
	
	private SentenceParser(){

	}
	
	public static SentenceParser getInstance(){
		if(instance == null) {
			instance = new SentenceParser();
		}
		return instance;
	}

	private int suitableWordLength;
	
	public int getSuitableWordLength() {
		return suitableWordLength;
	}

	public void setSuitableWordLength(int suitableWordLength) {
		this.suitableWordLength = suitableWordLength;
	}
	
	private boolean deleteSuitableWords=false;

	public boolean isDeleteSuitableWords() {
		return deleteSuitableWords;
	}

	public void setDeleteSuitableWords(boolean deleteSuitableWords) {
		this.deleteSuitableWords = deleteSuitableWords;
	}

	@Override
	public void setNext(ElementParser nextHandler) {
		this.nextHandler=nextHandler;
	}

	@Override
	public List<TextElement> parseText(String text, ElementType type) {
		if (type==ElementType.SENTENCE){
			List<TextElement> list=new ArrayList<TextElement>();
			Pattern pattern =Pattern.compile(PropertyLoader.LEXEME_PATTERN);
			Matcher matcher=pattern.matcher(text);
			while(matcher.find()){
				String s=matcher.group();
				list.add(new SimpleTextElement(detectTypeOfLexeme(s), s));
			}
			return list;
		} else{
			throw new UnsupportedOperationException();
		}
		
		
		
	}

	@Override
	public String assembleText(List<String> list, ElementType type) {
		if (type==ElementType.SENTENCE){
			StringBuilder sb=new StringBuilder();
			for (String strElement:list){
				if (!deleteSuitableWords){
					sb.append(strElement);
				} else{
					boolean match=strElement.matches(PropertyLoader.CONSONANT_PATTERN);
					int n=strElement.length();
					if(match&&n==getSuitableWordLength()){
						
					} else{
						sb.append(strElement);
					}
				}
			}
			return sb.toString();
		} else{
			return nextHandler.assembleText(list, type);
		}
	}
	
	private ElementType detectTypeOfLexeme(String lexeme){
		if (isLexemeWord(lexeme)){
			return ElementType.WORD;
		} else{
			return ElementType.PUNCTUATION;
		}
	}
	
	private boolean isLexemeWord(String lexeme){
		return lexeme.matches(PropertyLoader.WORD_PATTERN);
		
	}
}
