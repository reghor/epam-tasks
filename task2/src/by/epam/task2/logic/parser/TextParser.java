package by.epam.task2.logic.parser;

import java.util.ArrayList;
import java.util.List;

import by.epam.task2.common.constant.PropertyLoader;
import by.epam.task2.entity.ComplexTextElement;
import by.epam.task2.entity.ElementType;
import by.epam.task2.entity.TextElement;

public  class TextParser implements ElementParser {
	
	private ElementParser nextHandler;
	
	private static TextParser instance;

	private TextParser(){

	}

	public static TextParser getInstance(){
		if(instance == null) {
			instance = new TextParser();
		}
		return instance;
	}

	@Override 
	public List<TextElement> parseText(String text, ElementType type){
		if (type==ElementType.TEXT){
			String[] processedText=text.split(PropertyLoader.PARAGRAPH_PATTERN);
			List<TextElement> list=new ArrayList<TextElement>();
			for (String s:processedText){
				list.add(new ComplexTextElement(ElementType.PARAGRAPH, s));
			}
			return list;
		} else{
			return nextHandler.parseText(text, type);
		}
	}

	@Override
	public void setNext(ElementParser nextHandler) {
		this.nextHandler=nextHandler;
	}

	@Override
	public String assembleText(List<String> list, ElementType type) {
		if (type==ElementType.TEXT){
			StringBuilder sb=new StringBuilder();
			for (String strElement:list){
				sb.append(strElement);
				sb.append("\n");
			}
			return sb.toString();
		} else{
			return nextHandler.assembleText(list, type);
		}
	}
}
