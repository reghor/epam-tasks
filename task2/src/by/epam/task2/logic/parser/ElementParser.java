package by.epam.task2.logic.parser;

import java.util.List;

import by.epam.task2.entity.ElementType;
import by.epam.task2.entity.TextElement;

public interface ElementParser {
	
	public void setNext(ElementParser nextHandler);
	
	public List<TextElement> parseText(String text, ElementType type);
	
	public String assembleText(List<String> list, ElementType type);
}
