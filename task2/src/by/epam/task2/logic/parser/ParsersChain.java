package by.epam.task2.logic.parser;

import java.util.List;

import by.epam.task2.entity.ElementType;
import by.epam.task2.entity.TextElement;

public class ParsersChain {
	
	private static  ParsersChain instance;
	
	private ElementParser firstHandler;
	
	private ParsersChain(){
		
	}
	
	{
		createParsingChain();
	}
	
	public static ParsersChain getInstance(){
		if(instance == null) {
            instance = new ParsersChain();
        }

        return instance;
	}
	
	public void createParsingChain(){
		SentenceParser sentenceHandler=SentenceParser.getInstance();
		ParagraphParser paragraphHandler=ParagraphParser.getInstance();
		TextParser textHandler=TextParser.getInstance();
		textHandler.setNext((ElementParser)paragraphHandler);
		firstHandler=(ElementParser)textHandler;
		paragraphHandler.setNext((ElementParser)sentenceHandler);
	}
	
	public void createAssemblingChain(){
		SentenceParser sentenceHandler=SentenceParser.getInstance();
		ParagraphParser paragraphHandler=ParagraphParser.getInstance();
		TextParser textHandler=TextParser.getInstance();
		firstHandler=(ElementParser) sentenceHandler;
		sentenceHandler.setNext((ElementParser)paragraphHandler);
		paragraphHandler.setNext((ElementParser)textHandler);
	}
	
	public String assembleText(List<String> list, ElementType type){
		return firstHandler.assembleText(list, type);
	}
	
	public List<TextElement> parseText(String text, ElementType type){
		return firstHandler.parseText(text,  type);
	}
}
