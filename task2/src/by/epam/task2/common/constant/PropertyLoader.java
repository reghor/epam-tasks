package by.epam.task2.common.constant;

import java.util.ResourceBundle;

public final class PropertyLoader {

	static {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("regex");
		PARAGRAPH_PATTERN = resourceBundle.getString("pattern.paragraph");
		SENTENCE_PATTERN = resourceBundle.getString("pattern.sentence");
		LEXEME_PATTERN = resourceBundle.getString("pattern.lexeme");
		WORD_PATTERN = resourceBundle.getString("pattern.word");
		PUNCTUATION_PATTERN = resourceBundle.getString("pattern.punctuation");
		CONSONANT_PATTERN = resourceBundle.getString("pattern.consonant");
		resourceBundle = ResourceBundle.getBundle("file");
		INPUT_FILE_PATH = resourceBundle.getString("file.input.path");
		OUTPUT_FILE_PATH = resourceBundle.getString("file.output.path");
	}

	private PropertyLoader() {

	}

	public static final String PARAGRAPH_PATTERN;
	public static final String SENTENCE_PATTERN;
	public static final String LEXEME_PATTERN;
	public static final String WORD_PATTERN;
	public static final String PUNCTUATION_PATTERN;
	public static final String INPUT_FILE_PATH;
	public static final String OUTPUT_FILE_PATH;
	public static final String CONSONANT_PATTERN;
}
