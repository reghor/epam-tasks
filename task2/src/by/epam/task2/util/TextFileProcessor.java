package by.epam.task2.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;

public final class TextFileProcessor {

	private static final org.apache.log4j.Logger logger= LogManager.getLogger(TextFileProcessor.class);
	static {
		logger.setLevel(Level.INFO);
	}

	public static String getTextFromFile(String path) throws IOException{
		StringBuilder sb = new StringBuilder();
			FileReader fr = new FileReader(path);
			BufferedReader br = new BufferedReader(fr);
			String str;
			while ((str = br.readLine()) != null) {
				sb.append(str);
				sb.append('\n');
			}
			fr.close();
		return sb.toString();
	}

	public static void writeTextToFile(String text, String path) throws IOException{
		BufferedWriter bufferedWriter = null;
		try {
			bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path)));
			bufferedWriter.write(text);
		
		} finally {
			try {
				bufferedWriter.close();
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

}
