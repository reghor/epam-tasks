package by.epam.task2.launcher;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;

import by.epam.task2.common.constant.PropertyLoader;
import by.epam.task2.textoperations.TextOperationsProcessor;
import by.epam.task2.util.TextFileProcessor;

public class Main {
	private static final org.apache.log4j.Logger logger = LogManager.getLogger(TextFileProcessor.class);
	static {
		logger.setLevel(Level.INFO);
	}

	public static void main(String[] args) {
		try {
			TextOperationsProcessor.LoadText(PropertyLoader.INPUT_FILE_PATH);
			TextOperationsProcessor.startParsing();
			System.out.print(TextOperationsProcessor.startAssembling());
			System.out.print(TextOperationsProcessor.getAllSentecesSortedByNumberOfWords());
			TextOperationsProcessor.switchToDeletingWords(4);
			TextFileProcessor.writeTextToFile(TextOperationsProcessor.startAssembling(), PropertyLoader.OUTPUT_FILE_PATH);
			TextOperationsProcessor.switchOffDeletingWords();
		} catch (NullPointerException e) {
			logger.error(e);
		} catch (FileNotFoundException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}
	}
}