package by.epam.task2.textoperations;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import by.epam.task2.entity.ComplexTextElement;
import by.epam.task2.entity.ElementType;
import by.epam.task2.entity.TextElement;
import by.epam.task2.logic.comparator.SortSentencesByNumberOfWords;
import by.epam.task2.logic.parser.ParagraphParser;
import by.epam.task2.logic.parser.ParsersChain;
import by.epam.task2.logic.parser.SentenceParser;
import by.epam.task2.util.TextFileProcessor;

public abstract class TextOperationsProcessor {
	
	private static final ParsersChain chain=ParsersChain.getInstance();
	
	private static String text;
	
	private static TextElement root;
	
	public static String getText(){
		return text;
	}
	
	public static void startParsing (){
		if (!text.isEmpty()){
			ParagraphParser.clearAllSentencesList();
			chain.createParsingChain();
			root=new ComplexTextElement(ElementType.TEXT,text);
		}
	}
	
	public static String startAssembling(){
		if (root!=null){
			chain.createAssemblingChain();
			return root.assembleText();
		}
		return "There is nothing to assemble! Parse text first.";
	}
	
	public static void LoadText (String path) throws IOException{
		text=TextFileProcessor.getTextFromFile(path);
	}
	
	public static List<TextElement> parseText(String text,ElementType type){
		return chain.parseText(text,type);
	}
	
	public static String assembleText(List<String> list, ElementType outputType){
		return chain.assembleText(list, outputType);
	}
	
	public static String getAllSentecesSortedByNumberOfWords() {
		List<TextElement> listOfSentences=ParagraphParser.getAllSentences();
		Collections.sort(listOfSentences,new SortSentencesByNumberOfWords());
		StringBuilder allSentences=new StringBuilder();
		for (TextElement sentence:listOfSentences){
			allSentences.append(sentence.assembleText());
		}
		return allSentences.toString();
		
		
	}
	
	public static void switchToDeletingWords(int suitableLength){
		SentenceParser sParser= SentenceParser.getInstance();
		sParser.setDeleteSuitableWords(true);
		sParser.setSuitableWordLength(suitableLength);
	}
	
	public static void switchOffDeletingWords(){
		SentenceParser.getInstance().setDeleteSuitableWords(false);
	}
}
