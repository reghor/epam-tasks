package by.epam.task2.entity;

import java.util.List;

public interface TextElement {
	
	public String assembleText();
	
	public void parseText();
	
	public ElementType getType();
	
	public List<TextElement> getChildElements();
	
	public void setChildElements(List<TextElement> list);
	
	public void setTextContent(String text);
	
	public String getTextContent();
	
	public void split(String textRepresentation, ElementType type);

}
