package by.epam.task2.entity;

import java.util.List;

import by.epam.task2.entity.ElementType;

public class SimpleTextElement implements TextElement {
	
	private ElementType type;
	
	private String textContent;
	
	public SimpleTextElement(ElementType type, String textRepresentation){
		this.type = type;
		setTextContent(textRepresentation);
	}

	@Override
	public String assembleText() {
		return getTextContent();
	}

	@Override
	public void parseText() {
		throw new UnsupportedOperationException();
	}

	@Override
	public ElementType getType() {
		return type;
	}

	@Override
	public List<TextElement> getChildElements() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setTextContent(String text) {
		 this.textContent=text;
	}

	@Override
	public String getTextContent() {
		return textContent;
	}

	@Override
	public void setChildElements(List<TextElement> list) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void split(String textRepresentation, ElementType type) {
		throw new UnsupportedOperationException();		
	}
}
