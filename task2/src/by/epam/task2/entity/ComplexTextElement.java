package by.epam.task2.entity;

import java.util.ArrayList;
import java.util.List;

import by.epam.task2.entity.ElementType;
import by.epam.task2.textoperations.TextOperationsProcessor;

public class ComplexTextElement implements TextElement {
	
	private ElementType type;
	
	private List<TextElement> childElements;
	
	public ComplexTextElement( ElementType type, String textRepresentation ) {
		this.type=type;
		split(textRepresentation,getType());
	}

	@Override
	public String assembleText() {
		List<String> list=new ArrayList<String>();
		for (TextElement childElement:getChildElements()){
			list.add(childElement.assembleText());
		}
		return TextOperationsProcessor.assembleText(list, getType());
	}

	@Override
	public void parseText() {

	}

	@Override
	public ElementType getType() {
		return type;
	}

	@Override
	public List<TextElement> getChildElements() {
		return childElements;
	}

	@Override
	public void setTextContent(String text) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getTextContent() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setChildElements(List<TextElement> list) {
		childElements=list;
	}

	@Override
	public void split(String textRepresentation, ElementType type) {
		setChildElements(TextOperationsProcessor.parseText(textRepresentation,getType()));
	}

}
