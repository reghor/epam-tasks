package by.epam.task2.entity;

public enum ElementType{
	//Complex element types
	TEXT, PARAGRAPH, SENTENCE,

	//Simple element types
	WORD, PUNCTUATION;
}