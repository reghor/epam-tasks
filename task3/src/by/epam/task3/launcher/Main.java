package by.epam.task3.launcher;

import java.util.LinkedList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import by.epam.task3.entity.Client;
import by.epam.task3.entity.Operator;

//Class for launching the program
public class Main {

	public static void main(String[] args) {
		ScheduledExecutorService executor = Executors.newScheduledThreadPool(10);

		LinkedList<Client> clientList = new LinkedList<Client>();
		clientList.add(new Client("Vasya"));
		clientList.add(new Client("Tanya"));
		// clientList.add(new Client("Petya"));
		// clientList.add(new Client("Kolya"));
		// clientList.add(new Client("Elena"));
		// clientList.add(new Client("Anna"));
		for (int i = 0; i < clientList.size(); i++) {
			executor.schedule(clientList.get(i), 1 + i, TimeUnit.SECONDS);
		}

		LinkedList<Operator> operatorList = new LinkedList<Operator>();
		// operatorList.add(new Operator("Bob"));
		// operatorList.add(new Operator("Sandra"));
		operatorList.add(new Operator("John"));
		for (int i = 0; i < operatorList.size(); i++) {
			executor.schedule(operatorList.get(i), 500, TimeUnit.MILLISECONDS);
		}

		executor.shutdown();
	}
}