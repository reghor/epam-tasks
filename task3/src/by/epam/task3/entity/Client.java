package by.epam.task3.entity;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

//Entity class represents a Client(Call) 

public class Client extends Thread {

	private CountDownLatch latch = new CountDownLatch(1);

	private volatile boolean waiting = true;

	private final Random random = new Random();

	public boolean isWaiting() {
		return waiting;
	}

	public void setWaiting(boolean isWaiting) {
		this.waiting = isWaiting;
	}

	private static final Logger LOGGER;

	static {
		LOGGER = Logger.getLogger(Client.class);
		new DOMConfigurator().doConfigure("log4j.xml", LogManager.getLoggerRepository());
		LOGGER.setLevel(Level.INFO);
	}

	public void run() {

		ClientQueue.enqueueClient(this);
		while (waiting) {
			if (random.nextBoolean()) {
				try {
					latch.await(random.nextInt(2000) + 1000, TimeUnit.MILLISECONDS);
					if (!waiting) {
						break;
					}
					ClientQueue.removeFromQueue(this);
					reportClientTiredToWait();
					sleep(random.nextInt(1000) + 500);
					ClientQueue.enqueueClient(this);
					reportClientDecidedToCallAgain();
				} catch (InterruptedException e) {
					LOGGER.error(e);
				}
			}
		}
	}

	public void await(long sleepTime) throws InterruptedException {
		waiting = false;
		latch.countDown();
		sleep(sleepTime);
	}

	public Client(String name) {
		super(name);
	}

	private void reportClientTiredToWait() {
		LOGGER.info("Client " + getName() + " was tired to wait and decided to hang up");
	}

	private void reportClientDecidedToCallAgain() {
		LOGGER.info("Client " + getName() + " decided to call again");
	}

	@Override
	public String toString() {
		return "Client " + getName();
	}
}
