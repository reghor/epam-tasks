package by.epam.task3.entity;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

//Entity Class ClientQueue - Queue which keeps clients

public class ClientQueue {

	private static final Logger LOGGER;

	static {
		LOGGER = Logger.getLogger(ClientQueue.class);
		new DOMConfigurator().doConfigure("log4j.xml", LogManager.getLoggerRepository());
		LOGGER.setLevel(Level.INFO);
	}

	private static final ClientQueue instance=new ClientQueue();

	private BlockingQueue<Client> queue;

	public static void printQueue() {
		System.out.println("LIST OF CLIENTS:");
		for (Client client : ClientQueue.getInstance().queue) {
			System.out.println("CLIENT " + client.getName());
		}
		System.out.println("END OF LIST OF CLIENTS:");

	}

	private static ClientQueue getInstance() {
		return instance;
	}

	private ClientQueue() {
		this.queue = new LinkedBlockingQueue<Client>();
	}

	public static void enqueueClient(Client client) {
		getInstance().queue.add(client);
		reportClientEnqueued(client.getName());
	}

	public static void removeFromQueue(Client client) {
		ClientQueue.getInstance().queue.remove(client);
		reportClientDeletedFromQueue(client.getName());
	}

	public static Client pollFirst(long time, TimeUnit timeUnit) throws InterruptedException {
		Client client = null;
		client = getInstance().queue.poll(time, timeUnit);

		if (client != null) {
			reportClientRetrievedFromQueue(client.getName());
		}
		return client;
	}

	private static void reportClientEnqueued(String name) {
		// LOGGER.info("Client "+name+" was put on the waiting list");
	}

	private static void reportClientDeletedFromQueue(String name) {
		// LOGGER.info("Client " +name+" was deleted from waiting list");
	}

	private static void reportClientRetrievedFromQueue(String name) {
		// LOGGER.info("Client " +name+" was retrieved from waiting list");
	}

}
