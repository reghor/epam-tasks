package by.epam.task3.entity;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

// Entity Class Operator processes calls from clients

public class Operator extends Thread {

	private static final Logger LOGGER;

	private Random random = new Random();

	static {
		LOGGER = Logger.getLogger(Operator.class);
		new DOMConfigurator().doConfigure("log4j.xml", LogManager.getLoggerRepository());
		LOGGER.setLevel(Level.INFO);
	}

	private volatile boolean running;

	public Operator(String name) {
		super(name);
		running = true;
	}

	@Override
	public void run() {
		while (running) {
			Client client = null;
			try {
				client = ClientQueue.pollFirst(5, TimeUnit.SECONDS);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			if (client != null) {
				String clientName = client.getName();
				reportOperatorReceivedCall(clientName);
				try {
					long conversationTime = random.nextInt(2000) + 3000;
					client.await(conversationTime);
					sleep(conversationTime);
					reportOperatorFinishedConversation(clientName);
				} catch (InterruptedException e) {
					LOGGER.error(e);
				}
			} else {
				reportOperatorFinishedHisWork();
				running = false;
			}
		}
	}

	private void reportOperatorReceivedCall(String name) {
		LOGGER.info("Operator " + getName() + " received call from Client " + name);
	}

	private void reportOperatorFinishedConversation(String name) {
		LOGGER.info("Operator " + getName() + " finished conversation with Client " + name);
	}

	private void reportOperatorFinishedHisWork() {
		LOGGER.info("Operator " + getName() + " finished his work for today, he(she) is too tired and decided to go home.");
	}

}
