$(document).ready(function() {
	$('.order_id_form').hide();
	var table=$('#table_id').DataTable({
		"stateSave": true,
		"stateDuration": 7,
		 "ajax": {
	            "url": path,
				"type": 'GET',    
				 "data": function ( d ) {
		                d.command = "get_all_customer_orders_ajax";
				 }
		 },
		"language": {
			"url": dataTablesLocale
		},
		"processing": true,

		"columns": [
		            { "data": "id" },
		            { "data": "description" },
		            { "data": "maxLoadHeight" },
		            { "data": "maxLoadWidth" },
		            { "data": "maxLoadLength" },
		            { "data": "weight" },
		            { "data": "volume" },
		            { "data": "arrivalPoint" },
		            { "data": "departurePoint" },
		            { "data": "statusRepr" }
		            ]
	})
	;
	$('#table_id tbody').on( 'click', 'tr', function () {
		if ( $(this).hasClass('selected') ) {
			$(this).removeClass('selected');
			$(".order_id").removeAttr('value');
			$('.order_id_form').hide();
		}
		else {
			table.$('tr.selected').removeClass('selected');
			$(this).addClass('selected');
			$(".order_id").attr('value',table.row('.selected').data().id);
			$('.order_id_form').show();
		}
	} );


});