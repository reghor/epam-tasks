$(document).ready(function() {
	$('#regions_departure').attr("disabled", true);
	$('#regions_arrival').attr("disabled", true);
//	$('#cities_departure').attr("disabled", true).append($("<option></option>")
//			.attr("value","")
//			.text(defaultPickCity)); 
//	$('#cities_arrival').attr("disabled", true).append($("<option></option>")
//			.attr("value","")
//			.text(defaultPickCity)); 
	dataString ="command=get_countries_ajax";

	$.ajax({
		type: "POST",
		url: path,
		data: dataString,
		dataType: "json",

		success: function( data, textStatus, jqXHR) {
			if(data.success){

				var list = data.countryContainer;
				$.each(list, function( key, value ) {
					$('#countries_departure')
					.append($("<option></option>")
							.attr("value",value)
							.text(key)); 
					$('#countries_arrival')
					.append($("<option></option>")
							.attr("value",value)
							.text(key)); 
				});
				$('#countries_departure')
				.append($("<option></option>")
						.attr("disabled","disabled").attr("selected","selected")
						.text(defaultPickCountry)); 
				$('#countries_arrival')
				.append($("<option></option>")
						.attr("disabled","disabled").attr("selected","selected")
						.text(defaultPickCountry)); 

			} 
			else {
				
			}
		},

		error: function(jqXHR, textStatus, errorThrown){

		},

		
		beforeSend: function(jqXHR, settings){
			
		},

		
		complete: function(jqXHR, textStatus){
		}

	});        

	$("#countries_departure").change(function(){

		dataString ="country_id="+$("#countries_departure").find('option:selected', this).attr('value')+"&command=get_country_regions_ajax";

		$.ajax({
			type: "POST",
			url: path,
			data: dataString,
			dataType: "json",

			success: function( data, textStatus, jqXHR) {
				if(data.success){

					var list = data.regionContainer;
					$.each(list, function( key, value ) {
						$('#regions_departure')
						.append($("<option></option>")
								.attr("value",value)
								.text(key)); 
					});
					$('#regions_departure')
					.append($("<option></option>")
							.attr("disabled","disabled").attr("selected","selected")
							.text(defaultPickRegion)); 

				} 
				else {
					$("#ajaxResponse").html("<div><b>Country code in Invalid!</b></div>");
				}
			},

			error: function(jqXHR, textStatus, errorThrown){
				$("#ajaxResponse").html(jqXHR.responseText);
			},

			beforeSend: function(jqXHR, settings){
				$('#regions_departure').attr("disabled", true);
				$('#regions_departure').find('option').remove();
			},

			complete: function(jqXHR, textStatus){
				$('#regions_departure').attr("disabled", false);
				$('#cities_departure').find('option').remove();
				$('#cities_departure')
				.append($("<option></option>")
						.attr("value","")
						.text(defaultPickCity)); 
			}

		});        
	});
	
	
	$("#countries_arrival").change(function(){

		dataString ="country_id="+$("#countries_arrival").find('option:selected', this).attr('value')+"&command=get_country_regions_ajax";

		$.ajax({
			type: "POST",
			url: path,
			data: dataString,
			dataType: "json",

			success: function( data, textStatus, jqXHR) {
				if(data.success){

					var list = data.regionContainer;
					$.each(list, function( key, value ) {
						$('#regions_arrival')
						.append($("<option></option>")
								.attr("value",value)
								.text(key)); 
					});
					$('#regions_arrival')
					.append($("<option></option>")
							.attr("disabled","disabled").attr("selected","selected")
							.text(defaultPickRegion)); 

				} 
				else {
					$("#ajaxResponse").html("<div><b>Country code in Invalid!</b></div>");
				}
			},

			error: function(jqXHR, textStatus, errorThrown){
			},

			beforeSend: function(jqXHR, settings){
				$('#regions_arrival').attr("disabled", true);
				$('#regions_arrival').find('option').remove();
				
			},

			complete: function(jqXHR, textStatus){
				$('#regions_arrival').attr("disabled", false);
				$('#cities_arrival').find('option').remove();
				$('#cities_arrival')
				.append($("<option></option>")
						.attr("value","")
						.text(defaultPickCity)); 
			}

		});        
	});
	
	
	$("#regions_departure").change(function(){

		dataString ="region_id="+$("#regions_departure").find('option:selected', this).attr('value')+"&command=get_region_cities_ajax";

		$.ajax({
			type: "POST",
			url: path,
			data: dataString,
			dataType: "json",

			success: function( data, textStatus, jqXHR) {
				if(data.success){

					var list = data.cityContainer;
					$.each(list, function( key, value ) {
						$('#cities_departure')
						.append($("<option></option>")
								.attr("value",value)
								.text(key)); 
					});
					$('#cities_departure')
					.append($("<option></option>")
							.attr("disabled","disabled").attr("selected","selected")
							.text(defaultPickCity)); 

				} 
				else {
					$("#ajaxResponse").html("<div><b>Country code in Invalid!</b></div>");
				}
			},

			error: function(jqXHR, textStatus, errorThrown){
				$("#ajaxResponse").html(jqXHR.responseText);
			},

			beforeSend: function(jqXHR, settings){
				$('#cities_departure').attr("disabled", true);
				$('#cities_departure').find('option').remove();
				
			},

			complete: function(jqXHR, textStatus){
				$('#cities_departure').attr("disabled", false);
			}

		});        
	});
	
	
	$("#regions_arrival").change(function(){

		dataString ="region_id="+$("#regions_arrival").find('option:selected', this).attr('value')+"&command=get_region_cities_ajax";

		$.ajax({
			type: "POST",
			url: path,
			data: dataString,
			dataType: "json",

			success: function( data, textStatus, jqXHR) {
				if(data.success){

					var list = data.cityContainer;
					$.each(list, function( key, value ) {
						$('#cities_arrival')
						.append($("<option></option>")
								.attr("value",value)
								.text(key)); 
					});
					$('#cities_arrival')
					.append($("<option></option>")
							.attr("disabled","disabled").attr("selected","selected")
							.text(defaultPickCity)); 

				} 
				else {
					$("#ajaxResponse").html("<div><b>Country code in Invalid!</b></div>");
				}
			},

			error: function(jqXHR, textStatus, errorThrown){
				$("#ajaxResponse").html(jqXHR.responseText);
			},

			beforeSend: function(jqXHR, settings){
				$('#cities_arrival').attr("disabled", true);
				$('#cities_arrival').find('option').remove();
			},

			complete: function(jqXHR, textStatus){
				$('#cities_arrival').attr("disabled", false);
			}

		});        
	});
});
