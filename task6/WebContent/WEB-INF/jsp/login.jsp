<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<fmt:requestEncoding value="UTF-8" />
<fmt:setBundle basename="page_name" var="page_name" />
<fmt:message key="common.login" bundle="${page_name}" var="currentPage" />
<c:set var="currentPage" scope="session" value="${currentPage}" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.common" var="loc" />
<fmt:setBundle basename="regex" var="regex" />
<fmt:message key="login" bundle="${regex}" var="login_pattern" />
<fmt:message key="password" bundle="${regex}" var="password_pattern" />
<fmt:message key="common.register.title.login" bundle="${loc}" var="login_title" />
<fmt:message key="common.register.title.password" bundle="${loc}" var="password_title" />
<html>
<head>
<meta charset="utf-8">
<link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet" type="text/css">
<title>ILogistics</title>
<script lang="javascript">
	function submitform(id) {
		document.getElementById(id).submit();
	}
</script>
<c:if test="${not empty pageContext.request.queryString}">
	<c:set var="lastQueryString" scope="session" value="${pageContext.request.queryString}" />
</c:if>
</head>
<body>
	<div id="main-page">
		<div id="header">
			<div id="header_nav">
				<div id="header_nav_menu">
					<jsp:include page="guestHeaderNav.jsp" />
				</div>
			</div>
			<div id="header_content">
				<div id="header_logo" class="f_left">
					<h1>
						<span style="color: #55ACEE">ILogistics</span>
					</h1>
				</div>
			</div>
		</div>
		<div id="login_form">
			<form action="controller" method="post" name="login_form">
				<input type="hidden" name="command" value="login" />
				<div class="login_field">
					<label for="login"> <fmt:message key="common.login.label" bundle="${loc}" />
					</label>
					<input type="text" id="login" name="login" pattern="${login_pattern }" placeholder=<fmt:message key="common.login.placeholder" bundle="${loc}"/>
						title="${login_title }"
					>
				</div>
				<div class="login_field">
					<label for="password"> <fmt:message key="common.password.label" bundle="${loc}" />
					</label>
					<input type="password" id="password" name="password" pattern="${password_pattern }"
						title="${password_title }"
						placeholder=<fmt:message key="common.password.placeholder" bundle="${loc}"/>
					>
				</div>
				<div class="login_button">
					<input type="submit" name="login_button" value=<fmt:message key="common.button.submit" bundle="${loc}"/>>
				</div>
			</form>
		</div>
	</div>
</body>
</html>
