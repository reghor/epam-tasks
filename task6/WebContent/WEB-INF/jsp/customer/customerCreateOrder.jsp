<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.customer" var="loc" />
<fmt:setBundle basename="regex" var="regex" />
<html>
<head>
<title></title>
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/css/main.css"
	rel="stylesheet" type="text/css">
<script type="text/javascript">
	var path = "${pageContext.request.contextPath}/controller";
	var defaultPickCountry = "<fmt:message key="list.pick.country" bundle="${loc}" />";
	var defaultPickRegion = "<fmt:message key="list.pick.region" bundle="${loc}" />";
	var defaultPickCity = "<fmt:message key="list.pick.city" bundle="${loc}" />";
</script>
<script type="text/javascript" charset="utf8"
	src="${pageContext.request.contextPath}/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" charset="utf8"
	src="${pageContext.request.contextPath}/js/cascade.js"></script>
<script lang="javascript">
	function submitform(id) {
		document.getElementById(id).submit();
	}
</script>
<fmt:message key="order.title.numberic" bundle="${loc}"
	var="title_numberic" />
<fmt:message key="order.description" bundle="${regex}"
	var="description_pattern" />
</head>
<body>
	<div id="main-page">
		<div id="header">
			<div id="header_nav">
				<div id="header_nav_menu">
					<jsp:include page="customerHeaderNav.jsp"></jsp:include>
				</div>
			</div>
			<div id="header_content">
				<jsp:include page="customerHeaderLogo.jsp"></jsp:include>
			</div>
		</div>
		<div id="admin_content">
			<h1 class="header-of-fields">
				<fmt:message key="header.create.order" bundle="${loc}" />
			</h1>
			<div class="header-of-fields">
				<c:choose>
					<c:when test="${param.success=='true' }">
						<fmt:message key="header.order.added" bundle="${loc}" />
					</c:when>
					<c:when test="${param.success=='false'}">
						<fmt:message key="header.order.not.added" bundle="${loc}" />
					</c:when>
					<c:when test="${requestScope.wrong_input=='true'}">
						<fmt:message key="header.order.wrong.input" bundle="${loc}" />
					</c:when>
				</c:choose>
			</div>
			<form action="${pageContext.request.contextPath}/controller"
				method="POST">
				<input type="hidden" name="command" value="create_new_order">
				<table border="1">
					<tr>
						<th><fmt:message key="table.order.description"
								bundle="${loc}" /></th>
						<th><fmt:message key="table.order.max.load.height"
								bundle="${loc}" /></th>
						<th><fmt:message key="table.order.max.load.width"
								bundle="${loc}" /></th>
						<th><fmt:message key="table.order.max.load.length"
								bundle="${loc}" /></th>
						<th><fmt:message key="table.order.weight" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.volume" bundle="${loc}" /></th>
					</tr>
					<tr>
						<td><input type="text" name="description"
							pattern="${description_pattern }" name="description" required
							placeholder="<fmt:message key="order.placeholder.description" bundle="${loc}" />"
							title="<fmt:message key="order.title.description" bundle="${loc}" />"
							value="${param.description}" /></td>
						<td><input type="number" name="max_load_height" min="1"
							max="999" required value="${param.max_load_height}"
							placeholder="<fmt:message key="order.placeholder.height" bundle="${loc}" />"
							title="${title_numberic }" /></td>
						<td><input type="number" name="max_load_width" min="1"
							max="999" required value="${param.max_load_width}"
							placeholder="<fmt:message key="order.placeholder.width" bundle="${loc}" />"
							title="${title_numberic }" /></td>
						<td><input type="number" name="max_load_length" min="1"
							max="999" required value="${param.max_load_length}"
							placeholder="<fmt:message key="order.placeholder.length" bundle="${loc}" />"
							title="${title_numberic }" /></td>
						<td><input type="number" name="weight" min="1" max="999"
							required value="${param.weight}"
							placeholder="<fmt:message key="order.placeholder.weight" bundle="${loc}" />"
							title="${title_numberic }" /></td>
						<td><input type="number" name="volume" min="1" max="999"
							required value="${param.volume}"
							placeholder="<fmt:message key="order.placeholder.volume" bundle="${loc}" />"
							title="${title_numberic }" /></td>
					</tr>
					<tr>
						<td><fmt:message key="table.order.point.departure"
								bundle="${loc}" /></td>
						<td><select id="countries_departure"></select></td>
						<td><select id="regions_departure"></select></td>
						<td><select id="cities_departure" name="cities_departure"
							required><option value=""><fmt:message
										key="list.pick.city" bundle="${loc}" /></option></select></td>
					</tr>
					<tr>
						<td><fmt:message key="table.order.point.arrival"
								bundle="${loc}" /></td>
						<td><select id="countries_arrival"></select></td>
						<td><select id="regions_arrival"></select></td>
						<td><select id="cities_arrival" name="cities_arrival"
							required><option value=""><fmt:message
										key="list.pick.city" bundle="${loc}" /></option></select></td>
						<td><input type="submit"
							value="<fmt:message key="table.create.order.button" bundle="${loc }"/>"
							class="submit-button" /></td>
					</tr>
				</table>
			</form>
		</div>
		<div class="clr"></div>
	</div>
</body>
</html>