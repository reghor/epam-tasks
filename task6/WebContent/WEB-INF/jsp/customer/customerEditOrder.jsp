<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.customer" var="loc" />
<html>
<head>
<title></title>
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
	var path = "${pageContext.request.contextPath}/controller";
	var defaultPickCountry = "<fmt:message key="list.pick.country" bundle="${loc}" />";
	var defaultPickRegion = "<fmt:message key="list.pick.region" bundle="${loc}" />";
	var defaultPickCity = "<fmt:message key="list.pick.city" bundle="${loc}" />";
</script>
<script type="text/javascript" charset="utf8" src="${pageContext.request.contextPath}/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" charset="utf8" src="${pageContext.request.contextPath}/js/cascadeCustomerReview.js"></script>
<script type="text/javascript"></script>
<script lang="javascript">
	function submitform(id) {
		document.getElementById(id).submit();
	}
</script>
<fmt:message key="order.title.numberic" bundle="${loc}" var="title_numberic" />
</head>
<body>
	<div id="main-page">
		<div id="header">
			<div id="header">
				<div id="header_nav">
					<div id="header_nav_menu">
						<jsp:include page="customerHeaderNav.jsp"></jsp:include>
					</div>
				</div>
				<div id="header_content">
					<jsp:include page="customerHeaderLogo.jsp"></jsp:include>
				</div>
			</div>
			<div id="admin_content">
				
				<c:if test="${reviewed_order.order.statusRepr=='registered' }">
				<h1 class="header-of-fields">
					<fmt:message key="header.edit.order" bundle="${loc}" />
				</h1>
				<form action="controller" method="POST">
					<input type="hidden" name="command" value="edit_order">
					<input type="hidden" name="order_id" value="${reviewed_order.order.id}" />
					<table border="1">
						<tr>
							<th><fmt:message key="table.order.description" bundle="${loc}" /></th>
							<th><fmt:message key="table.order.max.load.height" bundle="${loc}" /></th>
							<th><fmt:message key="table.order.max.load.width" bundle="${loc}" /></th>
							<th><fmt:message key="table.order.max.load.length" bundle="${loc}" /></th>
							<th><fmt:message key="table.order.weight" bundle="${loc}" /></th>
							<th><fmt:message key="table.order.volume" bundle="${loc}" /></th>
						</tr>
						<tr>
							<td><input type="text" name="description" pattern=".{5,50}" name="description" required title="5 to 50 characters"
									title="<fmt:message key="order.title.description" bundle="${loc}" />" value="${reviewed_order.order.description}"
								/></td>
							<td><input type="number" name="max_load_height" min="1" max="999" value="${reviewed_order.order.maxLoadHeight}" title="${title_numberic }" required /></td>
							<td><input type="number" name="max_load_width" min="1" max="999" value="${reviewed_order.order.maxLoadHeight}" title="${title_numberic }" required /></td>
							<td><input type="number" name="max_load_length" min="1" max="999" value="${reviewed_order.order.maxLoadWidth}" title="${title_numberic }" required /></td>
							<td><input type="number" name="weight" min="1" max="999" value="${reviewed_order.order.weight}" title="${title_numberic }" required /></td>
							<td><input type="number" name="volume" min="1" max="999" value="${reviewed_order.order.volume}" title="${title_numberic }" required /></td>
						</tr>
						<tr>
							<td><fmt:message key="table.order.point.departure" bundle="${loc}" /></td>
							<td><select id="countries_departure"></select></td>
							<td><select id="regions_departure"></select></td>
							<td><select id="cities_departure" name="cities_departure" required>
									<option value="${reviewed_order.order.departurePointId}">${reviewed_order.order.departurePoint}</option>
									<option value=""><fmt:message key="list.pick.city" bundle="${loc}" /></option>
							</select></td>
						</tr>
						<tr>
							<td><fmt:message key="table.order.point.arrival" bundle="${loc}" /></td>
							<td><select id="countries_arrival"></select></td>
							<td><select id="regions_arrival"></select></td>
							<td><select id="cities_arrival" name="cities_arrival" required>
									<option value="${reviewed_order.order.arrivalPointId}">${reviewed_order.order.arrivalPoint}</option>
									<option value=""><fmt:message key="list.pick.city" bundle="${loc}" /></option>
							</select></td>
							<td><input type="submit" value="<fmt:message key="action.edit" bundle="${loc}" />" class="submit-button" /></td>
						</tr>
					</table>
				</form>
				</c:if>
			</div>
			<div class="clr"></div>
		</div>
	</div>
</body>
</html>