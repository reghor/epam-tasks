<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.customer" var="loc" />
<html>
<head>
<title></title>
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/css/main.css"
	rel="stylesheet" type="text/css">
<script type="text/javascript">
	var path = "${pageContext.request.contextPath}/controller";
	var defaultPickCountry = "<fmt:message key="list.pick.country" bundle="${loc}" />";
	var defaultPickRegion = "<fmt:message key="list.pick.region" bundle="${loc}" />";
	var defaultPickCity = "<fmt:message key="list.pick.city" bundle="${loc}" />";
</script>
<script type="text/javascript" charset="utf8"
	src="${pageContext.request.contextPath}/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" charset="utf8"
	src="${pageContext.request.contextPath}/js/cascadeCustomerReview.js"></script>
<script type="text/javascript"></script>
<script lang="javascript">
	function submitform(id) {
		document.getElementById(id).submit();
	}
</script>
<fmt:message key="order.title.numberic" bundle="${loc}"
	var="title_numberic" />
</head>
<body>
	<div id="main-page">
		<div id="header">
			<div id="header">
				<div id="header_nav">
					<div id="header_nav_menu">
						<jsp:include page="customerHeaderNav.jsp"></jsp:include>
					</div>
				</div>
				<div id="header_content">
					<jsp:include page="customerHeaderLogo.jsp"></jsp:include>
				</div>
			</div>
			<div id="admin_content">
				<h1 class="header-of-fields">
					<fmt:message key="header.details.order" bundle="${loc}" />
				</h1>
				<table border="1">
					<tr>
						<th>
							<fmt:message key="table.order.id" bundle="${loc}" />
						</th>
						<th>
							<fmt:message key="table.order.description" bundle="${loc}" />
						</th>
						<th>
							<fmt:message key="table.order.max.load.height" bundle="${loc}" />
						</th>
						<th>
							<fmt:message key="table.order.max.load.width" bundle="${loc}" />
						</th>
						<th>
							<fmt:message key="table.order.max.load.length" bundle="${loc}" />
						</th>
						<th>
							<fmt:message key="table.order.weight" bundle="${loc}" />
						</th>
						<th>
							<fmt:message key="table.order.volume" bundle="${loc}" />
						</th>
					</tr>
					<tr>
						<td>${reviewed_order.order.id}</td>
						<td>${reviewed_order.order.description}</td>
						<td>${reviewed_order.order.maxLoadHeight}</td>
						<td>${reviewed_order.order.maxLoadWidth}</td>
						<td>${reviewed_order.order.maxLoadLength}</td>
						<td>${reviewed_order.order.weight}</td>
						<td>${reviewed_order.order.volume}</td>
					</tr>
					<tr>
						<th>
							<fmt:message key="table.order.point.arrival" bundle="${loc}" />
						</th>
						<th>
							<fmt:message key="table.order.point.departure" bundle="${loc}" />
						</th>
						<th>
							<fmt:message key="table.order.region.arrival" bundle="${loc}" />
						</th>
						<th>
							<fmt:message key="table.order.region.departure" bundle="${loc}" />
						</th>
						<th>
							<fmt:message key="table.order.country.arrival" bundle="${loc}" />
						</th>
						<th>
							<fmt:message key="table.order.country.departure" bundle="${loc}" />
						</th>
						<th>
							<fmt:message key="table.order.status" bundle="${loc}" />
						</th>
					</tr>
					<tr>
						<td>${reviewed_order.order.arrivalPoint}</td>
						<td>${reviewed_order.order.departurePoint}</td>
						<td>${reviewed_order.order.arrivalRegion}</td>
						<td>${reviewed_order.order.departureRegion}</td>
						<td>${reviewed_order.order.arrivalCountry}</td>
						<td>${reviewed_order.order.departureCountry}</td>
						<td>${reviewed_order.order.statusRepr}</td>
					</tr>

				</table>
				<table>
					<tr>
						<td>
							<c:if
								test="${reviewed_order.order.statusRepr!='assigned' && reviewed_order.order.statusRepr!='accepted' }">
								<form action="controller" method="POST" class="order_id_form">
									<input type="hidden" name="order_id"
										value="${reviewed_order.order.id}">
									<input type="hidden" name="command" value="delete_order">
									<input type="submit"
										value="<fmt:message key="action.delete" bundle="${loc}" />">
								</form>
							</c:if>
						</td>
						<td>
							<c:if test="${reviewed_order.order.statusRepr=='registered' }">
								<form>
									<input type="hidden" name="command" value="edit_order_page" />
									<input type="hidden" name="order_id"
										value="${reviewed_order.order.id}">
									<input type="submit"
										value="<fmt:message key="action.edit" bundle="${loc}" />"
										class="submit-button" />
								</form>
							</c:if>
						</td>
					</tr>
				</table>
			</div>

			<div class="clr"></div>
		</div>
	</div>
</body>
</html>