<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<c:if test="${not empty pageContext.request.queryString}">
	<c:set var="lastQueryString" scope="session" value="${pageContext.request.queryString}" />
</c:if>
<title></title>
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.common" var="loc" />
<fmt:message key="common.register.password.alert" bundle="${loc}" var="password_alert" />
<fmt:message key="common.register.name.first" bundle="${loc}" var="first_name" />
<fmt:message key="common.register.name.last" bundle="${loc}" var="last_name" />
<fmt:message key="common.register.login" bundle="${loc}" var="login" />
<fmt:message key="common.register.number.phone" bundle="${loc}" var="phone_number" />
<fmt:message key="common.register.email" bundle="${loc}" var="email" />
<fmt:message key="common.register.password" bundle="${loc}" var="password" />
<fmt:message key="common.register.password.confirmation" bundle="${loc}" var="confirm_password" />
<fmt:message key="common.page.register" bundle="${loc}" var="register" />
<fmt:message key="common.page.login" bundle="${loc}" var="logn" />
<fmt:message key="common.register.title.name.first" bundle="${loc}" var="first_name_title" />
<fmt:message key="common.register.title.name.last" bundle="${loc}" var="last_name_title" />
<fmt:message key="common.register.title.login" bundle="${loc}" var="login_title" />
<fmt:message key="common.register.title.password" bundle="${loc}" var="password_title" />
<fmt:message key="common.register.title.password.confirmation" bundle="${loc}" var="password_confirmation_title" />
<fmt:message key="common.register.title.number.phone" bundle="${loc}" var="phone_number_title" />
<fmt:message key="common.register.title.email" bundle="${loc}" var="email_title" />
<fmt:message key="common.register.error.input.uniqueness" bundle="${loc }" var="input_uniqueness" />
<fmt:setBundle basename="regex" var="regex" />
<fmt:message key="name.first" bundle="${regex}" var="first_name_pattern" />
<fmt:message key="name.last" bundle="${regex}" var="last_name_pattern" />
<fmt:message key="email" bundle="${regex}" var="email_pattern" />
<fmt:message key="phone.number" bundle="${regex}" var="phone_number_pattern" />
<fmt:message key="password" bundle="${regex}" var="password_pattern" />
<fmt:message key="login" bundle="${regex}" var="login_pattern" />
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet" type="text/css">
<script lang="javascript">
	function submitform(id) {
		document.getElementById(id).submit();
	}
</script>
<script lang="javascript">
	function checkPasswords(form) {
		if (form.password.value != form.confirm_password.value) {
			alert('${password_alert}');
			return false;
		} else {
			return true;
		}
	}
</script>
</head>
<body>
	<div id="main-page">
		<div id="header">
			<div id="header_nav">
				<div id="header_nav_menu">
					<jsp:include page="guestHeaderNav.jsp" />
				</div>
			</div>
			<div id="header_content">
				<div id="header_logo" class="f_left">
					<h1>
						<span style="color: #55ACEE">ILogistics</span>
					</h1>
				</div>
			</div>
			<div class="header_sep"></div>
		</div>
		<div id="content">
			<h1 class="header-of-fields">
				<fmt:message key="header.welcome.message" bundle="${loc }"/>
			</h1>
		</div>
	</div>
	<div class="clr"></div>
</body>
</html>