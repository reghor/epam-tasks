<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.dispatcher" var="loc" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<c:if test="${not empty pageContext.request.queryString}">
	<c:set var="lastQueryString" scope="session" value="${pageContext.request.queryString}" />
</c:if>
<title>Insert title here</title>
</head>
<body>
	<ul>
		<li>
			<form action="${pageContext.request.contextPath}/controller" name="show_all_orders_dispatcher" id="show_all_orders_dispatcher" method="get">
				<input type="hidden" name="command" value="show_all_orders_dispatcher" />
				<a href="javascript: submitform('show_all_orders_dispatcher')">
					<fmt:message key="main.order.all" bundle="${loc}" />
				</a>
			</form>
		</li>
		<li>
			<form action="${pageContext.request.contextPath}/controller" name="get_all_cars" id="get_all_cars" method="get">
				<input type="hidden" name="command" value="get_all_cars" />
				<a href="javascript: submitform('get_all_cars')">
					<fmt:message key="main.car.all" bundle="${loc}" />
				</a>
			</form>
		</li>
		<li>
			<form action="${pageContext.request.contextPath}/controller" name="add_new_car" id="add_new_car" method="get">
				<input type="hidden" name="command" value="add_new_car_page_reference" />
				<a href="javascript: submitform('add_new_car')">
					<fmt:message key="main.car.add" bundle="${loc}" />
				</a>
			</form>
		</li>
		<li>
			<form action="${pageContext.request.contextPath}/controller" name="get_all_drivers" id="get_all_drivers" method="get">
				<input type="hidden" name="command" value="get_all_drivers" />
				<a href="javascript: submitform('get_all_drivers')">
					<fmt:message key="main.driver.all" bundle="${loc}" />
				</a>
			</form>
		</li>
		<li>
			<form action="${pageContext.request.contextPath}/controller" name="register_driver_page_reference" id="register_driver_page_reference" method="get">
				<input type="hidden" name="command" value="register_driver_page_reference" />
				<a href="javascript: submitform('register_driver_page_reference')">
					<fmt:message key="main.driver.add" bundle="${loc}" />
				</a>
			</form>
		</li>
		<li>
			<form action="${pageContext.request.contextPath}/controller" name="logout" id="logout" method="get">
				<input type="hidden" name="command" value="logout" />
				<a href="javascript: submitform('logout')">
					<fmt:message key="main.logout" bundle="${loc}" />
				</a>
			</form>
		</li>
	</ul>
	<div id="header_languages">
		<ul>
			<li>
				<form name="ru_button" id="ru_button" action="${pageContext.request.contextPath}/controller" method="post">
					<input type="hidden" name="local" value="ru" />
					<input type="hidden" name="command" value="localization" />
					<a href="javascript: submitform('ru_button')">
						<img src="${pageContext.request.contextPath}/images/ru_button.png">
					</a>
				</form>
			</li>
			<li>
				<form name="en_button" id="en_button" action="${pageContext.request.contextPath}/controller" method="post">
					<input type="hidden" name="local" value="en" />
					<input type="hidden" name="command" value="localization" />
					<a href="javascript: submitform('en_button')">
						<img src="${pageContext.request.contextPath}/images/en_button.png">
					</a>
				</form>
			</li>
		</ul>
	</div>
	<div id="greeting">
		<fmt:message key="main.greeting" bundle="${loc}" />
		, ${user.firstName}!
	</div>
</body>
</html>