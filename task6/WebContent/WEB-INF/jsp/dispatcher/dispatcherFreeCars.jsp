<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.dispatcher" var="loc" />
<html>
<head>
<title></title>
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet" type="text/css">
<script lang="javascript">
	function submitform(id) {
		document.getElementById(id).submit();
	}
</script>
</head>
<body>
	<div id="main-page">
		<div id="header">
			<div id="header_nav">
				<div id="header_nav_menu">
					<jsp:include page="dispatcherHeaderNav.jsp" />
				</div>
			</div>
			<jsp:include page="dispatcherHeaderLogo.jsp" />
			<div id="admin_content">
				<c:choose>
					<c:when test="${not empty free_cars }">
						<h1 class="header-of-fields">
							<fmt:message key="header.cars.all" bundle="${loc}" />
						</h1>
						<table border="1" style="text-align: center;">
							<tr>
								<th><fmt:message key="table.car.id" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.vehicle.id" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.make.of.car" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.year" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.colour" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.height.max" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.length.max" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.width.max" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.volume" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.weight.max" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.condition" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.status" bundle="${loc}" /></th>
							</tr>
							<c:forEach items="${free_cars}" var="car">
								<form action="${pageContext.request.contextPath}/controller" id="add_car" method="get">
									<tr>
										<td>${car.id}</td>
										<td>${car.vehicleIdNumber}</td>
										<td>${car.makeOfCar}</td>
										<td>${car.year}</td>
										<td>${car.colour}</td>
										<td>${car.maxHeight}</td>
										<td>${car.maxLength}</td>
										<td>${car.maxWidth}</td>
										<td>${car.maxVolume}</td>
										<td>${car.maxWeight}</td>
										<td>${car.carCondition}</td>
										<td>${car.status}</td>
										<td><input type="hidden" name="command" value="assign_driver_car" /> <input type="hidden" name="car_id" value="${car.id}" /> <input
												type="submit" value="<fmt:message key="driver.action.assign.car" bundle="${loc}"/>"
											/> <input type="hidden" name="driver_id" value="${driver_id }" /></td>
									</tr>
								</form>
							</c:forEach>
						</table>
					</c:when>
					<c:otherwise>
						<h1 class="header-of-fields">
							<fmt:message key="header.no.free.cars" bundle="${loc}" />
						</h1>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="clr"></div>
		</div>
	</div>
</body>
</html>