<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.dispatcher" var="loc" />
<html>
<head>
<title></title>
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet" type="text/css">
<script lang="javascript">
	function submitform(id) {
		document.getElementById(id).submit();
	}
</script>
</head>
<body>
	<div id="main-page">
		<div id="header">
			<div id="header_nav">
				<div id="header_nav_menu">
					<jsp:include page="dispatcherHeaderNav.jsp"/>
				</div>
				
		</div>
<jsp:include page="dispatcherHeaderLogo.jsp"/>
			<div id="admin_content">
			<h1 class="header-of-fields"><fmt:message key="header.all.drivers" bundle="${loc}" /></h1>
			<c:choose>
					<c:when test="${param.success=='true'}">
						<div class="header-of-fields">
							<fmt:message key="driver.car.assigned" bundle="${loc}" />
						</div>
					</c:when>
					<c:when test="${param.success=='false'}">
						<div class="header-of-fields">
							<fmt:message key="driver.car.not.assigned" bundle="${loc}" />
						</div>
					</c:when>
				</c:choose>
			<c:choose>
					<c:when test="${not empty all_drivers }">
				<table border="1" >
					<tr>
						<th><fmt:message key="table.car.id" bundle="${loc}" /></th>
						<th><fmt:message key="table.user.first.name" bundle="${loc}" /></th>
						<th><fmt:message key="table.user.last.name" bundle="${loc}" /></th>
						<th><fmt:message key="table.user.phone.number" bundle="${loc}" /></th>
						<th><fmt:message key="table.user.email" bundle="${loc}" /></th>



					</tr>
					
					<c:forEach items="${all_drivers}" var="driver">
						<tr>
							<td>${driver.id}</td>
							<td>${driver.firstName}</td>
							<td>${driver.lastName}</td>
							<td>${driver.phoneNumber}</td>
							<td>${driver.email}</td>
							<td><form action="${pageContext.request.contextPath}/controller"  method="get">
									<input type="hidden" name="driver_id" value="${driver.id}" /> 
									<input type="hidden" name="command" value="get_driver_car" /> 
									<input	type="submit" value="<fmt:message key="driver.action.show.car" bundle="${loc}"/>" />
								</form></td>
						</tr>
					</c:forEach>
					
				</table>
				</c:when>
				<c:otherwise>
				<h1 class="header-of-fiels"><fmt:message key="table.car.heightion" bundle="${loc}" /></h1>
				</c:otherwise>
					</c:choose>
			</div>
			<div class="clr"></div>

		</div>
	</div>
</body>
</html>