<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.dispatcher" var="loc" />
<html>
<head>
<title></title>
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet" type="text/css">
<script lang="javascript">
	function submitform(id) {
		document.getElementById(id).submit();
	}
</script>
</head>
<body>
	<div id="main-page">
		<div id="header">
			<div id="header_nav">
				<div id="header_nav_menu">
					<jsp:include page="dispatcherHeaderNav.jsp" />
				</div>
			</div>
			<jsp:include page="dispatcherHeaderLogo.jsp" />
			<c:choose>
				<c:when test="${(empty suitable_cars_list) }">
					<h1 class="header-of-fields">
						<fmt:message key="header.no.suitable.cars" bundle="${loc}" />
					</h1>
				</c:when>
				<c:otherwise>
					<h1 class="header-of-fields">
						<fmt:message key="header.pick.a.car" bundle="${loc}" />
					</h1>
					<br>
					<table border="1">
						<tr>
							<th><fmt:message key="table.car.id" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.vehicle.id" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.make.of.car" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.year" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.colour" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.condition" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.height.max" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.length.max" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.width.max" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.volume" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.weight.max" bundle="${loc}" /></th>
						</tr>
						<c:forEach var="car" items="${suitable_cars_list }">
							<tr>
								<td>${car.id}</td>
								<td>${car.vehicleIdNumber}</td>
								<td>${car.makeOfCar}</td>
								<td>${car.year}</td>
								<td>${car.colour}</td>
								<td>${car.carCondition}</td>
								<td>${car.maxHeight}</td>
								<td>${car.maxLength}</td>
								<td>${car.maxWidth}</td>
								<td>${car.maxVolume}</td>
								<td>${car.maxWeight}</td>
								<td><form action="${pageContext.request.contextPath}/controller" name="assign_order" id="${car.vehicleIdNumber}" method="post">
										<input type="hidden" name="command" value="assign_order_car_pick" />
										<input type="hidden" name="car_id" value="${car.id}" />
										<input type="hidden" name="order_id" value="${order_id}">
										<a href="javascript: submitform('${car.vehicleIdNumber}')">
											<fmt:message key="table.action.assignment.perform" bundle="${loc}" />
										</a>
									</form></td>
							</tr>
						</c:forEach>
					</table>
				</c:otherwise>
			</c:choose>
		</div>
		<div class="clr"></div>
	</div>
</body>
</html>