<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.dispatcher" var="loc" />
<html>
<head>
<title></title>
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet" type="text/css">
<script lang="javascript">
	function submitform(id) {
		document.getElementById(id).submit();
	}
</script>
</head>
<body>
	<div id="main-page">
		<div id="header">
			<div id="header_nav">
				<div id="header_nav_menu">
					<jsp:include page="dispatcherHeaderNav.jsp" />
				</div>
			</div>
			<jsp:include page="dispatcherHeaderLogo.jsp" />
			<div id="admin_content">
				<h1 class="header-of-fields">
					<fmt:message key="header.details.order" bundle="${loc}" />
				</h1>
				<table border="1">
					<tr>
						<th><fmt:message key="table.order.id" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.description" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.max.load.height" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.max.load.width" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.max.load.length" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.weight" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.volume" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.point.arrival" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.point.departure" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.status" bundle="${loc}" /></th>
					</tr>
					<tr>
						<td>${reviewed_order.order.id}</td>
						<td>${reviewed_order.order.description}</td>
						<td>${reviewed_order.order.maxLoadHeight}</td>
						<td>${reviewed_order.order.maxLoadWidth}</td>
						<td>${reviewed_order.order.maxLoadLength}</td>
						<td>${reviewed_order.order.weight}</td>
						<td>${reviewed_order.order.volume}</td>
						<td>${reviewed_order.order.arrivalPoint}</td>
						<td>${reviewed_order.order.departurePoint}</td>
						<td>${reviewed_order.order.status}</td>
					</tr>
				</table>
				<c:if test="${reviewed_order.order.status.lowerCaseValue!='registered' && reviewed_order.order.status.lowerCaseValue!='declined'}">
					<h1 class="header-of-fields">
						<fmt:message key="header.details.driver" bundle="${loc}" />
					</h1>
					<br>
					<table border="1">
						<tr>
							<th><fmt:message key="table.driver.name.first" bundle="${loc}" /></th>
							<th><fmt:message key="table.driver.name.last" bundle="${loc}" /></th>
							<th><fmt:message key="table.driver.phonenumber" bundle="${loc}" /></th>
						</tr>
						<tr>
							<td>${reviewed_order.driver.firstName}</td>
							<td>${reviewed_order.driver.lastName}</td>
							<td>${reviewed_order.driver.phoneNumber}</td>
						</tr>
					</table>
					<br>
					<h1 class="header-of-fields">
						<fmt:message key="header.details.car" bundle="${loc}" />
					</h1>
					<br>
					<table border="1">
						<tr>
							<th><fmt:message key="table.car.id" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.vehicle.id" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.make.of.car" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.year" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.colour" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.height.max" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.length.max" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.width.max" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.volume" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.weight.max" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.condition" bundle="${loc}" /></th>
						</tr>
						<tr>
							<td>${reviewed_order.car.id}</td>
							<td>${reviewed_order.car.vehicleIdNumber}</td>
							<td>${reviewed_order.car.makeOfCar}</td>
							<td>${reviewed_order.car.year}</td>
							<td>${reviewed_order.car.colour}</td>
							<td>${reviewed_order.car.maxHeight}</td>
							<td>${reviewed_order.car.maxLength}</td>
							<td>${reviewed_order.car.maxWidth}</td>
							<td>${reviewed_order.car.maxVolume}</td>
							<td>${reviewed_order.car.maxWeight}</td>
							<td>${reviewed_order.car.carCondition}</td>
						</tr>
					</table>
				</c:if>
				<c:choose>
					<c:when test="${reviewed_order.order.status.lowerCaseValue=='registered' }">
						<br>
						<h1 class="header-of-fields">
							<fmt:message key="header.order.actions" bundle="${loc}" />
						</h1>
						<br>
						<table border="1">
							<tr>
								<td><form action="${pageContext.request.contextPath}/controller" name="assign_order" method="get">
										<input type="hidden" name="command" value="assign_order_car" />
										<input type="hidden" name="order_id" value="${reviewed_order.order.id}" />
										<input type="hidden" name="max_load_height" value="${reviewed_order.order.maxLoadHeight}" />
										<input type="hidden" name="max_load_width" value="${reviewed_order.order.maxLoadWidth}" />
										<input type="hidden" name="max_load_length" value="${reviewed_order.order.maxLoadLength}" />
										<input type="hidden" name="volume" value="${reviewed_order.order.volume}" />
										<input type="hidden" name="weight" value="${reviewed_order.order.weight}" />
										<input type="submit" value="<fmt:message key="table.action.assignment.perform" bundle="${loc}" />" />
									</form></td>
								<td><form action="${pageContext.request.contextPath}/controller" name="assign_order" id="assign_order" method="get">
										<input type="hidden" name="command" value="decline_order" />
										<input type="hidden" name="order_id" value="${reviewed_order.order.id}" />
										<input type="submit" value="<fmt:message key="table.action.order.decline" bundle="${loc}" />" />
									</form></td>
							<tr />
						</table>
					</c:when>
					<c:when test="${reviewed_order.order.status.lowerCaseValue=='assigned' }">
						<br>
						<h1 class="header-of-fields">
							<fmt:message key="header.order.actions" bundle="${loc}" />
						</h1>
						<br>
						<table border="1">
							<tr>
								<td><form action="${pageContext.request.contextPath}/controller" name="cancel_assignment" method="post">
										<input type="hidden" name="command" value="cancel_assignment" />
										<input type="hidden" name="order_id" value="${reviewed_order.order.id}" />
										<input type="submit" value="<fmt:message key="table.action.assignment.cancel" bundle="${loc}" />" />
									</form></td>
							<tr />
						</table>
					</c:when>
				</c:choose>
			</div>
			<div class="clr"></div>
		</div>
	</div>
</body>
</html>