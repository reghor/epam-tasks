<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.dispatcher" var="loc" />
<html>
<head>
<title></title>
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet" type="text/css">
<script lang="javascript">
	function submitform(id) {
		document.getElementById(id).submit();
	}
</script>
</head>
<body>
	<div id="main-page">
		<div id="header">
			<div id="header_nav">
				<div id="header_nav_menu">
					<jsp:include page="dispatcherHeaderNav.jsp" />
				</div>
			</div>
			<jsp:include page="dispatcherHeaderLogo.jsp" />
			<div id="admin_content">
				<c:choose>
					<c:when test="${not empty driver_car}">
						<h1 class="header-of-fields">
							<fmt:message key="header.details.driver.car" bundle="${loc}" />
						</h1>
						<table border="1" style="text-align: center;">
							<tr>
								<th><fmt:message key="table.car.id" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.vehicle.id" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.make.of.car" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.year" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.colour" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.height.max" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.length.max" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.width.max" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.volume" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.weight.max" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.condition" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.status" bundle="${loc}" /></th>
							</tr>
							<tr>
								<td>${driver_car.id}</td>
								<td>${driver_car.vehicleIdNumber}</td>
								<td>${driver_car.makeOfCar}</td>
								<td>${driver_car.year}</td>
								<td>${driver_car.colour}</td>
								<td>${driver_car.maxHeight}</td>
								<td>${driver_car.maxLength}</td>
								<td>${driver_car.maxWidth}</td>
								<td>${driver_car.maxVolume}</td>
								<td>${driver_car.maxWeight}</td>
								<td>${driver_car.carCondition}</td>
								<td>${driver_car.status}</td>
								<c:if test="${driver_car.status.lowerCaseValue=='free'}">
									<td><form action="${pageContext.request.contextPath}/controller" id="cancel_car_assignment" method="get">
											<input type="hidden" name="driver_id" value="${driver_id}" />
											<input type="hidden" name="command" value="cancel_car_assignment" />
											<input type="submit" value="<fmt:message key="table.action.assignment.cancel" bundle="${loc}"/>" />
										</form></td>
								</c:if>
							</tr>
						</table>
					</c:when>
					<c:otherwise>
						<div class="header-of-fields">
							<fmt:message key="header.no.assigned.car" bundle="${loc}" />
						</div>
						<form action="${pageContext.request.contextPath}/controller" id="delete_car" class="header-of-fields" method="get">
							<input type="hidden" name="driver_id" value="${driver_id}" />
							<input type="hidden" name="command" value="get_free_cars" />
							<input type="submit" value="<fmt:message key="driver.action.assign.car" bundle="${loc}"/>" />
						</form>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="clr"></div>
		</div>
	</div>
</body>
</html>