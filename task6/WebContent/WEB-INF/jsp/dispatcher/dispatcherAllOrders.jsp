<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.dispatcher" var="loc" />
<html>
<head>
<title></title>
<meta charset="UTF-8">
<script type="text/javascript">
	var path = "${pageContext.request.contextPath}/controller";
	var dataTablesLocale = "<fmt:message key="datatables.path" bundle="${loc}" />";
</script>
<link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet" type="text/css">
<script type="text/javascript" charset="utf8" src="${pageContext.request.contextPath}/js/jquery-1.11.2.min.js"></script>
<link href="${pageContext.request.contextPath}/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
<script type="text/javascript" charset="utf8" src="${pageContext.request.contextPath}/js/jquery.dataTables.min.10.6.js"></script>
<script type="text/javascript" charset="utf8" src="${pageContext.request.contextPath}/js/table_all_orders.js">
	
</script>
<script lang="javascript">
	function submitform(id) {
		document.getElementById(id).submit();
	}
</script>
</head>
<body>
	<div id="main-page">
		<div id="header">
			<div id="header_nav">
				<div id="header_nav_menu">
					<jsp:include page="dispatcherHeaderNav.jsp" />
				</div>
			</div>
			<jsp:include page="dispatcherHeaderLogo.jsp" />
			<div class="header-of-fields">
			<c:choose>
				<c:when test="${param.no_such_order=='true' }">
					<fmt:message key="header.no.such.order" bundle="${loc}" />
					<br>
					<br>
				</c:when>
				<c:when test="${param.no_such_car=='true' }">
					<fmt:message key="header.no.such.car" bundle="${loc}" />
					<br>
					<br>
				</c:when>
			</c:choose>
			</div>
			<form action="${pageContext.request.contextPath}/controller" class="order_id_form">
				<input type="hidden" name="command" value="review_order">
				<input type="hidden" name="order_id" value="" class="order_id">
				<input type="submit" value="<fmt:message key="table.action.view" bundle="${loc}" />" class="submit_review">
			</form>
			<table id="table_id" class="display">
				<thead>
					<tr>
						<th><fmt:message key="table.order.id" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.description" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.max.load.height" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.max.load.width" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.max.load.length" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.weight" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.volume" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.point.arrival" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.point.departure" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.status" bundle="${loc}" /></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			<form action="${pageContext.request.contextPath}/controller" class="order_id_form">
				<input type="hidden" name="command" value="review_order">
				<input type="hidden" name="order_id" value="" class="order_id">
				<input type="submit" value="<fmt:message key="table.action.view" bundle="${loc}" />" class="submit_review">
			</form>
		</div>
		<div class="clr"></div>
	</div>
</body>
</html>