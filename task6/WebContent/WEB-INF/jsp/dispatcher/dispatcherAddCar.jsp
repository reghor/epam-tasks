<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.dispatcher" var="loc" />
<fmt:setBundle basename="regex" var="regex" />
<fmt:message key="car.colour" var="colour_pattern" bundle="${regex }" />
<fmt:message key="car.id" var="id_pattern" bundle="${regex }" />
<fmt:message key="car.make.of.car" var="make_of_car_pattern" bundle="${regex }" />
<html>
<head>
<title></title>
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet" type="text/css">
<script lang="javascript">
	function submitform(id) {
		document.getElementById(id).submit();
	}
</script>
<fmt:message key="title.car.add.number" bundle="${loc}" var="number_title" />
</head>
<body>
	<div id="main-page">
		<div id="header">
			<div id="header_nav">
				<div id="header_nav_menu">
					<jsp:include page="dispatcherHeaderNav.jsp" />
				</div>
			</div>
			<jsp:include page="dispatcherHeaderLogo.jsp" />
			<div id="admin_content">
			<div class="header-of-fields">
				<h1 >
					<fmt:message key="header.car.create" bundle="${loc}" />
				</h1>
				<c:choose>
					<c:when test="${param.success=='true'}">
						<b ><fmt:message key="header.car.added" bundle="${loc}" /></b>
					</c:when>
					<c:when test="${requestScope.success=='false'}">
						<b ><fmt:message key="header.car.not.added" bundle="${loc}" /></b>
					</c:when>
				</c:choose>
				</div>
				<form action="${pageContext.request.contextPath}/controller" id="add_car" method="post">
					<input type="hidden" name="command" value="add_new_car" />
					<table border="1" style="text-align: center;">
						<tr>
							<th><fmt:message key="table.car.vehicle.id" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.make.of.car" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.year" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.colour" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.height.max" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.length.max" bundle="${loc}" /></th>
						</tr>
						<tr>
							<td><input type="text" name="car_vehicle_id_number" required value="${param.car_vehicle_id_number}" pattern="${id_pattern }"
									title="<fmt:message key="title.car.add.id" bundle="${loc}" />"
								/></td>
							<td><input type="text" name="make_of_car" required value="${param.make_of_car}" pattern="${make_of_car_pattern }"
									title="<fmt:message key="title.car.add.make.of.car" bundle="${loc}" />"
								/></td>
							<td><input type="number" name="car_year" required value="${param.car_year}" min="1970" max="2015"
									title="<fmt:message key="title.car.add.year" bundle="${loc}" />"
								/></td>
							<td><input type="text" name="car_colour" required value="${param.car_colour}" pattern="${colour_pattern }"
									title="<fmt:message key="title.car.add.colour" bundle="${loc}" />"
								/></td>
							<td><input type="number" name="car_max_height" required value="${param.car_max_height}" min="1" max="999" title="${number_title }" /></td>
							<td><input type="number" name="car_max_length" required value="${param.car_max_length}" min="1" max="999" title="${number_title }" /></td>
						</tr>
						<tr>
							<th><fmt:message key="table.car.width.max" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.volume" bundle="${loc}" /></th>
							<th><fmt:message key="table.car.weight.max" bundle="${loc}" /></th>
						<tr>
							<td><input type="number" required name="car_max_width" value="${param.car_max_width}" min="1" max="999" title="${number_title }" /></td>
							<td><input type="number" required name="car_max_volume" value="${param.car_max_volume}" min="1" max="999" title="${number_title }" /></td>
							<td><input type="number" required name="car_max_weight" value="${param.car_max_weight}" min="1" max="999" title="${number_title }" /></td>
							<td><input type="submit" required value="<fmt:message key="table.action.create" bundle="${loc}"/>" /></td>
						</tr>
					</table>
				</form>
			</div>
			</div>
			<div class="clr"></div>
		</div>
</body>
</html>