<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>

<html>
<head>
<title></title>
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.common" var="loc" />
<fmt:message key="common.register.password.alert" bundle="${loc}" var="password_alert" />
<fmt:message key="common.register.name.first" bundle="${loc}" var="first_name" />
<fmt:message key="common.register.name.last" bundle="${loc}" var="last_name" />
<fmt:message key="common.register.login" bundle="${loc}" var="login" />
<fmt:message key="common.register.number.phone" bundle="${loc}" var="phone_number" />
<fmt:message key="common.register.email" bundle="${loc}" var="email" />
<fmt:message key="common.register.password" bundle="${loc}" var="password" />
<fmt:message key="common.register.password.confirmation" bundle="${loc}" var="confirm_password" />
<fmt:message key="common.page.register" bundle="${loc}" var="register" />
<fmt:message key="common.page.login" bundle="${loc}" var="logn" />
<fmt:message key="common.register.title.name.first" bundle="${loc}" var="first_name_title" />
<fmt:message key="common.register.title.name.last" bundle="${loc}" var="last_name_title" />
<fmt:message key="common.register.title.login" bundle="${loc}" var="login_title" />
<fmt:message key="common.register.title.password" bundle="${loc}" var="password_title" />
<fmt:message key="common.register.title.password.confirmation" bundle="${loc}" var="password_confirmation_title" />
<fmt:message key="common.register.title.number.phone" bundle="${loc}" var="phone_number_title" />
<fmt:message key="common.register.title.email" bundle="${loc}" var="email_title" />
<fmt:message key="common.register.error.input.uniqueness" bundle="${loc }" var="input_uniqueness" />

<fmt:setBundle basename="regex" var="regex" />
<fmt:message key="name.first" bundle="${regex}" var="first_name_pattern" />
<fmt:message key="name.last" bundle="${regex}" var="last_name_pattern" />
<fmt:message key="email" bundle="${regex}" var="email_pattern" />
<fmt:message key="phone.number" bundle="${regex}" var="phone_number_pattern" />
<fmt:message key="password" bundle="${regex}" var="password_pattern" />
<fmt:message key="login" bundle="${regex}" var="login_pattern" />

<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet" type="text/css">

<script lang="javascript">
	function submitform(id) {
		document.getElementById(id).submit();
	}
</script>
<script lang="javascript">
	function checkPasswords(form) {
		if (form.password.value != form.confirm_password.value) {
			alert('${password_alert}');
			return false;
		} else {
			return true;
		}
	}
</script>

</head>
<body>
	<div id="main-page">
		<div id="header">
			<div id="header_nav">
				<div id="header_nav_menu">
					<jsp:include page="dispatcherHeaderNav.jsp"/>
				</div>
				
		</div>
<jsp:include page="dispatcherHeaderLogo.jsp"/>
			<div class="header_sep"></div>
		</div>

		<div id="content">
			<div id="registration_form">
				<form action="controller" method="post" name="registration" onsubmit="return checkPasswords(this)">
					<input type="hidden" name="command" value="register_driver" />
					<b><fmt:message key="header.add.driver" bundle="${loc}"/></b>
					<div class="registration_field">
						<label for="first_name">${first_name}</label> <input type="text" id="first_name" name="first_name" value="${userFirstName}" placeholder="${first_name}"
							pattern="${first_name_pattern}" title="${last_name_title }" required />
					</div>
					<div class="registration_field">
						<label for="last_name">${last_name}</label> <input type="text" id="last_name" name="last_name" value="${userLastName}"
							placeholder="${last_name}" pattern="${last_name_pattern}" title="${last_name_title }')" required />
					</div>
					<div class="registration_field">
						<label for="login">${login}</label> <input type="text" id="login" name="login" value="${userLogin}" placeholder="${login}"
							pattern="${login_pattern}" title="${login_title}" required />
					</div>
					<div class="registration_field">
						<label for="phone_number">${phone_number}</label> <input type="text" id="phone_number" name="phone_number" value="${userPhoneNumber}"
							pattern="${phone_number_pattern}" placeholder="80XX-XXX-XX-XX" title="${phone_number_title}" required />
					</div>
					<div class="registration_field">
						<label for="email">${email}</label><input type="email" id="email" name="email" value="${userEmail}" placeholder="${email}"
							pattern="${email_pattern}" title="${email_title }" required />
					</div>
					<div class="registration_field">
						<label for="password">${password}</label> <input type="password" id="password" name="password" placeholder="${password}"
							pattern="${password_pattern}" title="${password_title }" required />
					</div>
					<div class="registration_field">
						<label for="confirm_password">${confirm_password}</label> <input type="password" id="confirm_password" name="confirm_password"
							pattern="${password_pattern}" placeholder="${confirm_password}" title="${password_confirmation_title }" required />
					</div>
					<div id="registration_button">
						<input type="submit" name="registration_button" value="${register }">
					</div>
				</form>
			</div>
			<c:if test="${not empty wrong_first_name }">
				<div id="registration_error" class="page_error">${first_name_title}</div>
			</c:if>
			<c:if test="${not empty wrong_last_name }">
				<div id="registration_error" class="page_error">${last_name_title}</div>
			</c:if>
			<c:if test="${not empty wrong_login}">
				<div id="registration_error" class="page_error">${login_title}</div>
			</c:if>
			<c:if test="${not empty wrong_email}">
				<div id="registration_error" class="page_error">${email_title}</div>
			</c:if>
			<c:if test="${not empty wrong_phone_number }">
				<div id="registration_error" class="page_error">${phone_number_title}</div>
			</c:if>
			<c:if test="${not empty wrong_password }">
				<div id="registration_error" class="page_error">$password_title}</div>
			</c:if>
			<c:if test="${not empty password_match_error }">
				<div id="registration_error" class="page_error">${password_alert}</div>
			</c:if>
			<c:if test="${not empty input_not_unique }">
			<div id="registration_error" class="page_error">${input_uniqueness}</div>
				
			</c:if>
		</div>
	</div>
	<div class="clr"></div>
</body>
</html>