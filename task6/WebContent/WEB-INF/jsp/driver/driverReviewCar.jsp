<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.driver" var="loc" />
<html>
<head>
<title></title>
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet" type="text/css">
<script lang="javascript">
	function submitform(id) {
		document.getElementById(id).submit();
	}
</script>
</head>
<body>
	<div id="main-page">
		<div id="header">
			<div id="header_nav">
				<div id="header_nav_menu">
					<jsp:include page="driverHeaderNav.jsp" />
				</div>
			</div>
			<jsp:include page="driverHeaderLogo.jsp" />
			<div id="admin_content">
				<div class="header-of-fields">
					<c:if test="${param.success=='false' }">
						<b class="header-of-fields"><fmt:message key="car.is.busy" bundle="${loc}" /></b>
					</c:if>
				</div>
				<c:choose>
					<c:when test="${not empty car }">
						<h1 class="header-of-fields">
							<fmt:message key="header.details.car" bundle="${loc}" />
						</h1>
						<br>
						<table border="1">
							<tr>
								<th><fmt:message key="table.car.id" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.vehicle.id" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.make.of.car" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.year" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.colour" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.condition" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.height.max" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.length.max" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.width.max" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.volume" bundle="${loc}" /></th>
								<th><fmt:message key="table.car.weight.max" bundle="${loc}" /></th>
							</tr>
							<tr>
								<td>${car.id}</td>
								<td>${car.vehicleIdNumber}</td>
								<td>${car.makeOfCar}</td>
								<td>${car.year}</td>
								<td>${car.colour}</td>
								<td>${car.carCondition}</td>
								<td>${car.maxHeight}</td>
								<td>${car.maxLength}</td>
								<td>${car.maxWidth}</td>
								<td>${car.maxVolume}</td>
								<td>${car.maxWeight}</td>
								<c:choose>
									<c:when test="${car.carConditionRepr=='faulty' }">
										<td><form action="${pageContext.request.contextPath}/controller" id="make_car_faultless" method="post">
												<input type="hidden" name="command" value="make_car_faultless" />
												<input type="hidden" name="car_id" value="${car.id}" />
												<input type="submit" value="<fmt:message key="car.action.condition.faultless" bundle="${loc}" />" />
											</form></td>
									</c:when>
									<c:otherwise>
										<td><form action="${pageContext.request.contextPath}/controller" id="make_car_faulty" method="post">
												<input type="hidden" name="command" value="make_car_faulty" />
												<input type="hidden" name="car_id" value="${car.id}" />
												<input type="submit" value="<fmt:message key="car.action.condition.faulty" bundle="${loc}" />" />
											</form></td>
									</c:otherwise>
								</c:choose>
							</tr>
						</table>
					</c:when>
					<c:otherwise>
						<h1 class="header-of-fields">
							<fmt:message key="header.car.empty" bundle="${loc}" />
						</h1>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="clr"></div>
		</div>
	</div>
</body>
</html>