<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.driver" var="loc" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<c:if test="${not empty pageContext.request.queryString}">
	<c:set var="lastQueryString" scope="session" value="${pageContext.request.queryString}" />
</c:if>
</head>
<body>
	<ul>
		<li>
			<form action="${pageContext.request.contextPath}/controller" name="get_driver_car" id="get_driver_car" method="get">
				<input type="hidden" name="command" value="get_driver_car" />
				<a href="javascript: submitform('get_driver_car')">
					<fmt:message key="main.car" bundle="${loc}" />
				</a>
			</form>
		</li>
		<li>
			<form action="${pageContext.request.contextPath}/controller" name="create_new_order_reference" id="create_new_order_reference" method="get">
				<input type="hidden" name="command" value="show_driver_order" />
				<a href="javascript: submitform('create_new_order_reference')">
					<fmt:message key="main.order" bundle="${loc}" />
				</a>
			</form>
		</li>
		<li>
			<form action="${pageContext.request.contextPath}/controller" name="logout" id="logout" method="post">
				<input type="hidden" name="command" value="logout" />
				<a href="javascript: submitform('logout')">
					<fmt:message key="main.logout" bundle="${loc}" />
				</a>
			</form>
		</li>
	</ul>
	<div id="header_languages">
		<ul>
			<li>
				<form name="ru_button" id="ru_button" action="${pageContext.request.contextPath}/controller" method="post">
					<input type="hidden" name="local" value="ru" />
					<input type="hidden" name="command" value="localization" />
					<a href="javascript: submitform('ru_button')">
						<img src="${pageContext.request.contextPath}/images/ru_button.png">
					</a>
				</form>
			</li>
			<li>
				<form name="en_button" id="en_button" action="${pageContext.request.contextPath}/controller" method="post">
					<input type="hidden" name="local" value="en" />
					<input type="hidden" name="command" value="localization" />
					<a href="javascript: submitform('en_button')">
						<img src="${pageContext.request.contextPath}/images/en_button.png">
					</a>
				</form>
			</li>
		</ul>
	</div>
	<div id="greeting">
		<fmt:message key="main.greeting" bundle="${loc}" />
		, ${user.firstName}!
	</div>
</body>
</html>