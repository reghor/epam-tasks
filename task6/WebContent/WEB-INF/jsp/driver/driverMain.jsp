<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.driver" var="loc" />
<html>
<head>
<title></title>
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet" type="text/css">
<script lang="javascript">
	function submitform(id) {
		document.getElementById(id).submit();
	}
</script>

</head>
<body>
	<ctg:display-date />
	<div id="main-page">
		<div id="header">
			<div id="header_nav">
				<div id="header_nav_menu">
					<jsp:include page="driverHeaderNav.jsp"></jsp:include>



				</div>
				
			</div>

			<div id="header_content">
				<jsp:include page="driverHeaderLogo.jsp"></jsp:include>
			</div>
		</div>


		<div class="header_sep"></div>

		<div id="admin_content"></div>
		<div class="clr"></div>
	</div>
</body>
</html>