<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.driver" var="loc" />
<html>
<head>
<title></title>
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet" type="text/css">
<script lang="javascript">
	function submitform(id) {
		document.getElementById(id).submit();
	}
</script>
</head>
<body>
	<div id="main-page">
		<div id="header">
			<div id="header_nav">
				<div id="header_nav_menu">
					<jsp:include page="driverHeaderNav.jsp" />
				</div>

			</div>
			<jsp:include page="driverHeaderLogo.jsp" />
			<div id="admin_content">
			<c:choose>
			<c:when test="${not empty order }">
				<h1 class="header-of-fields"><fmt:message key="header.details.order" bundle="${loc}" /></h1>
				<table border="1">
					<tr>
						<th><fmt:message key="table.order.id" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.description" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.max.load.height" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.max.load.width" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.max.load.length" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.weight" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.volume" bundle="${loc}" /></th>
					</tr>

					<tr>
						<td>${order.id}</td>
						<td>${order.description}</td>
						<td>${order.maxLoadHeight}</td>
						<td>${order.maxLoadWidth}</td>
						<td>${order.maxLoadLength}</td>
						<td>${order.weight}</td>
						<td>${order.volume}</td>

					</tr>

					<tr>
						<th><fmt:message key="table.order.point.arrival" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.point.departure" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.region.arrival" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.region.departure" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.country.arrival" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.country.departure" bundle="${loc}" /></th>
						<th><fmt:message key="table.order.status" bundle="${loc}" /></th>
					</tr>
					<tr>
						<td>${order.arrivalPoint}</td>
						<td>${order.departurePoint}</td>
						<td>${order.arrivalRegion}</td>
						<td>${order.departureRegion}</td>
						<td>${order.arrivalCountry}</td>
						<td>${order.departureCountry}</td>
						<td>${order.statusRepr}</td>
					</tr>
				</table>
				
				<br>
				<h1 class="header-of-fields"><fmt:message key="header.order.actions" bundle="${loc}" /></h1>
				<br>
				<table border="1">
					<c:choose>
						<c:when test="${order.status.lowerCaseValue=='assigned' }">
							<tr>
								<td><form action="${pageContext.request.contextPath}/controller" name="accept_order" id="accept_order" method="get">
										 <input type="hidden" name="order_id" value="${order.id}" />
										 <input type="hidden" name="command" value="accept_order" />
										 <a href="javascript: submitform('accept_order')"><fmt:message key="order.action.acceptance" bundle="${loc}" /></a>
									</form></td>
							<tr/>
						</c:when>
						<c:when test="${order.status.lowerCaseValue=='accepted' }">
							<tr>
								<td><form action="${pageContext.request.contextPath}/controller" name="accomplish_order" id="accomplish_order" method="post">
										<input type="hidden" name="order_id" value="${order.id}" />
										<input type="hidden" name="command" value="accomplish_order" />
										
										<a href="javascript: submitform('accomplish_order')"><fmt:message key="order.action.accomplishment" bundle="${loc}" /></a>
									</form></td>
							<tr />
						</c:when>
					</c:choose>
				</table>
				</c:when>
				<c:otherwise>
				<h1 class="header-of-fields"><fmt:message key="header.order.empty" bundle="${loc}" /></h1>
				</c:otherwise>
				</c:choose>
			</div>
			<div class="clr"></div>
		</div>
	</div>
</body>
</html>