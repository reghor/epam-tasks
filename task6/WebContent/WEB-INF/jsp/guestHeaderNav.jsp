<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.common" var="loc" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<c:if test="${not empty pageContext.request.queryString}">
	<c:set var="lastQueryString" scope="session" value="${pageContext.request.queryString}" />
</c:if>
</head>
<body>
	<ul>
		<li>
			<form action="${pageContext.request.contextPath}/controller" name="register_page_reference" id="register_page_reference" method="get">
				<input type="hidden" name="command" value="register_page_reference" />
				<a href="javascript: submitform('register_page_reference')">
					<fmt:message key="common.page.register" bundle="${loc}" />
				</a>
			</form>
		</li>
		<li>
			<form action="${pageContext.request.contextPath}/controller" name="login_page_reference" id="login_page_reference" method="get">
				<input type="hidden" name="command" value="login_page_reference" />
				<a href="javascript: submitform('login_page_reference')">
					<fmt:message key="common.page.login" bundle="${loc}" />
				</a>
			</form>
		</li>
		<li>
			<form action="${pageContext.request.contextPath}/controller" name="welcome_page_reference" id="welcome_page_reference" method="get">
				<input type="hidden" name="command" value="welcome_page_reference" />
				<a href="javascript: submitform('welcome_page_reference')">
					<fmt:message key="common.page.main" bundle="${loc}" />
				</a>
			</form>
		</li>
	</ul>
	<div id="header_languages">
		<ul>
			<li>
				<form name="ru_button" id="ru_button" action="${pageContext.request.contextPath}/controller" method="post">
					<input type="hidden" name="local" value="ru" />
					<input type="hidden" name="command" value="localization" />
					<a href="javascript: submitform('ru_button')">
						<img src="${pageContext.request.contextPath}/images/ru_button.png">
					</a>
				</form>
			</li>
			<li>
				<form name="en_button" id="en_button" action="${pageContext.request.contextPath}/controller" method="post">
					<input type="hidden" name="local" value="en" />
					<input type="hidden" name="command" value="localization" />
					<a href="javascript: submitform('en_button')">
						<img src="${pageContext.request.contextPath}/images/en_button.png">
					</a>
				</form>
			</li>
		</ul>
	</div>
</body>
</html>