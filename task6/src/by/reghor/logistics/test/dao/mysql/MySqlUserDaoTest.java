package by.reghor.logistics.test.dao.mysql;

import static org.junit.Assert.assertEquals;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.dao.mysql.pool.ConnectionPool;
import by.reghor.logistics.dao.mysql.pool.exception.ConnectionPoolException;
import by.reghor.logistics.entity.User;
import by.reghor.logistics.entity.UserRole;
import by.reghor.logistics.util.MySqlHelper;
import by.reghor.logistics.util.PasswordHasher;

public class MySqlUserDaoTest {
	private static final String SQL_AUTHORIZE_USER = "SELECT id, login, first_name, last_name, password, email,  phone_number, role FROM users WHERE login=? AND password=?";

	private static final String SQL_REGISTER_CUSTOMER = "INSERT INTO users (login, first_name, last_name, password, email,  phone_number) VALUES(?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE login=login";

	private static final String SQL_REGISTER_DRIVER = "INSERT INTO users (login, first_name, last_name, password, email,  phone_number, role) VALUES(?, ?, ?, ?, ?, ?, 'driver') ON DUPLICATE KEY UPDATE login=login";

	private static final String STATEMENT_EXCEPTION_MESSAGE = "Problems while working with statement";
	private static final String CONNECTION_POOL_EXCEPTION_MESSAGE = "Can not take connection from connection pool";
	private static final String ALGORITHM_EXCEPTION_MESSAGE = "Can not find hashing algorithm";
	private static final String CONNECTION_POOL_INIT_EXCEPTION_MESSAGE = "Connection pool initialization Exception";

	private static final String ENCODING_EXCEPTION_MESSAGE = "Encoding is not supported";
	private static final String USER_PASSWORD = "123";
	private static final String USER_LOGIN = "custt";

	@BeforeClass
	public static void connectionInit() {
		ConnectionPool connectionPool = ConnectionPool.getInstance();
		try {
			connectionPool.initPoolData();
		} catch (ConnectionPoolException e) {
			throw new RuntimeException(CONNECTION_POOL_INIT_EXCEPTION_MESSAGE);
		}
	}
	
	@Test
	public void testRegisterCustomer() throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ConnectionPool connectionPool = null;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();

			preparedStatement = connection.prepareStatement(SQL_REGISTER_CUSTOMER);
			String passwordHash;
			passwordHash = PasswordHasher.sha256("123");
			preparedStatement.setString(1, "custt");
			preparedStatement.setString(2, "custtf");
			preparedStatement.setString(3, "custtl");
			preparedStatement.setString(4, passwordHash);
			preparedStatement.setString(5, "custte");
			preparedStatement.setString(6, "custtp");
			assertEquals(1, preparedStatement.executeUpdate());
		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} catch (NoSuchAlgorithmException e) {
			throw new DaoException(ALGORITHM_EXCEPTION_MESSAGE, e);
		} catch (UnsupportedEncodingException e) {
			throw new DaoException(ENCODING_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement);
		}
	}

	@Test
	public void testRegisterDriver() throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ConnectionPool connectionPool = null;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			preparedStatement = connection.prepareStatement(SQL_REGISTER_DRIVER);
			String passwordHash = PasswordHasher.sha256("123");
			preparedStatement.setString(1, "drivt");
			preparedStatement.setString(2, "drivtf");
			preparedStatement.setString(3, "drivtl");
			preparedStatement.setString(4, passwordHash);
			preparedStatement.setString(5, "drivte");
			preparedStatement.setString(6, "drivtp");
			assertEquals(1, preparedStatement.executeUpdate());

		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} catch (NoSuchAlgorithmException e) {
			throw new DaoException(ALGORITHM_EXCEPTION_MESSAGE, e);
		} catch (UnsupportedEncodingException e) {
			throw new DaoException(ENCODING_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement);
		}
	}
	
	@Test
	public void testAuthorizeUser() throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		ConnectionPool connectionPool = null;
		User user = null;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();

			preparedStatement = connection.prepareStatement(SQL_AUTHORIZE_USER);
			String passwordHash;
			passwordHash = PasswordHasher.sha256(USER_PASSWORD);
			preparedStatement.setString(1, USER_LOGIN);
			preparedStatement.setString(2, passwordHash);

			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				user = MySqlHelper.getUserFromResultSet(resultSet);
			}
			assertEquals("custtf", user.getFirstName());
			assertEquals("custtl", user.getLastName());
			assertEquals("custtp", user.getPhoneNumber());
			assertEquals("custte", user.getEmail());
			assertEquals(UserRole.CUSTOMER, user.getRole());

		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} catch (NoSuchAlgorithmException e) {
			throw new DaoException(ALGORITHM_EXCEPTION_MESSAGE, e);
		} catch (UnsupportedEncodingException e) {
			throw new DaoException(ENCODING_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement, resultSet);
		}
	}

	

	@AfterClass
	public static void connectionClose() {
		ConnectionPool.getInstance().dispose();
	}
}
