package by.reghor.logistics.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.reghor.logistics.service.ajax.AjaxCommand;
import by.reghor.logistics.service.ajax.AjaxCommandRetriever;
import by.reghor.logistics.service.ajax.exception.AjaxCommandException;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.CommandRetriever;
import by.reghor.logistics.service.command.dispatcher.CommandDispatcher;

/**Controller class which accepts http requests from users and sends back http responses. Currently processes only GET and POST methods.  
 * @author Hornet
 *
 */
public final class Controller extends HttpServlet {

	private static final String COMMAND_NAME = "command";
	private static final Logger LOGGER = LogManager.getLogger(Controller.class.getName());
	private static final long serialVersionUID = 1L;
	private static final String AJAX_ENDING = "_ajax";

	private static final String AJAX_COMMAND_EXCEPTION_MESSAGE = "Exception while executing Ajax command";

	/**Processes GET requests
	 * 
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	/**Processes POST requests
	 * 
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String commandName = request.getParameter(COMMAND_NAME);
		System.out.println("command name is: "+commandName );
		if (commandName.endsWith(AJAX_ENDING)) {
			AjaxCommand command = AjaxCommandRetriever.getInstance().getAjax(commandName);
			try {
				System.out.println(command.getClass());
				command.execute(request, response);
			} catch (AjaxCommandException e) {
				LOGGER.error(AJAX_COMMAND_EXCEPTION_MESSAGE, e);
			}
		} else {
			Command command = CommandRetriever.getInstance().getCommand(commandName);
			System.out.println(command.getClass());
			CommandDispatcher.forward(command, request, response);
		}
	}
}
