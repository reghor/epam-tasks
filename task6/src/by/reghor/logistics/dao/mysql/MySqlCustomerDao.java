package by.reghor.logistics.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import by.reghor.logistics.bean.ReviewedOrder;
import by.reghor.logistics.dao.CustomerDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.dao.mysql.pool.ConnectionPool;
import by.reghor.logistics.dao.mysql.pool.exception.ConnectionPoolException;
import by.reghor.logistics.entity.Order;
import by.reghor.logistics.util.MySqlHelper;

/**Executes sql commands from customer
 * @author Hornet
 *
 */
public final class MySqlCustomerDao implements CustomerDao {

	private static final MySqlCustomerDao instance = new MySqlCustomerDao();

	private static final String SQL_ADD_NEW_ORDER = "INSERT INTO orders (id_customer, description, max_load_height, max_load_width, max_load_length, weight, volume,id_departure_point, id_arrival_point) "
			+ "VALUES(?,?,?,?,?,?,?,?,?)";

	private static final String SQL_DELETE_ORDER = "DELETE FROM orders WHERE id=? AND id_customer=? AND (status IN ('registered', 'declined', 'accomplished')) ";

	private static final String SQL_EDIT_ORDER = "UPDATE orders o SET description=?, max_load_height=?, max_load_width=?, max_load_length=?, weight=?, volume=?, id_departure_point=?, id_arrival_point=?, status='registered' "
			+ "WHERE id=? AND id_customer=? AND status='registered'";

	private static final String SQL_GET_ALL_USER_ORDERS_EN = "SELECT o.id, description, max_load_height, max_load_width, max_load_length,weight, volume, status,"
			+ " c1.city_name_en  AS departure_point_name, c2.city_name_en AS arrival_point_name "
			+ "FROM orders o "
			+ "JOIN cities c1 ON id_departure_point=c1.id " 
			+ "JOIN cities c2 ON id_arrival_point=c2.id " 
			+ "WHERE id_customer=? ";

	private static final String SQL_GET_ALL_USER_ORDERS_RU = "SELECT o.id, description, max_load_height, max_load_width, max_load_length,weight, volume, status,"
			+ " c1.city_name_ru  AS departure_point_name, c2.city_name_ru AS arrival_point_name "
			+ "FROM orders o "
			+ "JOIN cities c1 ON id_departure_point=c1.id " 
			+ "JOIN cities c2 ON id_arrival_point=c2.id " + "WHERE id_customer=? ";

	private static final String SQL_GET_COUNTRIES_EN = "SELECT id, country_name_en AS country_name " 
			+ "FROM countries cn";
	private static final String SQL_GET_COUNTRIES_RU = "SELECT id, country_name_ru AS country_name " 
			+ "FROM countries cn";

	private static final String SQL_GET_COUNTRY_REGIONS_EN = "SELECT id,  region_name_en AS region_name "
			+ "FROM regions r "
			+ "WHERE id_country=? ";
	private static final String SQL_GET_COUNTRY_REGIONS_RU = "SELECT id,  region_name_ru AS region_name " 
			+ "FROM regions r "
			+ "WHERE id_country=? ";

	private static final String SQL_GET_REGION_CITIES_EN = "SELECT id, city_name_en AS city_name " 
			+ "FROM cities ct " 
			+ "WHERE id_region=? ";
	private static final String SQL_GET_REGION_CITIES_RU = "SELECT id, city_name_ru AS city_name " 
			+ "FROM cities ct " 
			+ "WHERE id_region=? ";

	private static final String SQL_REVIEW_CUSTOMER_ORDER_EN = "SELECT o.id, description, max_load_height, max_load_width, max_load_length,weight, volume, status, "
			+ "ct1.id AS departure_point_id, ct1.city_name_en AS departure_point_name,ct2.id AS arrival_point_id, ct2.city_name_en AS arrival_point_name , "
			+ "r1.id AS departure_region_id, r1.region_name_en AS departure_region_name, r2.id AS arrival_region_id, r2.region_name_en AS arrival_region_name, "
			+ "cn1.id AS departure_country_id, cn1.country_name_en AS departure_country_name, cn2.id AS arrival_country_id, cn2.country_name_en AS arrival_country_name "
			+ "FROM orders o  "
			+ "JOIN cities ct1 ON id_departure_point=ct1.id "
			+ "JOIN cities ct2 ON id_arrival_point=ct2.id  "
			+ "JOIN regions r1 ON ct1.id_region=r1.id "
			+ "JOIN regions r2 ON ct2.id_region=r2.id "
			+ "JOIN countries cn1 ON r1.id_country=cn1.id "
			+ "JOIN countries cn2 ON r2.id_country=cn2.id " + "WHERE o.id=? AND id_customer=?";

	private static final String SQL_REVIEW_CUSTOMER_ORDER_RU = "SELECT o.id, description, max_load_height, max_load_width, max_load_length,weight, volume, status, "
			+ "ct1.id AS departure_point_id, ct1.city_name_ru AS departure_point_name,ct2.id AS arrival_point_id, ct2.city_name_ru AS arrival_point_name , "
			+ "r1.id AS departure_region_id, r1.region_name_ru AS departure_region_name, r2.id AS arrival_region_id, r2.region_name_ru AS arrival_region_name, "
			+ "cn1.id AS departure_country_id, cn1.country_name_ru AS departure_country_name, cn2.id AS arrival_country_id, cn2.country_name_ru AS arrival_country_name "
			+ "FROM orders o  "
			+ "JOIN cities ct1 ON id_departure_point=ct1.id "
			+ "JOIN cities ct2 ON id_arrival_point=ct2.id  "
			+ "JOIN regions r1 ON ct1.id_region=r1.id "
			+ "JOIN regions r2 ON ct2.id_region=r2.id "
			+ "JOIN countries cn1 ON r1.id_country=cn1.id "
			+ "JOIN countries cn2 ON r2.id_country=cn2.id " + "WHERE o.id=? AND id_customer=?";

	private static final String COUNTRY_NAME_ATTRIBUTE = "country_name";
	private static final String CITY_NAME_ATTRIBUTE = "city_name";
	private static final String REGION_NAME_ATTRIBUTE = "region_name";
	private static final String ID_ATTRIBUTE = "id";

	private static final String STATEMENT_EXCEPTION_MESSAGE = "Problems while working with statement";
	private static final String CONNECTION_POOL_EXCEPTION_MESSAGE = "Can not take connection from connection pool";

	private static final String RUSSAIN_LANGUAGE = "ru";
	private static final String ENGLISH_LANGUAGE = "en";

	private MySqlCustomerDao() {

	}

	public static MySqlCustomerDao getInstance() {
		return instance;

	}
	@Override
	public void createNewOrder(int userId, String description, int maxLoadHeight, int maxLoadWidth, int maxLoadLength, int weight, int volume,
			int departurePointId, int arrivalPointId) throws DaoException {
		ConnectionPool connectionPool = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			preparedStatement = connection.prepareStatement(SQL_ADD_NEW_ORDER);
			preparedStatement.setInt(1, userId);
			preparedStatement.setString(2, description);
			preparedStatement.setInt(3, maxLoadHeight);
			preparedStatement.setInt(4, maxLoadWidth);
			preparedStatement.setInt(5, maxLoadLength);
			preparedStatement.setInt(6, weight);
			preparedStatement.setInt(7, volume);
			preparedStatement.setInt(8, departurePointId);
			preparedStatement.setInt(9, arrivalPointId);

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement);
		}
	}

	@Override
	public boolean deleteOrder(int userId, int orderId) throws DaoException {
		ConnectionPool connectionPool = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		boolean orderDeleted = false;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_ORDER);
			preparedStatement.setInt(1, orderId);
			preparedStatement.setInt(2, userId);
			int updatedRows = preparedStatement.executeUpdate();
			if (updatedRows == 1) {
				orderDeleted = true;
			}

		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement);
		}
		return orderDeleted;

	}

	@Override
	public void editOrder(int userId, int orderId, String description, int maxLoadHeight, int maxLoadWidth, int maxLoadLength, int weight,
			int volume, int departurePointId, int arrivalPointId) throws DaoException {
		ConnectionPool connectionPool = null;
		PreparedStatement statement = null;
		Connection connection = null;

		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			statement = connection.prepareStatement(SQL_EDIT_ORDER);
			statement.setString(1, description);
			statement.setInt(2, maxLoadHeight);
			statement.setInt(3, maxLoadWidth);
			statement.setInt(4, maxLoadLength);
			statement.setInt(5, weight);
			statement.setInt(6, volume);
			statement.setInt(7, departurePointId);
			statement.setInt(8, arrivalPointId);
			statement.setInt(9, orderId);
			statement.setInt(10, userId);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, statement);
		}

	}

	@Override
	public List<Order> getAllUserOrders(int userId, String language) throws DaoException {
		ConnectionPool connectionPool = null;
		PreparedStatement statement = null;
		Connection connection = null;
		List<Order> orderList = null;
		ResultSet result = null;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			if (language.equals(RUSSAIN_LANGUAGE)) {
				statement = connection.prepareStatement(SQL_GET_ALL_USER_ORDERS_RU);
			} else {
				statement = connection.prepareStatement(SQL_GET_ALL_USER_ORDERS_EN);
			}
			statement.setInt(1, userId);
			result = statement.executeQuery();
			orderList = new ArrayList<Order>();
			while (result.next()) {
				orderList.add(MySqlHelper.getOrderFromResultSet(result));
			}
		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, statement, result);
		}
		return orderList;

	}

	@Override
	public Map<String, Integer> getCountries(String language) throws DaoException {
		ConnectionPool connectionPool = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		Map<String, Integer> countriesMap = null;
		ResultSet result = null;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			if (language.equals(RUSSAIN_LANGUAGE)) {
				preparedStatement = connection.prepareStatement(SQL_GET_COUNTRIES_RU);
			} else {
				preparedStatement = connection.prepareStatement(SQL_GET_COUNTRIES_EN);
			}
			result = preparedStatement.executeQuery();
			countriesMap = new TreeMap<>();
			while (result.next()) {
				int countryId = result.getInt(ID_ATTRIBUTE);
				String countryName = result.getString(COUNTRY_NAME_ATTRIBUTE);
				countriesMap.put(countryName, countryId);
			}

		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement, result);
		}
		return countriesMap;
	}

	@Override
	public Map<String, Integer> getCountryReginons(int countryId, String language) throws DaoException {
		ConnectionPool connectionPool = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		Map<String, Integer> regionsMap = null;
		ResultSet result = null;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			if (language.equals(RUSSAIN_LANGUAGE)) {
				preparedStatement = connection.prepareStatement(SQL_GET_COUNTRY_REGIONS_RU);
			} else {
				preparedStatement = connection.prepareStatement(SQL_GET_COUNTRY_REGIONS_EN);
			}
			preparedStatement.setInt(1, countryId);
			result = preparedStatement.executeQuery();
			regionsMap = new TreeMap<>();
			while (result.next()) {
				int regionId = result.getInt(ID_ATTRIBUTE);
				String regionName = result.getString(REGION_NAME_ATTRIBUTE);
				regionsMap.put(regionName, regionId);
			}

		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement, result);
		}
		return regionsMap;
	}

	@Override
	public Map<String, Integer> getRegionCities(int regionId, String language) throws DaoException {
		ConnectionPool connectionPool = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		Map<String, Integer> citiesMap = null;
		ResultSet result = null;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			if (language.equals(RUSSAIN_LANGUAGE)) {
				preparedStatement = connection.prepareStatement(SQL_GET_REGION_CITIES_RU);
			} else {
				preparedStatement = connection.prepareStatement(SQL_GET_REGION_CITIES_EN);
			}
			preparedStatement.setInt(1, regionId);
			result = preparedStatement.executeQuery();
			citiesMap = new TreeMap<>();
			while (result.next()) {
				int cityId = result.getInt(ID_ATTRIBUTE);
				String cityName = result.getString(CITY_NAME_ATTRIBUTE);
				citiesMap.put(cityName, cityId);
			}

		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement, result);
		}
		return citiesMap;
	}

	@Override
	public ReviewedOrder reviewOrder(int userId, int orderId, String language) throws DaoException {
		ConnectionPool connectionPool = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		ReviewedOrder reviewOrder = null;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			if (language.equals(RUSSAIN_LANGUAGE)) {
				preparedStatement = connection.prepareStatement(SQL_REVIEW_CUSTOMER_ORDER_RU);
			} else if (language.equals(ENGLISH_LANGUAGE)) {
				preparedStatement = connection.prepareStatement(SQL_REVIEW_CUSTOMER_ORDER_EN);
			}
			preparedStatement.setInt(1, orderId);
			preparedStatement.setInt(2, userId);
			result = preparedStatement.executeQuery();
			while (result.next()) {
				reviewOrder = new ReviewedOrder();
				reviewOrder.setOrder(MySqlHelper.getOrderFromResultSetDetailed(result));
			}
			return reviewOrder;
		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement, result);
		}
	}
	
	
}
