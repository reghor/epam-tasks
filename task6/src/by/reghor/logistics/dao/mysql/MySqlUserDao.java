package by.reghor.logistics.dao.mysql;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import by.reghor.logistics.dao.UserDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.dao.mysql.pool.ConnectionPool;
import by.reghor.logistics.dao.mysql.pool.exception.ConnectionPoolException;
import by.reghor.logistics.entity.User;
import by.reghor.logistics.util.MySqlHelper;
import by.reghor.logistics.util.PasswordHasher;

/**
 * Performs sql commands of registering loggin in users.
 * 
 * @author Hornet
 *
 */
public final class MySqlUserDao implements UserDao {

	private static final MySqlUserDao instance = new MySqlUserDao();

	private static final String SQL_AUTHORIZE_USER = "SELECT id, login, first_name, last_name,  email,  phone_number, role FROM users "
			+ "WHERE login=? AND password=?";

	private static final String SQL_REGISTER_CUSTOMER = "INSERT INTO users (login, first_name, last_name, password, email,  phone_number) VALUES(?, ?, ?, ?, ?, ?) "
			+ "ON DUPLICATE KEY UPDATE login=login";

	private static final String SQL_REGISTER_DRIVER = "INSERT INTO users (login, first_name, last_name, password, email,  phone_number, role) VALUES(?, ?, ?, ?, ?, ?, 'driver') "
			+ "ON DUPLICATE KEY UPDATE login=login";

	private static final String STATEMENT_EXCEPTION_MESSAGE = "Problems while working with statement";
	private static final String CONNECTION_POOL_EXCEPTION_MESSAGE = "Can not take connection from connection pool";
	private static final String ALGORITHM_EXCEPTION_MESSAGE = "Can not find hashing algorithm";
	private static final String ENCODING_EXCEPTION_MESSAGE = "Encoding is not supported";

	private MySqlUserDao() {
	}

	public static MySqlUserDao getInstance() {
		return instance;
	}

	@Override
	public User authorizeUser(String login, String password) throws DaoException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		ConnectionPool connectionPool = null;
		User user = null;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();

			preparedStatement = connection.prepareStatement(SQL_AUTHORIZE_USER);
			String passwordHash;
			passwordHash = PasswordHasher.sha256(password);
			preparedStatement.setString(1, login);
			preparedStatement.setString(2, passwordHash);

			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				user = MySqlHelper.getUserFromResultSet(resultSet);
			}

		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} catch (NoSuchAlgorithmException e) {
			throw new DaoException(ALGORITHM_EXCEPTION_MESSAGE, e);
		} catch (UnsupportedEncodingException e) {
			throw new DaoException(ENCODING_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement, resultSet);
		}
		return user;
	}

	@Override
	public boolean registerCustomer(String login, String firstName, String lastName, String password, String email, String phoneNumber)
			throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ConnectionPool connectionPool = null;
		boolean userInputUnique = true;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();

			preparedStatement = connection.prepareStatement(SQL_REGISTER_CUSTOMER);
			String passwordHash;
			passwordHash = PasswordHasher.sha256(password);
			preparedStatement.setString(1, login);
			preparedStatement.setString(2, firstName);
			preparedStatement.setString(3, lastName);
			preparedStatement.setString(4, passwordHash);
			preparedStatement.setString(5, email);
			preparedStatement.setString(6, phoneNumber);
			int rows = preparedStatement.executeUpdate();
			if (rows == 0) {
				userInputUnique = false;
			}

		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} catch (NoSuchAlgorithmException e) {
			throw new DaoException(ALGORITHM_EXCEPTION_MESSAGE, e);
		} catch (UnsupportedEncodingException e) {
			throw new DaoException(ENCODING_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement);
		}
		return userInputUnique;
	}

	@Override
	public boolean registerDriver(String login, String firstName, String lastName, String password, String email, String phoneNumber)
			throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ConnectionPool connectionPool = null;
		boolean userInputUnique = true;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();

			preparedStatement = connection.prepareStatement(SQL_REGISTER_DRIVER);
			String passwordHash = PasswordHasher.sha256(password);
			preparedStatement.setString(1, login);
			preparedStatement.setString(2, firstName);
			preparedStatement.setString(3, lastName);
			preparedStatement.setString(4, passwordHash);
			preparedStatement.setString(5, email);
			preparedStatement.setString(6, phoneNumber);
			int rows = preparedStatement.executeUpdate();
			if (rows == 0) {
				userInputUnique = false;
			}

		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} catch (NoSuchAlgorithmException e) {
			throw new DaoException(ALGORITHM_EXCEPTION_MESSAGE, e);
		} catch (UnsupportedEncodingException e) {
			throw new DaoException(ENCODING_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement);
		}
		return userInputUnique;
	}

}
