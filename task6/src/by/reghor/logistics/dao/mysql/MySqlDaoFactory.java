package by.reghor.logistics.dao.mysql;

import by.reghor.logistics.dao.CustomerDao;
import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.DispatcherDao;
import by.reghor.logistics.dao.DriverDao;
import by.reghor.logistics.dao.UserDao;

public final class MySqlDaoFactory extends DaoFactory {

	private static final MySqlDaoFactory instance = new MySqlDaoFactory();

	private MySqlDaoFactory() {
	}

	public static MySqlDaoFactory getInstance() {
		return instance;
	}

	@Override
	public UserDao getUserDao() {
		return MySqlUserDao.getInstance();
	}

	@Override
	public CustomerDao getCustomerDao() {
		return MySqlCustomerDao.getInstance();
	}

	@Override
	public DispatcherDao getDispatcherDao() {
		return MySqlDispatcherDao.getInstance();
	}

	@Override
	public DriverDao getDriverDao() {
		return MySqlDriverDao.getInstance();
	}

}
