package by.reghor.logistics.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.reghor.logistics.bean.ReviewedOrder;
import by.reghor.logistics.dao.DispatcherDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.dao.mysql.pool.ConnectionPool;
import by.reghor.logistics.dao.mysql.pool.exception.ConnectionPoolException;
import by.reghor.logistics.entity.Car;
import by.reghor.logistics.entity.Order;
import by.reghor.logistics.entity.OrderStatus;
import by.reghor.logistics.entity.User;
import by.reghor.logistics.util.MySqlHelper;

/**
 * Executes SQL commands of dispatcher.
 * 
 * @author Hornet
 *
 */
public final class MySqlDispatcherDao implements DispatcherDao {

	private static final MySqlDispatcherDao instance = new MySqlDispatcherDao();

	private static final String STATEMENT_EXCEPTION_MESSAGE = "Problems while working with statement";
	private static final String CONNECTION_POOL_EXCEPTION_MESSAGE = "Can not take connection from connection pool";

	private static final String RUSSAIN_LANGUAGE = "ru";
	private static final String ENGLISH_LANGUAGE = "en";

	private static final String SQL_GET_SUITABLE_CARS = "SELECT id, vehicle_id_number, max_height, max_length, max_width, max_volume, max_weight, make_of_car, colour, year, car_condition, status "
			+ "FROM сars c "
			+ "WHERE car_condition='faultless' "
			+ "AND ?<max_height "
			+ "AND ?<max_length "
			+ "AND ?<max_width "
			+ "AND ?<max_volume " 
			+ "AND ?<max_weight " 
			+ "AND status='free' "
			+ "AND id_driver IS NOT NULL";

	private static final String SQL_ASSIGN_ORDER_CAR = "UPDATE orders o "
			+ "JOIN сars c ON c.car_condition='faultless' AND c.status='free' AND c.id=? AND c.id_driver IS NOT NULL "
			+ "SET id_car=c.id , o.status='assigned', c.status='busy' " 
			+ "WHERE o.id=? " 
			+ "AND o.status='registered' "
			+ "AND max_load_height<max_height "
			+ "AND max_load_length<max_length "
			+ "AND max_load_width<max_width "
			+ "AND volume<max_volume " 
			+ "AND weight<max_weight " ;

	private static final String SQL_ASSIGN_DRIVER_CAR = "UPDATE сars c1 " 
			+ " JOIN users u ON u.id=? AND role='driver' " 
			+ " SET c1.id_driver=u.id "
			+ " WHERE  c1.id=? AND c1.status='free'  AND c1.id_driver IS NULL ";
	private static final String SQL_CANCEL_DRIVER_CAR_ASSIGNMENT = "UPDATE сars c " 
			+ "JOIN users u ON u.id=? AND role='driver' "
			+ "SET c.id_driver=NULL " 
			+ "WHERE  c.id_driver=u.id AND c.status='free'  ";

	private static final String SQL_CANCEL_ORDER_ASSIGNMENT = "UPDATE orders o " 
			+ "JOIN сars c ON o.id_car=c.id "
			+ "SET c.status='free',id_car=NULL, o.status='registered' "
			+ "WHERE o.id=? AND o.status='assigned' ";

	private static final String SQL_DELETE_CAR = "DELETE FROM сars WHERE id=? AND status='free'";

	private static final String SQL_GET_ALL_CARS = "SELECT id, id_driver, vehicle_id_number, max_height, max_length, max_width, max_volume, max_weight, make_of_car, colour, year, car_condition, status "
			+ "FROM сars c  ";

	private static final String SQL_GET_FREE_CARS = "SELECT  c.id ,  vehicle_id_number, max_height, max_length, max_width, max_volume, max_weight, make_of_car, colour, year, car_condition, status "
			+ "FROM сars c " 
			+ "WHERE c.status='free' AND id_driver IS NULL ";

	private static final String SQL_GET_ALL_DRIVERS = "SELECT id,  first_name, last_name,  phone_number, email FROM users u WHERE role='driver'";

	private static final String SQL_GET_ALL_ORDERS_DISPATCHER_EN = "SELECT o.id, description, max_load_height, max_load_width, max_load_length,weight, volume, o.status,"
			+ " c1.city_name_en  AS departure_point_name, c2.city_name_en AS arrival_point_name "
			+ "FROM orders o "
			+ "JOIN cities c1 ON id_departure_point=c1.id " 
			+ "JOIN cities c2 ON id_arrival_point=c2.id ";

	private static final String SQL_GET_ALL_ORDERS_DISPATCHER_RU = "SELECT o.id, description, max_load_height, max_load_width, max_load_length,weight, volume, o.status,"
			+ " c1.city_name_ru  AS departure_point_name, c2.city_name_ru AS arrival_point_name "
			+ "FROM orders o "
			+ "JOIN cities c1 ON id_departure_point=c1.id " 
			+ "JOIN cities c2 ON id_arrival_point=c2.id ";

	private static final String SQL_GET_DRIVER_CAR = "SELECT id,  vehicle_id_number, max_height, max_length, max_width, max_volume, max_weight, make_of_car, colour, year, car_condition, status "
			+ "FROM сars c WHERE id_driver=? ";

	private static final String SQL_REVIEW_ORDER_EN = "SELECT o.id, description, max_load_height, max_load_width, max_load_length,weight, volume, o.status, email, "
			+ "ct1.id AS departure_point_id, ct1.city_name_en AS departure_point_name,ct2.id AS arrival_point_id, ct2.city_name_en AS arrival_point_name , "
			+ "r1.id AS departure_region_id, r1.region_name_en AS departure_region_name, r2.id AS arrival_region_id, r2.region_name_en AS arrival_region_name, "
			+ "cn1.id AS departure_country_id, cn1.country_name_en AS departure_country_name, cn1.id AS arrival_country_id, cn1.country_name_en AS arrival_country_name, "
			+ "cr.id AS id_car,   cr.vehicle_id_number, cr.max_height, cr.max_length, cr.max_width, cr.max_volume, cr.max_weight, cr.colour, cr.make_of_car,cr.year, car_condition, "
			+ "id_driver , first_name , last_name , phone_number "
			+ "FROM orders o  "
			+ "JOIN cities ct1 ON id_departure_point=ct1.id "
			+ "JOIN cities ct2 ON id_arrival_point=ct2.id  "
			+ "JOIN regions r1 ON ct1.id_region=r1.id "
			+ "JOIN regions r2 ON ct2.id_region=r2.id "
			+ "JOIN countries cn1 ON r1.id_country=cn1.id "
			+ "JOIN countries cn2 ON r2.id_country=cn2.id "
			+ "LEFT JOIN сars cr ON o.id_car=cr.id "
			+ "LEFT JOIN users us ON us.id=cr.id_driver " 
			+ "WHERE o.id=? ";

	private static final String SQL_REVIEW_ORDER_RU = "SELECT o.id, description, max_load_height, max_load_width, max_load_length,weight, volume, o.status, email, "
			+ "ct1.id AS departure_point_id, ct1.city_name_ru AS departure_point_name,ct2.id AS arrival_point_id, ct2.city_name_ru AS arrival_point_name , "
			+ "r1.id AS departure_region_id, r1.region_name_ru AS departure_region_name, r2.id AS arrival_region_id, r2.region_name_ru AS arrival_region_name, "
			+ "cn1.id AS departure_country_id, cn1.country_name_ru AS departure_country_name, cn1.id AS arrival_country_id, cn1.country_name_ru AS arrival_country_name, "
			+ "cr.id AS id_car,   cr.vehicle_id_number, cr.max_height, cr.max_length, cr.max_width, cr.max_volume, cr.max_weight, cr.colour, cr.make_of_car,cr.year, car_condition, "
			+ "id_driver , first_name , last_name , phone_number "
			+ "FROM orders o  "
			+ "JOIN cities ct1 ON id_departure_point=ct1.id "
			+ "JOIN cities ct2 ON id_arrival_point=ct2.id  "
			+ "JOIN regions r1 ON ct1.id_region=r1.id "
			+ "JOIN regions r2 ON ct2.id_region=r2.id "
			+ "JOIN countries cn1 ON r1.id_country=cn1.id "
			+ "JOIN countries cn2 ON r2.id_country=cn2.id "
			+ "LEFT JOIN сars cr ON o.id_car=cr.id "
			+ "LEFT JOIN users us ON us.id=cr.id_driver " 
			+ "WHERE o.id=? ";

	private static final String SQL_ADD_NEW_CAR = "INSERT INTO сars ( vehicle_id_number, max_height, max_length, max_width, max_volume, max_weight, make_of_car, colour, year, car_condition) "
			+ "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, 'faulty') " 
			+ "ON DUPLICATE KEY UPDATE max_height=max_height ";

	private static final String SQL_DECLINE_ORDER = "UPDATE orders o " 
			+ "SET  o.status='declined' " 
			+ "WHERE o.id=? AND o.status='registered' ";;

	private MySqlDispatcherDao() {
	}

	public static MySqlDispatcherDao getInstance() {
		return instance;
	}

	@Override
	public boolean assignOrderCar(int orderId, int carId) throws DaoException {
		ConnectionPool connectionPool = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		boolean assignmentSuccessful=false;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			preparedStatement = connection.prepareStatement(SQL_ASSIGN_ORDER_CAR);
			preparedStatement.setInt(1, carId);
			preparedStatement.setInt(2, orderId);
			int rows=preparedStatement.executeUpdate();
			if (rows==2){
				assignmentSuccessful=true;	
			}

		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement);
		}
		return assignmentSuccessful;
	}

	@Override
	public void cancelOrderCarAssignment(int orderId) throws DaoException {
		ConnectionPool connectionPool = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			preparedStatement = connection.prepareStatement(SQL_CANCEL_ORDER_ASSIGNMENT);
			preparedStatement.setInt(1, orderId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement);
		}
	}

	@Override
	public boolean deleteCar(int carId) throws DaoException {
		ConnectionPool connectionPool = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		boolean carDeleted=false;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_CAR);
			preparedStatement.setInt(1, carId);
			int rows=preparedStatement.executeUpdate();
			if (rows==1){
				carDeleted=true;
			}
		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement);
		}
		return carDeleted;
	}

	@Override
	public List<Car> getAllCars() throws DaoException {
		ConnectionPool connectionPool = null;
		PreparedStatement statement = null;
		Connection connection = null;
		List<Car> carList = null;
		ResultSet result = null;

		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			statement = connection.prepareStatement(SQL_GET_ALL_CARS);
			result = statement.executeQuery();
			carList = new ArrayList<>();
			while (result.next()) {
				Car car = MySqlHelper.getCarFromResultSet(result);
				carList.add(car);
			}
		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, statement, result);
		}
		return carList;
	}

	@Override
	public List<User> getAllDrivers() throws DaoException {
		ConnectionPool connectionPool = null;
		PreparedStatement statement = null;
		Connection connection = null;
		List<User> driverList = null;
		ResultSet result = null;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			statement = connection.prepareStatement(SQL_GET_ALL_DRIVERS);
			result = statement.executeQuery();
			driverList = new ArrayList<>();
			while (result.next()) {
				User driver = MySqlHelper.getDriverFromResultSet(result);
				driverList.add(driver);
			}
		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, statement, result);
		}
		return driverList;
	}

	@Override
	public List<Order> getAllOrders(String language) throws DaoException {
		List<Order> orderList = null;
		if (language.equals(RUSSAIN_LANGUAGE)) {
			orderList = getOrders(SQL_GET_ALL_ORDERS_DISPATCHER_RU);
		} else if (language.equals(ENGLISH_LANGUAGE)) {
			orderList = getOrders(SQL_GET_ALL_ORDERS_DISPATCHER_EN);
		}
		return orderList;
	}

	@Override
	public Car getDriverCar(int driverId) throws DaoException {
		ConnectionPool connectionPool = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		Car car = null;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			preparedStatement = connection.prepareStatement(SQL_GET_DRIVER_CAR);
			preparedStatement.setInt(1, driverId);
			ResultSet result = preparedStatement.executeQuery();
			while (result.next()) {
				car = MySqlHelper.getCarFromResultSet(result);
			}
		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement);
		}
		return car;
	}

	@Override
	public List<Car> getSuitableCarsForAssign(int maxHeight, int maxLength, int maxWidth, int maxVolume, int maxWeight) throws DaoException {
		ConnectionPool connectionPool = null;
		PreparedStatement statement = null;
		Connection connection = null;
		List<Car> carList = null;
		ResultSet result = null;
		try {
			System.out.println(maxHeight+"|"+maxLength+"|"+maxWidth+"|"+maxVolume+"|"+maxWeight);
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			statement = connection.prepareStatement(SQL_GET_SUITABLE_CARS);
			statement.setInt(1, maxHeight);
			statement.setInt(2, maxWidth);
			statement.setInt(3, maxLength);
			statement.setInt(4, maxVolume);
			statement.setInt(5, maxWeight);
			result = statement.executeQuery();
			carList = new ArrayList<>();
			while (result.next()) {
				carList.add(MySqlHelper.getCarFromResultSet(result));
			}
		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, statement, result);
		}
		return carList;
	}

	@Override
	public ReviewedOrder reviewOrder(int id, String language) throws DaoException {
		ConnectionPool connectionPool = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		ReviewedOrder reviewOrder = null;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			if (language.equals(RUSSAIN_LANGUAGE)) {
				preparedStatement = connection.prepareStatement(SQL_REVIEW_ORDER_RU);
			} else if (language.equals(ENGLISH_LANGUAGE)) {
				preparedStatement = connection.prepareStatement(SQL_REVIEW_ORDER_EN);
			}
			preparedStatement.setInt(1, id);
			result = preparedStatement.executeQuery();
			while (result.next()) {
				reviewOrder = new ReviewedOrder();
				reviewOrder.setOrder(MySqlHelper.getOrderFromResultSetDetailed(result));
				if (reviewOrder.getOrder().getStatus() != OrderStatus.REGISTERED && reviewOrder.getOrder().getStatus() != OrderStatus.DECLINED) {
					reviewOrder.setDriver(MySqlHelper.getDriverFromResultSet(result));
					reviewOrder.setCar(MySqlHelper.getCarNoStatusFromResultSet(result));
				}
			}
			return reviewOrder;
		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement, result);
		}
	}

	private List<Order> getOrders(String query) throws DaoException {
		ConnectionPool connectionPool = null;
		PreparedStatement statement = null;
		Connection connection = null;
		List<Order> orderList = null;
		ResultSet result = null;

		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			statement = connection.prepareStatement(query);
			result = statement.executeQuery();
			orderList = new ArrayList<Order>();
			while (result.next()) {
				orderList.add(MySqlHelper.getOrderFromResultSet(result));
			}
		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, statement, result);
		}
		return orderList;
	}

	@Override
	public boolean addNewCar(String carNumber, String makeOfCar, String carColour, int carYear, int carMaxHeight, int carMaxLength, int carMaxWidth,
			int carMaxVolume, int carMaxWeight) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ConnectionPool connectionPool = null;
		boolean userInputUnique = true;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();

			preparedStatement = connection.prepareStatement(SQL_ADD_NEW_CAR);
			preparedStatement.setString(1, carNumber);
			preparedStatement.setInt(2, carMaxHeight);
			preparedStatement.setInt(3, carMaxLength);
			preparedStatement.setInt(4, carMaxWidth);
			preparedStatement.setInt(5, carMaxVolume);
			preparedStatement.setInt(6, carMaxWeight);
			preparedStatement.setString(7, makeOfCar);
			preparedStatement.setString(8, carColour);
			preparedStatement.setInt(9, carYear);
			int rows = preparedStatement.executeUpdate();
			if (rows == 0) {
				userInputUnique = false;
			}

		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement);
		}
		return userInputUnique;
	}

	@Override
	public List<Car> getFreeCars() throws DaoException {
		ConnectionPool connectionPool = null;
		PreparedStatement statement = null;
		Connection connection = null;
		List<Car> carList = null;
		ResultSet result = null;

		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			statement = connection.prepareStatement(SQL_GET_FREE_CARS);
			result = statement.executeQuery();
			carList = new ArrayList<>();
			while (result.next()) {
				Car car = MySqlHelper.getCarFromResultSet(result);
				carList.add(car);
			}
		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, statement, result);
		}
		return carList;
	}

	@Override
	public boolean assignDriverCar(int driverId, int carId) throws DaoException {
		ConnectionPool connectionPool = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		boolean carAssigned = false;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			preparedStatement = connection.prepareStatement(SQL_ASSIGN_DRIVER_CAR);
			preparedStatement.setInt(1, driverId);
			preparedStatement.setInt(2, carId);
			int rows = preparedStatement.executeUpdate();
			if (rows == 1) {
				carAssigned = true;
			}
		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement);
		}
		return carAssigned;
	}

	@Override
	public void cancelDriverCarAssignment(int driverId) throws DaoException {
		ConnectionPool connectionPool = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			preparedStatement = connection.prepareStatement(SQL_CANCEL_DRIVER_CAR_ASSIGNMENT);
			preparedStatement.setInt(1, driverId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement);
		}
	}

	@Override
	public void declineOrder(int orderId) throws DaoException {
		ConnectionPool connectionPool = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			preparedStatement = connection.prepareStatement(SQL_DECLINE_ORDER);
			preparedStatement.setInt(1, orderId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement);
		}

	}
}
