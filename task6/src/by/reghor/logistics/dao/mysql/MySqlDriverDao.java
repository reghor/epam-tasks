package by.reghor.logistics.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import by.reghor.logistics.dao.DriverDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.dao.mysql.pool.ConnectionPool;
import by.reghor.logistics.dao.mysql.pool.exception.ConnectionPoolException;
import by.reghor.logistics.entity.Car;
import by.reghor.logistics.entity.Order;
import by.reghor.logistics.util.MySqlHelper;

/**
 * Performs sql commands of driver.
 * 
 * @author Hornet
 *
 */
public final class MySqlDriverDao implements DriverDao {

	private static final MySqlDriverDao instance = new MySqlDriverDao();

	private static final String STATEMENT_EXCEPTION_MESSAGE = "Problems while working with statement";
	private static final String CONNECTION_POOL_EXCEPTION_MESSAGE = "Can not take connection from connection pool";
	private static final String RUSSAIN_LANGUAGE = "ru";
	private static final String ENGLISH_LANGUAGE = "en";

	private static final String SQL_ACCCOMPLISH_ORDER = "UPDATE orders o " 
			+ "JOIN сars c ON o.id_car=c.id AND c.id_driver=? "
			+ "SET  o.status='accomplished', c.status='free' " 
			+ "WHERE o.id=? AND o.status='accepted' ";

	private static final String SQL_ACCEPT_ORDER = "UPDATE orders o " 
			+ "JOIN сars c ON o.id_car=c.id AND c.id_driver=? "
			+ "SET  o.status='accepted' " 
			+ "WHERE o.id=? AND o.status='assigned' ";

	private static final String SQL_GET_DRIVER_CAR = "SELECT id, id_driver, vehicle_id_number, max_height, max_length, max_width, max_volume, max_weight,"
			+ " make_of_car, colour, year, car_condition, status FROM сars c WHERE id_driver=? ";

	private static final String SQL_GET_DRIVER_ORDER_EN = "SELECT DISTINCT o.id, description, max_load_height, max_load_width, max_load_length,weight, volume, o.status, "
			+ "ct1.id AS departure_point_id, ct1.city_name_en AS departure_point_name,ct2.id AS arrival_point_id, ct2.city_name_en AS arrival_point_name , "
			+ "r1.id AS departure_region_id, r1.region_name_en AS departure_region_name, r2.id AS arrival_region_id, r2.region_name_en AS arrival_region_name, "
			+ "cn1.id AS departure_country_id, cn1.country_name_en AS departure_country_name, cn2.id AS arrival_country_id, cn2.country_name_en AS arrival_country_name "
			+ "FROM orders o  "
			+ "JOIN cities ct1 ON id_departure_point=ct1.id "
			+ "JOIN cities ct2 ON id_arrival_point=ct2.id  "
			+ "JOIN regions r1 ON ct1.id_region=r1.id "
			+ "JOIN regions r2 ON ct2.id_region=r2.id "
			+ "JOIN countries cn1 ON r1.id_country=cn1.id "
			+ "JOIN countries cn2 ON r2.id_country=cn2.id "
			+ "JOIN сars c ON id_driver=? "
			+ "WHERE o.status IN ('assigned','accepted') AND c.id=o.id_car ";

	private static final String SQL_GET_DRIVER_ORDER_RU = "SELECT DISTINCT o.id, description, max_load_height, max_load_width, max_load_length,weight, volume, o.status, "
			+ "ct1.id AS departure_point_id, ct1.city_name_ru AS departure_point_name,ct2.id AS arrival_point_id, ct2.city_name_ru AS arrival_point_name , "
			+ "r1.id AS departure_region_id, r1.region_name_ru AS departure_region_name, r2.id AS arrival_region_id, r2.region_name_ru AS arrival_region_name, "
			+ "cn1.id AS departure_country_id, cn1.country_name_ru AS departure_country_name, cn1.id AS arrival_country_id, cn1.country_name_ru AS arrival_country_name "
			+ "FROM orders o  "
			+ "JOIN cities ct1 ON id_departure_point=ct1.id "
			+ "JOIN cities ct2 ON id_arrival_point=ct2.id  "
			+ "JOIN regions r1 ON ct1.id_region=r1.id "
			+ "JOIN regions r2 ON ct2.id_region=r2.id "
			+ "JOIN countries cn1 ON r1.id_country=cn1.id "
			+ "JOIN countries cn2 ON r2.id_country=cn2.id "
			+ "JOIN сars c ON id_driver=? "
			+ "WHERE o.status IN ('assigned', 'accepted') AND c.id=o.id_car";

	private static final String SQL_MAKE_CAR_FAULTLESS = "UPDATE сars c SET  car_condition='faultless' " 
			+ "WHERE id=? AND id_driver=?";

	private static final String SQL_MAKE_CAR_FAULTY = "UPDATE сars c " 
			+ "SET  car_condition='faulty' WHERE id=? AND id_driver=? AND status='free'";

	private MySqlDriverDao() {
	}

	public static MySqlDriverDao getInstance() {
		return instance;
	}

	@Override
	public void acceptOrder(int orderId, int userId) throws DaoException {
		ConnectionPool connectionPool = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			preparedStatement = connection.prepareStatement(SQL_ACCEPT_ORDER);
			preparedStatement.setInt(1, userId);
			preparedStatement.setInt(2, orderId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement, result);
		}
	}

	@Override
	public void accomplishOrder(int orderId, int userId) throws DaoException {
		ConnectionPool connectionPool = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			preparedStatement = connection.prepareStatement(SQL_ACCCOMPLISH_ORDER);
			preparedStatement.setInt(1, userId);
			preparedStatement.setInt(2, orderId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement, result);
		}
	}

	@Override
	public Car getDriverCar(int driverId) throws DaoException {
		ConnectionPool connectionPool = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		Car car = null;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			preparedStatement = connection.prepareStatement(SQL_GET_DRIVER_CAR);
			preparedStatement.setInt(1, driverId);
			result = preparedStatement.executeQuery();
			while (result.next()) {
				car = MySqlHelper.getCarFromResultSet(result);
			}
		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement, result);
		}
		return car;
	}

	@Override
	public Order getDriverOrder(int driverId, String language) throws DaoException {
		ConnectionPool connectionPool = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		Order order = null;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			if (language.equals(RUSSAIN_LANGUAGE)) {
				preparedStatement = connection.prepareStatement(SQL_GET_DRIVER_ORDER_RU);
			} else if (language.equals(ENGLISH_LANGUAGE)) {
				preparedStatement = connection.prepareStatement(SQL_GET_DRIVER_ORDER_EN);
			}
			preparedStatement.setInt(1, driverId);
			result = preparedStatement.executeQuery();
			while (result.next()) {
				order = MySqlHelper.getOrderFromResultSetDetailed(result);
			}
		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement, result);
		}
		return order;
	}

	@Override
	public void makeCarFaultless(int carId, int userId) throws DaoException {
		ConnectionPool connectionPool = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			preparedStatement = connection.prepareStatement(SQL_MAKE_CAR_FAULTLESS);
			preparedStatement.setInt(1, carId);
			preparedStatement.setInt(2, userId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement, result);
		}
	}

	@Override
	public boolean makeCarFaulty(int carId, int userId) throws DaoException {
		ConnectionPool connectionPool = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		boolean success = false;
		try {
			connectionPool = ConnectionPool.getInstance();
			connection = connectionPool.takeConnection();
			preparedStatement = connection.prepareStatement(SQL_MAKE_CAR_FAULTY);
			preparedStatement.setInt(1, carId);
			preparedStatement.setInt(2, userId);
			int rows = preparedStatement.executeUpdate();
			if (rows == 1) {
				success = true;
			}
		} catch (SQLException e) {
			throw new DaoException(STATEMENT_EXCEPTION_MESSAGE, e);
		} catch (ConnectionPoolException e) {
			throw new DaoException(CONNECTION_POOL_EXCEPTION_MESSAGE, e);
		} finally {
			connectionPool.closeConnection(connection, preparedStatement, result);
		}
		return success;

	}
}
