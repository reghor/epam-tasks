package by.reghor.logistics.dao.mysql.pool.exception;


/**Exception which is thrown during using connection pool.
 * @author Hornet
 *
 */
public class ConnectionPoolException extends Exception {

	private static final long serialVersionUID = 1L;

	public ConnectionPoolException() {
        super();
    }

    public ConnectionPoolException(String message) {
        super(message);
    }

    public ConnectionPoolException(String message, Throwable exception) {
        super(message, exception);
    }

    public ConnectionPoolException(Throwable exception) {
        super(exception);
    }

}
