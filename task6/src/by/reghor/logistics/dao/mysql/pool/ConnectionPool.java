package by.reghor.logistics.dao.mysql.pool;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.reghor.logistics.dao.mysql.pool.exception.ConnectionPoolException;

public final class ConnectionPool {

	private class PooledConnection implements Connection {
		private Connection connection;
		private static final String CLOSE_CLOSED_CONNECTION_EXCEPTION_MESSAGE="Attempting to close closed	connection.";
		private static final String DELETING_CONNECTION_FROM_TAKEN_QUEUE_EXCEPTION_MESSAGE="Exception on deleting connection from the given away queue.";
		private static final String ALLOCATING_CONNECTION_EXCEPTION_MESSAGE="Exception on allocating connection in the pool.";
		

		/**Creates an instance of connection wrapper class PooledConnection 
		 * @param c - SQL connection
		 * @throws SQLException
		 */
		public PooledConnection(Connection c) throws SQLException {
			this.connection = c;
			this.connection.setAutoCommit(true);
		}

		@Override
		public void abort(Executor executor) throws SQLException {
			connection.abort(executor);
		}

		@Override
		public void clearWarnings() throws SQLException {
			connection.clearWarnings();
		}
		@Override
		public void close() throws SQLException {
			if  (connection.isClosed()) {
				throw new SQLException(CLOSE_CLOSED_CONNECTION_EXCEPTION_MESSAGE);}
			if  (connection.isReadOnly()) {
				connection.setReadOnly(false);
			}
				if	(!takenConQueue.remove(this)){
					throw new SQLException(	DELETING_CONNECTION_FROM_TAKEN_QUEUE_EXCEPTION_MESSAGE);}	
				if  (!availableConQueue.offer(this)) {
					throw new SQLException(	ALLOCATING_CONNECTION_EXCEPTION_MESSAGE);	}
			
		}

		@Override
		public void commit() throws SQLException {
			connection.commit();
		}

		@Override
		public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
			return connection.createArrayOf(typeName, elements);
		}

		@Override
		public Blob createBlob() throws SQLException {
			return connection.createBlob();
		}

		@Override
		public Clob createClob() throws SQLException {
			return connection.createClob();
		}

		@Override
		public NClob createNClob() throws SQLException {
			return connection.createNClob();
		}

		@Override
		public SQLXML createSQLXML() throws SQLException {
			return connection.createSQLXML();
		}

		@Override
		public Statement createStatement() throws SQLException {
			return connection.createStatement();
		}

		@Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
			return connection.createStatement(resultSetType, resultSetConcurrency);
		}

		@Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
			return connection.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
		}

		@Override
		public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
			return connection.createStruct(typeName, attributes);
		}

		@Override
		public boolean getAutoCommit() throws SQLException {
			return connection.getAutoCommit();
		}

		@Override
		public String getCatalog() throws SQLException {
			return connection.getCatalog();
		}

		@Override
		public Properties getClientInfo() throws SQLException {
			return connection.getClientInfo();
		}

		@Override
		public String getClientInfo(String name) throws SQLException {
			return connection.getClientInfo(name);
		}

		@Override
		public int getHoldability() throws SQLException {
			return connection.getHoldability();
		}

		@Override
		public DatabaseMetaData getMetaData() throws SQLException {
			return connection.getMetaData();
		}

		@Override
		public int getNetworkTimeout() throws SQLException {
			return connection.getNetworkTimeout();
		}

		@Override
		public String getSchema() throws SQLException {
			return connection.getSchema();
		}

		@Override
		public int getTransactionIsolation() throws SQLException {
			return connection.getTransactionIsolation();
		}

		@Override
		public Map<String, Class<?>> getTypeMap() throws SQLException {
			return connection.getTypeMap();
		}

		@Override
		public SQLWarning getWarnings() throws SQLException {
			return connection.getWarnings();
		}

		@Override
		public boolean isClosed() throws SQLException {
			return connection.isClosed();
		}

		@Override
		public boolean isReadOnly() throws SQLException {
			return connection.isReadOnly();
		}

		@Override
		public boolean isValid(int timeout) throws SQLException {
			return connection.isValid(timeout);
		}

		@Override
		public boolean isWrapperFor(Class<?> iface) throws SQLException {
			return connection.isWrapperFor(iface);
		}

		@Override
		public String nativeSQL(String sql) throws SQLException {
			return connection.nativeSQL(sql);
		}

		@Override
		public CallableStatement prepareCall(String sql) throws SQLException {
			return connection.prepareCall(sql);
		}

		@Override
		public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
			return connection.prepareCall(sql, resultSetType, resultSetConcurrency);
		}

		@Override
		public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
			return connection.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
		}

		@Override
		public PreparedStatement prepareStatement(String sql) throws SQLException {
			return connection.prepareStatement(sql);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
			return connection.prepareStatement(sql, autoGeneratedKeys);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
			return connection.prepareStatement(sql, resultSetType, resultSetConcurrency);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
			return connection.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
			return connection.prepareStatement(sql, columnIndexes);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
			return connection.prepareStatement(sql, columnNames);
		}

		public void reallyClose() throws SQLException {
			connection.close();
		}

		@Override
		public void releaseSavepoint(Savepoint savepoint) throws SQLException {
			connection.releaseSavepoint(savepoint);
		}

		@Override
		public void rollback() throws SQLException {
			connection.rollback();
		}

		@Override
		public void rollback(Savepoint savepoint) throws SQLException {
			connection.rollback();
		}

		@Override
		public void setAutoCommit(boolean autoCommit) throws SQLException {
			connection.setAutoCommit(autoCommit);
		}

		@Override
		public void setCatalog(String catalog) throws SQLException {
			connection.setCatalog(catalog);
		}

		@Override
		public void setClientInfo(Properties properties) throws SQLClientInfoException {
			connection.setClientInfo(properties);
		}

		@Override
		public void setClientInfo(String name, String value) throws SQLClientInfoException {
			connection.setClientInfo(name, value);
		}

		@Override
		public void setHoldability(int holdability) throws SQLException {

		}

		@Override
		public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
			connection.setNetworkTimeout(executor, milliseconds);
		}

		@Override
		public void setReadOnly(boolean readOnly) throws SQLException {
			connection.setReadOnly(readOnly);
		}

		@Override
		public Savepoint setSavepoint() throws SQLException {
			return connection.setSavepoint();
		}

		@Override
		public Savepoint setSavepoint(String name) throws SQLException {
			return connection.setSavepoint();
		}

		@Override
		public void setSchema(String schema) throws SQLException {
			connection.setSchema(schema);
		}

		@Override
		public void setTransactionIsolation(int level) throws SQLException {
			connection.setTransactionIsolation(level);
		}

		@Override
		public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
			connection.setTypeMap(map);
		}

		@Override
		public <T> T unwrap(Class<T> iface) throws SQLException {
			return connection.unwrap(iface);
		}
	}
	private static final ConnectionPool instance= new ConnectionPool();
	private static final Logger LOGGER=LogManager.getLogger(ConnectionPool.class.getName());
	private BlockingQueue<Connection> availableConQueue;
	private String driverName;
	private String password;
	private int poolSize;
	private BlockingQueue<Connection> takenConQueue;
	private static final String ON_CREATING_POOL_EXCEPTION_MESSAGE="SQLException in connection pool";
	private static final String NOT_RETURNED_CONNECTION_EXCEPTION_MESSAGE="Connection wasn't returned to the pool.";
	private static final String NOT_CLOSED_STATEMENT_EXCEPTION_MESSAGE="Statement wasn't closed.";
	private static final String NOT_CLOSED_RESULTSET_EXCEPTION_MESSAGE="ResultSet wasn't  closed.";
	private static final String NO_DATABASE_DRIVER_CLASS_EXCEPTION_MESSAGE="Can't find database driver class";
	private static final String NO_DATASOURCE_CONNECTION_EXCEPTION_MESSAGE="Error connecting to the data source";
	private static final String NOT_CLOSED_CONNECTION_EXCEPTION_MESSAGE="Error closing the connection.";



	private String url;

	private String user;


	private ConnectionPool() {
		DatabaseResourceManager databaseResourceManager = DatabaseResourceManager.getInstance();
		this.driverName = databaseResourceManager.getValue(DatabaseResourceManager.DB_DRIVER);
		this.url = databaseResourceManager.getValue(DatabaseResourceManager.DB_URL);
		this.user = databaseResourceManager.getValue(DatabaseResourceManager.DB_USER);
		this.password = databaseResourceManager.getValue(DatabaseResourceManager.DB_PASSWORD);
		try {
			this.poolSize = Integer.parseInt(databaseResourceManager.getValue(DatabaseResourceManager.DB_POOL_SIZE));
		} catch (NumberFormatException e) {
			LOGGER.error(e);
			poolSize = 10;
		}
	}

	/**Gets connection pool instance
	 * @return connection pool instance
	 */
	public static ConnectionPool getInstance() {
		return instance;
	}

	/**Returns taken connection to the available connection pool and closes used SQL statement.
	 * @param connection - taken pooled connection
	 * @param statement - used SQL statement 
	 */
	public void closeConnection(Connection connection,  Statement statement) {
		try {
			connection.close();
		} catch  (SQLException e) {
			LOGGER.log(Level.ERROR,   NOT_RETURNED_CONNECTION_EXCEPTION_MESSAGE);
		}
		try {
			statement.close ();
		} catch  (SQLException e) {
			LOGGER.log(Level.ERROR,   NOT_CLOSED_STATEMENT_EXCEPTION_MESSAGE);
		}



	}

	/**Returns taken connection to the available connection pool and closes used SQL statement.
	 * @param connection - taken pooled connection
	 * @param statement - used SQL statement 
	 * @param resultSet - used SQL result set
	 */
	public void closeConnection(Connection connection, Statement statement, ResultSet resultSet)  {
		try {
			connection.close();
		} catch  (SQLException e) {
			LOGGER.log( Level.ERROR,  NOT_RETURNED_CONNECTION_EXCEPTION_MESSAGE);
		}
		try { if(resultSet!=null){
			resultSet.close ();
		}
		} catch  (SQLException e) {
			LOGGER.log(Level.ERROR,  NOT_CLOSED_RESULTSET_EXCEPTION_MESSAGE);
		}
		try {
			statement.close();
		} catch  (SQLException e) {
			LOGGER.log(Level.ERROR,   NOT_CLOSED_STATEMENT_EXCEPTION_MESSAGE);
		}
	}
	
	
	/**
	 * Completely clears connection queue
	 */
	public void dispose() {
		clearConnectionQueue();
	}

	/**Initialize connection pool- loads jdbc driver, creates taken and available connection queues and fill available connection queue with SQL connections.
	 * @throws ConnectionPoolException when SQL connection can not be established or jdbc driver can not be loaded.
	 */
	public void initPoolData() throws ConnectionPoolException  {
		try{
			Class.forName(driverName);
			takenConQueue = new ArrayBlockingQueue<>(poolSize);
			availableConQueue = new ArrayBlockingQueue<>(poolSize);
			for (int i = 0; i < poolSize; i++) {
				Connection connection = DriverManager.getConnection(url, user, password);
				PooledConnection pooledConnection = new PooledConnection(connection);
				availableConQueue.add(pooledConnection);
			}
		}
		catch (SQLException	 e) {
			throw new ConnectionPoolException(ON_CREATING_POOL_EXCEPTION_MESSAGE);
		}
		catch (ClassNotFoundException e){
			throw new ConnectionPoolException(NO_DATABASE_DRIVER_CLASS_EXCEPTION_MESSAGE);
		}
	}

	/**Takes connection from connection pool (from available connection queue) 
	 * @return taken from connection pool free connection
	 * @throws ConnectionPoolException
	 */
	public Connection takeConnection() throws ConnectionPoolException {
		Connection connection = null;
		try {
			connection = availableConQueue.take();
			takenConQueue.offer(connection);
		} catch (InterruptedException e) {
			throw new ConnectionPoolException(NO_DATASOURCE_CONNECTION_EXCEPTION_MESSAGE, e);
		}
		return connection;
	}


	private void clearConnectionQueue()  {
		try{
			closeConnectionsQueue(takenConQueue);
			closeConnectionsQueue(availableConQueue);
		} catch (SQLException exception){
			LOGGER.log(Level.ERROR,   NOT_CLOSED_CONNECTION_EXCEPTION_MESSAGE, exception);
		}
	}

	private void closeConnectionsQueue(BlockingQueue<Connection> queue) throws SQLException {
		Connection connection;
		while   ((connection = queue.poll())   != null) {
			if  (!connection.getAutoCommit()) {
				connection.commit();
			}
			((PooledConnection)  connection).reallyClose();
		}
	}
}
