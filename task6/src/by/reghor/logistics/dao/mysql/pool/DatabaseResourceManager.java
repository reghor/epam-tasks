package by.reghor.logistics.dao.mysql.pool;

import java.util.ResourceBundle;

public final class DatabaseResourceManager {
	public static final String DB_DRIVER = "db.driver";
	public static final String DB_PASSWORD = "db.password";
	public static final String DB_POOL_SIZE = "db.poolsize";
	public static final String DB_URL = "db.url";
	public static final String DB_USER = "db.user";
	private static final DatabaseResourceManager instance = new DatabaseResourceManager();
	private static final String DATABASE_BUNDLE_NAME = "database";
	private ResourceBundle bundle = ResourceBundle.getBundle(DATABASE_BUNDLE_NAME);

	private DatabaseResourceManager() {
	
	}

	/**Returns instance of DatabaseResourceManager ( DatabaseResourceManager is singleton class)
	 * @return 
	 */
	public static DatabaseResourceManager getInstance() {
		return instance;
	}

	/**Retrieves info from bundle according to String key;
	 * @param key - String according to which info is gonna be retrieved from bundle 
	 * @return info from bundle according to String key
	 */
	public String getValue(String key) {
		return bundle.getString(key);
	}
}
