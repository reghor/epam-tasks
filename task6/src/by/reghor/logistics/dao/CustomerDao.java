package by.reghor.logistics.dao;

import java.util.List;
import java.util.Map;

import by.reghor.logistics.bean.ReviewedOrder;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.entity.Order;

/**
 * Interface, which describes methods of customer DAO .   
 * @author Hornet
 *
 */
public interface CustomerDao {
	/**Creates a new customer order with some particular parameters.
	 * @param userId - Id of the  current user (stored in the session context, when user is logged in), who creates the order.
	 * @param description - description of the order.
	 * @param maxLoadHeight - maximal order load height. 
	 * @param maxLoadWidth - maximal order load width.
	 * @param maxLoadLength - maximal order load length.
	 * @param weight - total order load weight.
	 * @param volume - total order load volume.
	 * @param departurePointId  - Id of the departure point.
	 * @param arrivalPointId - Id of the arrival point.
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	void createNewOrder(int userId, String description, int maxLoadHeight, int maxLoadWidth, int maxLoadLength, int weight, int volume,
			int departurePointId, int arrivalPointId) throws DaoException;

	/**Deletes customer order
	 * @param userId - Id of the current user (stored in the session context, when user is logged in).
	 * @param orderId - Id of the order
	 * @return true if deletion was successful and false if not.
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	boolean deleteOrder(int userId, int orderId) throws DaoException;

	/**Edits a customer order
	 * @param userId - Id of the  current user (stored in the session context, when user is logged in).
	 * @param orderId - Id of the order.
	 * @param description - description of the order.
	 * @param maxLoadHeight - maximal order load height. 
	 * @param maxLoadWidth - maximal order load width.
	 * @param maxLoadLength - maximal order load length.
	 * @param weight - total order load weight.
	 * @param volume - total order load volume.
	 * @param departurePointId  - Id of the departure point.
	 * @param arrivalPointId - Id of the arrival point.
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	void editOrder(int userId, int orderId, String description, int maxLoadHeight, int maxLoadWidth, int maxLoadLength, int weight, int volume,
			int departurePointId, int arrivalPointId) throws DaoException;

	/**
	 * @param userId - Id of the  current user (stored in the session context, when user is logged in).
	 * @param language - current language of the user session.
	 * @return list of all user orders
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	List<Order> getAllUserOrders(int userId, String language) throws DaoException;

	/**
	 * @param language - current language of the user session.
	 * @return Map of all countries and their Id's from the database
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	Map<String, Integer> getCountries(String language) throws DaoException;

	/**
	 * @param countryId - Country id of which regions should be returned.
	 * @param language - current language of the user session.
	 * @return - Map of all regions and their Id's from the database
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	Map<String, Integer> getCountryReginons(int countryId, String language) throws DaoException;

	/**
	 * @param regionId - Region id of which cities should be returned.
	 * @param language - current language of the user session.
	 * @return Map of all regions and their Id's from the database.
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	Map<String, Integer> getRegionCities(int regionId, String language) throws DaoException;

	/**
	 * @param userId - Id of the  current user (stored in the session context, when user is logged in).
	 * @param orderId Id of the reviewed order by customer.
	 * @param language - current language of the user session.
	 * @return Reviewed order
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	ReviewedOrder reviewOrder(int userId, int orderId, String language) throws DaoException;

}
