package by.reghor.logistics.dao.exception;

/**Exception of DAO layer. Will be thrown when there were exception during executing DAO commands (Exception on using connection pool or statement).
 * @author Hornet
 *
 */
public class DaoException extends Exception {

	private static final long serialVersionUID = 1L;

	public DaoException(String message) {
		super(message);
	}

	public DaoException(String message, Throwable exception) {
		super(message, exception);
	}

	public DaoException(Throwable exception) {
		super(exception);
	}
}
