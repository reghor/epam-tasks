package by.reghor.logistics.dao;

import java.util.Arrays;
import java.util.ResourceBundle;

import by.reghor.logistics.dao.mysql.MySqlDaoFactory;

/**
 * Abstract DAO factory class for creating and retrieving user, customer,
 * dispatcher, driver DAO objects and for detecting current type of DAO.
 * 
 * @author Hornet
 *
 */
public abstract class DaoFactory {

	private static final String DATABASE_BUNDLE_NAME = "database";
	private static final String CURRENT_DAO_ATTRIBUTE = "db.dao.default";

	/**
	 * Method for instantiating and retrieving DAO factory object. In this
	 * method current type of DAO is read from property file.
	 * 
	 * @return instance of DAO factory class.
	 */
	public static DaoFactory getInstance() {
		DaoFactory factory = null;
		ResourceBundle bundle = ResourceBundle.getBundle(DATABASE_BUNDLE_NAME);
		String defaultDaoString = bundle.getString(CURRENT_DAO_ATTRIBUTE);
		DaoType currentDaoType;
		if (Arrays.asList(DaoType.values()).contains(defaultDaoString.toUpperCase())) {
			currentDaoType = DaoType.valueOf(defaultDaoString.toUpperCase());
		} else {
			currentDaoType = DaoType.MYSQL;
		}
		switch (currentDaoType) {
		case MYSQL:
			factory = MySqlDaoFactory.getInstance();
			break;
		default:
		}
		return factory;
	}

	/**
	 * @return user DAO class instance
	 */
	public abstract UserDao getUserDao();

	/**
	 * @return customer DAO class instance
	 */
	public abstract CustomerDao getCustomerDao();

	/**
	 * @return dispatcher DAO class instance
	 */
	public abstract DispatcherDao getDispatcherDao();

	/**
	 * @return driver DAO class instance
	 */
	public abstract DriverDao getDriverDao();
}
