package by.reghor.logistics.dao;

import java.util.List;

import by.reghor.logistics.bean.ReviewedOrder;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.entity.Car;
import by.reghor.logistics.entity.Order;
import by.reghor.logistics.entity.User;

/**Interface, which describes methods of dispatcher DAO .   
 * @author Hornet
 *
 */
public interface DispatcherDao {

	/** Assigns car for transferring order load. 
	 * @param orderId - order's ID, for which car is assigned.
	 * @param carId - car's ID, which is assigned to take car about  order load.
	 * @return returns true if assignment was successful, if not- false
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	boolean assignOrderCar(int orderId, int carId) throws DaoException;

	/** Cancels assignment of order's car which was assigned by dispatcher.
	 * @param orderId - order's ID 
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	void cancelOrderCarAssignment(int orderId) throws DaoException;
	
	/**Cancels assignment of driver's car which was assigned by dispatcher. Marks order status as registered.
	 * @param driverId driver's ID
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	void cancelDriverCarAssignment(int driverId) throws DaoException;
	
	/**Declines order created by customer. Marks order status as declined. 
	 * @param orderId - order's ID 
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	void declineOrder(int orderId) throws DaoException;

	/**Deletes a car from database by it ID.
	 * @param carId - car's ID
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	boolean deleteCar(int carId) throws DaoException;

	/**Gets all cars stored in the database.
	 * @return list of all cars
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	List<Car> getAllCars() throws DaoException;

	/**Gets all  free cars (with free status) stored in the database.
	 * @return list of all free cars
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	List<Car> getFreeCars() throws DaoException;

	/**Gets all drivers registered in the database.
	 * @return list of all drivers
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	List<User> getAllDrivers() throws DaoException;

	/**Assigns a car for a driver.
	 * @param driverId - driver's ID
	 * @param carId - car's ID
	 * @return true if assignment was successful and false if not
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	boolean assignDriverCar(int driverId, int carId) throws DaoException;

	/**Gets all existing orders in the database.
	 * @param language - current language of the current user session
	 * @return list of all existing orders
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	List<Order> getAllOrders(String language) throws DaoException;

	/**Gets driver's car which was assigned by dispatcher. 
	 * @param driverId - driver's id (user's id to which assigned car)
	 * @return assigned car {@link by.reghor.logistics.entity.Car}
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	Car getDriverCar(int driverId) throws DaoException;

	/** Adds new car to the database.
	 * @param carNumber - car's number
	 * @param makeOfCar - make of car
	 * @param carColour - car color
	 * @param carYear - model year of car
	 * @param carMaxHeight - max height of load that can be put inside the car
	 * @param carMaxLength - max length of load that can be put inside the car
	 * @param carMaxWidth - max width of load that can be put inside the car
	 * @param carMaxVolume - max volume of load that can be put inside the car
	 * @param carMaxWeight - max weight of load that can be put inside the car
	 * @return returns true if car's number is unique and car was successfully added 
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	boolean addNewCar(String carNumber, String makeOfCar, String carColour, int carYear, int carMaxHeight, int carMaxLength, int carMaxWidth,
			int carMaxVolume, int carMaxWeight) throws DaoException;

	/**Gets suitable cars for assignment according to entered parameters. 
	 * @param maxHeight - max height of the load for which car is picked
	 * @param maxLength - max length of the load for which car is picked
	 * @param maxWidth - max width of the load for which car is picked
	 * @param volume - volume of the load for which car is picked
	 * @param weight - weight of the load for which car is picked
	 * @return list of suitable cars for order load
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	List<Car> getSuitableCarsForAssign(int maxHeight, int maxLength, int maxWidth, int volume, int weight) throws DaoException;

	/**Retrieves order (information about car, order, driver) by id and language from database for review. 
	 * @param orderId - order's id 
	 * @param language current session language (stored in session context)
	 * @return reviewed order object from database by ID
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	ReviewedOrder reviewOrder(int orderId, String language) throws DaoException;

}
