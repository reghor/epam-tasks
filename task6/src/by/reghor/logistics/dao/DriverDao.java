package by.reghor.logistics.dao;

import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.entity.Car;
import by.reghor.logistics.entity.Order;

public interface DriverDao {

	/**Accepts order for executing after it was assigned by dispatcher for driver. Marks order status as accepted.
	 * @param orderId - order ID 
	 * @param userId - driver ID (current user with role driver) 
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	void acceptOrder(int orderId, int userId) throws DaoException;

	/**Accomplishes driver's order which he has accepted. Marks order status as accomplished.
	 * @param orderId - order ID
	 * @param userId - driver ID (current user with role driver) 
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	void accomplishOrder(int orderId, int userId) throws DaoException;

	/**Gets driver car by driver's id. 
	 * @param driverId - driver ID (current user with role driver) 
	 * @return driver's car object
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	Car getDriverCar(int driverId) throws DaoException;

	/**Gets driver order by id of driver
	 * @param driverId - driver ID (current user with role driver) 
	 * @param language - current language of session
	 * @return driver's order
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	Order getDriverOrder(int driverId, String language) throws DaoException;

	/**Marks driver's car as faultless. 
	 * @param carId - ID of driver's car
	 * @param userId - driver ID (current user with role driver) 
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	void makeCarFaultless(int carId, int userId) throws DaoException;

	/**Marks driver's car as faulty. 
	 * @param carId - ID of driver's car
	 * @param userId - driver ID (current user with role driver) 
	 * @throws DaoException when problems happened while executing sql statement or problems with connection
	 */
	boolean makeCarFaulty(int carId, int userId) throws DaoException;
}
