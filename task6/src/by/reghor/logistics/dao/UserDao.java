package by.reghor.logistics.dao;

import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.entity.User;

public interface UserDao {

	/**Authorizes user by login and password
	 * @param login - user entered login
	 * @param password - user entered password
	 * @return if login was successful return User object according to user login and password
	 * @throws DaoException
	 */
	User authorizeUser(String login, String password) throws DaoException;

	/**Registers new customer in the database according to entered parameters, if not - null.
	 * @param login - new customer login
	 * @param firstName - new customer first name
	 * @param lastName - new customer last name
	 * @param password - new customer password
	 * @param email - new customer email address
	 * @param phoneNumber - new customer phone number
	 * @return true if not registered user entered unique login, email, phone number and registers new customer, otherwise returns false and doesn't register new customer
	 * @throws DaoException when problems happened while executing sql statement or problems with connection or problems with password hasher
	 */
	boolean registerCustomer(String login, String firstName, String lastName, String password, String email, String phoneNumber) throws DaoException;

	/**Registers new driver in the database according to entered parameters.
	 * @param login - new driver login
	 * @param firstName - new driver first name
	 * @param lastName - new driver last name
	 * @param password - new driver password
	 * @param email - new driver email address
	 * @param phoneNumber - new driver phone number
	 * @return true if dispatcher entered unique login, email, phone number and registers new driver, otherwise returns false and doesn't register new driver
	 * @throws DaoException when problems happened while executing sql statement or problems with connection or problems with password hasher
	 */
	boolean registerDriver(String login, String firstName, String lastName, String password, String email, String phoneNumber) throws DaoException;

}
