package by.reghor.logistics.dao;


/** Enum containing types of DAO (MySQL DAO, Oracle DAO etc.).
 * @author Hornet
 *
 */
public enum DaoType {
	MYSQL
}
