package by.reghor.logistics.listener;

import java.util.ResourceBundle;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import by.reghor.logistics.entity.User;
import by.reghor.logistics.entity.UserRole;

@WebListener
public class SessionCreationListener implements HttpSessionListener {

	private static final String CONFIG_BUNDLE_NAME = "config";
	private static final String DEFAULT_LANGUAGE_ATTRIBUTE = "default.language";
	private static final String USER_ATTRIBUTE = "user";
	private static final String LOCAL_ATTRIBUTE = "local";
	private static final Lock lock = new ReentrantLock();

	@Override
	public void sessionCreated(HttpSessionEvent httpSessionEvent) {
		lock.lock();
		HttpSession session = httpSessionEvent.getSession();
		ResourceBundle bundle = ResourceBundle.getBundle(CONFIG_BUNDLE_NAME);
		session.setAttribute(LOCAL_ATTRIBUTE, bundle.getString(DEFAULT_LANGUAGE_ATTRIBUTE));
		User user = new User();
		user.setRole(UserRole.GUEST);
		session.setAttribute(USER_ATTRIBUTE, user);
		lock.unlock();
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {

	}
}
