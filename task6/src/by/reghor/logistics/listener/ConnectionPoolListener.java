package by.reghor.logistics.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.reghor.logistics.dao.mysql.pool.ConnectionPool;
import by.reghor.logistics.dao.mysql.pool.exception.ConnectionPoolException;

@WebListener
public class ConnectionPoolListener implements ServletContextListener {

	private static final Logger LOGGER = LogManager.getLogger(ConnectionPoolListener.class.getName());
	private static final String CONNECTION_POOL_INIT_EXCEPTION_MESSAGE = "Connection pool initialization Exception";

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		ConnectionPool.getInstance().dispose();
	}

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {

		ConnectionPool connectionPool = ConnectionPool.getInstance();
		try {
			connectionPool.initPoolData();
		} catch (ConnectionPoolException e) {
			LOGGER.fatal(CONNECTION_POOL_INIT_EXCEPTION_MESSAGE, e);
			throw new RuntimeException(CONNECTION_POOL_INIT_EXCEPTION_MESSAGE);
		}

	}
}
