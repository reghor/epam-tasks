package by.reghor.logistics.bean;

import java.io.Serializable;

import by.reghor.logistics.entity.Car;
import by.reghor.logistics.entity.Order;
import by.reghor.logistics.entity.User;

/**
 * Java bean class which represents order reviewed by some user. This class
 * contains information about some particular order, also car and driver
 * assigned for this order by dispatcher. Contains setters and getters methods
 * for storing and returning car, driver and order objects with information
 * about reviewed order.
 * 
 * @author Hornet
 *
 *
 */

public class ReviewedOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	private Car car = new Car();

	private User driver = new User();

	private Order order = new Order();

	/**
	 * Returns assigned car of order reviewed by user
	 * 
	 * @return reviewed order car 
	 */
	public Car getCar() {
		return car;
	}

	/**
	 * Returns assigned driver of order reviewed by user
	 * 
	 * @return reviewed order driver(user object)
	 */
	public User getDriver() {
		return driver;
	}

	/**
	 * Returns order which is reviewed by user
	 * 
	 * @return order 
	 */
	public Order getOrder() {
		return order;
	}

	/**
	 * Sets car which is assigned for reviewed order
	 * 
	 * @param car
	 *            assigned car 
	 */
	public void setCar(Car car) {
		this.car = car;
	}

	/**
	 * Sets driver which is assigned for reviewed order
	 * 
	 * @param driver
	 *            driver(user object) 
	 */
	public void setDriver(User driver) {
		this.driver = driver;
	}

	/**
	 * Sets order which is reviewed by user
	 * 
	 * @param order
	 *            order 
	 */
	public void setOrder(Order order) {
		this.order = order;
	}

}
