package by.reghor.logistics.service.ajax;

import java.util.HashMap;
import java.util.Map;

import by.reghor.logistics.service.ajax.impl.GetAllOrdersAjax;
import by.reghor.logistics.service.ajax.impl.GetCountriesAjax;
import by.reghor.logistics.service.ajax.impl.GetCountryRegionsAjax;
import by.reghor.logistics.service.ajax.impl.GetCustomerOrdersAjax;
import by.reghor.logistics.service.ajax.impl.GetRegionCitiesAjax;

/**Class for storing and retrieving Ajax commands. 
 * @author Hornet
 *
 */
public final class AjaxCommandRetriever {

	private static final AjaxCommandRetriever instance = new AjaxCommandRetriever();
	private final Map<AjaxCommandName, AjaxCommand> commands = new HashMap<AjaxCommandName, AjaxCommand>();

	private AjaxCommandRetriever() {
		commands.put(AjaxCommandName.GET_ALL_ORDERS_AJAX, new GetAllOrdersAjax());
		commands.put(AjaxCommandName.GET_COUNTRIES_AJAX, new GetCountriesAjax());
		commands.put(AjaxCommandName.GET_COUNTRY_REGIONS_AJAX, new GetCountryRegionsAjax());
		commands.put(AjaxCommandName.GET_REGION_CITIES_AJAX, new GetRegionCitiesAjax());
		commands.put(AjaxCommandName.GET_ALL_CUSTOMER_ORDERS_AJAX, new GetCustomerOrdersAjax());
	}

	/**Retrieves instance of Ajax command retriever
	 * @return instance of Ajax command retriever
	 */
	public static AjaxCommandRetriever getInstance() {
		return instance;
	}

	/**Gets Ajax command object according to it's name
	 * @param commandName Ajax command name 
	 * @return Ajax command object
	 */
	public AjaxCommand getAjax(String commandName) {
		AjaxCommandName name = AjaxCommandName.valueOf(commandName.toUpperCase());
		AjaxCommand command = commands.get(name);
		return command;
	}

}
