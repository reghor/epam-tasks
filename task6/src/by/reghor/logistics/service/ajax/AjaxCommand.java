package by.reghor.logistics.service.ajax;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.service.ajax.exception.AjaxCommandException;

/**Interface describing Ajax command methods.
 * @author Hornet
 *
 */
public interface AjaxCommand {

	void execute(HttpServletRequest request, HttpServletResponse response) throws AjaxCommandException;

}
