package by.reghor.logistics.service.ajax.impl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.CustomerDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.dao.mysql.MySqlCustomerDao;
import by.reghor.logistics.service.ajax.AjaxCommand;
import by.reghor.logistics.service.ajax.exception.AjaxCommandException;
import by.reghor.logistics.util.AttributeDigester;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**Class for retrieving city names and IDs according to region ID   and converting them to JSON fromat.
 * @author Hornet
 *
 */
public class GetRegionCitiesAjax implements AjaxCommand {
	private static final String JSON_CONTENT_TYPE = "application/json;charset=utf-8";
	private static final String LOCAL_ATTRIBUTE = "local";
	private static final String SUCCESS_ATTRIBUTE = "success";
	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";
	private static final String GET_WRITER_EXCEPTION_MESSAGE = "Can not get writer";
	private static final String WRONG_REGION_ID_EXCEPTION_MESSAGE = "Wrong region id";
	private static final String REGION_ID_ATTRIBUTE = "region_id";
	private static final String CITY_LIST_ATTRIBUTE = "cityContainer";

	/**
	 * Retrieves city names and IDs according to the specified region ID
	 * stored in request object. Then converts them in JSON format and store
	 * them in response object. Calls
	 * {@link by.reghor.logistics.dao.CustomerDao#getCountryReginons(int, String) getCountryReginons} method.
	 * 
	 * @param request
	 *            request object
	 * @param response
	 *            response object
	 * @throws AjaxCommandException
	 *             when exception is thrown while executing DAO method or can
	 *             not get writer from response.
	 */
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws AjaxCommandException {
		response.setContentType(JSON_CONTENT_TYPE);
		PrintWriter out = null;
		try {
			out = response.getWriter();
			String stringRegionId = request.getParameter(REGION_ID_ATTRIBUTE);
			CustomerDao customerDao = MySqlCustomerDao.getInstance();
			Map<String, Integer> citiesMap = null;
			int regionId = Integer.parseInt(stringRegionId);
			citiesMap = customerDao.getRegionCities(regionId, AttributeDigester.getCurrentLocale(request));
			JsonObject jsonObject = new JsonObject();
			if (citiesMap != null) {
				Gson gson = new Gson();
				jsonObject.addProperty(SUCCESS_ATTRIBUTE, true);
				JsonElement cityListElement = gson.toJsonTree(citiesMap);
				jsonObject.add(CITY_LIST_ATTRIBUTE, cityListElement);
			} else {
				jsonObject.addProperty(SUCCESS_ATTRIBUTE, false);
			}
			out.println(jsonObject.toString());
		} catch (IOException e) {
			throw new AjaxCommandException(GET_WRITER_EXCEPTION_MESSAGE, e);
		} catch (DaoException e) {
			throw new AjaxCommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
		} catch (NumberFormatException e) {
			throw new AjaxCommandException(WRONG_REGION_ID_EXCEPTION_MESSAGE, e);
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

}
