package by.reghor.logistics.service.ajax.impl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.DispatcherDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.entity.Order;
import by.reghor.logistics.service.ajax.AjaxCommand;
import by.reghor.logistics.service.ajax.exception.AjaxCommandException;
import by.reghor.logistics.util.AttributeDigester;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**Class for retrieving all orders according to AJAX query, converting them into JSON format.
 * @author Hornet
 *
 */
public class GetAllOrdersAjax implements AjaxCommand {

	private static final String JSON_CONTENT_TYPE = "application/json;charset=utf-8";
	private static final String SUCCESS_ATTRIBUTE = "success";
	private static final String DATA_ATTRIBUTE = "data";
	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";
	private static final String GET_WRITER_EXCEPTION_MESSAGE = "Can not get writer";

	/** Retrieves all orders and serialize them in JSON format and stores in response object. Calls  {@link by.reghor.logistics.dao.DispatcherDao#getAllOrders(String) getAllOrders} method.
	 * @param request request object
	 * @param response response object
	 * @throws AjaxCommandException when exception is thrown while executing DAO method or can not get writer from response.
	 */
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws AjaxCommandException {
		PrintWriter out = null;
		try {
			DispatcherDao dispatcherDao = DaoFactory.getInstance().getDispatcherDao();
			List<Order> orderList = dispatcherDao.getAllOrders(AttributeDigester.getCurrentLocale(request));
			out = response.getWriter();
			response.setContentType(JSON_CONTENT_TYPE);
			JsonObject jsonObject = new JsonObject();
			if (orderList != null) {
				Gson gson = new Gson();
				jsonObject.addProperty(SUCCESS_ATTRIBUTE, true);
				JsonElement orderListElement = gson.toJsonTree(orderList);
				jsonObject.add(DATA_ATTRIBUTE, orderListElement);
			} else {
				jsonObject.addProperty(SUCCESS_ATTRIBUTE, false);
			}
			out.println(jsonObject.toString());
		} catch (DaoException e) {
			throw new AjaxCommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
		} catch (IOException e) {
			throw new AjaxCommandException(GET_WRITER_EXCEPTION_MESSAGE, e);
		} finally {
			if (out != null) {
				out.close();
			}
		}

	}
}
