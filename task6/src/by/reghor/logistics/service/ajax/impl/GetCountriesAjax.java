package by.reghor.logistics.service.ajax.impl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.CustomerDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.dao.mysql.MySqlCustomerDao;
import by.reghor.logistics.service.ajax.AjaxCommand;
import by.reghor.logistics.service.ajax.exception.AjaxCommandException;
import by.reghor.logistics.util.AttributeDigester;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**Class for retrieving according to AJAX query all countries with their IDs and converting them into JSON format.
 * @author Hornet
 *
 */
public class GetCountriesAjax implements AjaxCommand {
	private static final String JSON_CONTENT_TYPE = "application/json;charset=utf-8";
	private static final String SUCCESS_ATTRIBUTE = "success";
	private static final String COUNTRY_LIST_ATTRIBUTE = "countryContainer";
	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";
	private static final String GET_WRITER_EXCEPTION_MESSAGE = "Can not get writer";

	/**Retrieves all countries with their IDs converts them in JSON format  and stores in response object. Calls {@link by.reghor.logistics.dao.CustomerDao#getCountries(String) getCountries} method.
	 * @param request request object
	 * @param response response object
	 * @throws AjaxCommandException when exception is thrown while executing DAO method or can not get writer from response.
	 */
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws AjaxCommandException {
		response.setContentType(JSON_CONTENT_TYPE);
		JsonObject jsonObject = new JsonObject();
		CustomerDao customerDao = MySqlCustomerDao.getInstance();
		Map<String, Integer> countriesMap = null;
		PrintWriter out = null;
		try {
			countriesMap = customerDao.getCountries(AttributeDigester.getCurrentLocale(request));
			if (countriesMap != null) {
				Gson gson = new Gson();
				jsonObject.addProperty(SUCCESS_ATTRIBUTE, true);
				JsonElement countryObj = gson.toJsonTree(countriesMap);
				jsonObject.add(COUNTRY_LIST_ATTRIBUTE, countryObj);
			} else {
				jsonObject.addProperty(SUCCESS_ATTRIBUTE, false);
			}
			out = response.getWriter();
			out.println(jsonObject.toString());
		} catch (IOException e) {
			throw new AjaxCommandException(DAO_COMMAND_EXCEPTION_MESSAGE,e);
		} catch (DaoException e) {
			throw new AjaxCommandException(GET_WRITER_EXCEPTION_MESSAGE,e);
		} finally {
			if (out != null) {
				out.close();
			}
		}

	}

}
