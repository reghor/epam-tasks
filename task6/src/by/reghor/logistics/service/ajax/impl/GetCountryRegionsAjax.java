package by.reghor.logistics.service.ajax.impl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.CustomerDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.dao.mysql.MySqlCustomerDao;
import by.reghor.logistics.service.ajax.AjaxCommand;
import by.reghor.logistics.service.ajax.exception.AjaxCommandException;
import by.reghor.logistics.util.AttributeDigester;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Class for retrieving region names and IDs according to the country ID
 * and converting them into JSON format
 * 
 * @author Hornet
 *
 */
public class GetCountryRegionsAjax implements AjaxCommand {
	private static final String JSON_CONTENT_TYPE = "application/json;charset=utf-8";
	private static final String SUCCESS_ATTRIBUTE = "success";
	private static final String REGION_LIST_ATTRIBUTE = "regionContainer";
	private static final String COUNTRY_ID_ATTRIBUTE = "country_id";
	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";
	private static final String GET_WRITER_EXCEPTION_MESSAGE = "Can not get writer";
	private static final String WRONG_COUNTRY_ID_EXCEPTION_MESSAGE = "Wrong country id";

	/**
	 * Retrieves region names and IDs according to the specified country ID
	 * stored in request object. Then converts them in JSON format and store
	 * them in response object. Calls
	 * {@link by.reghor.logistics.dao.CustomerDao#getCountryReginons(int, String)} method.
	 * 
	 * @param request
	 *            request object
	 * @param response
	 *            response object
	 * @throws AjaxCommandException
	 *             when exception is thrown while executing DAO method or can
	 *             not get writer from response.
	 */
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws AjaxCommandException {
		response.setContentType(JSON_CONTENT_TYPE);
		String stringCountryId = request.getParameter(COUNTRY_ID_ATTRIBUTE);
		PrintWriter out = null;
		try {
			out = response.getWriter();
			JsonObject jsonObj = new JsonObject();
			CustomerDao customerDao = MySqlCustomerDao.getInstance();
			Map<String, Integer> regionsMap = null;
			int countryId = Integer.parseInt(stringCountryId);
			regionsMap = customerDao.getCountryReginons(countryId, AttributeDigester.getCurrentLocale(request));
			if (regionsMap != null) {
				Gson gson = new Gson();
				JsonElement regionObj = gson.toJsonTree(regionsMap);
				jsonObj.add(REGION_LIST_ATTRIBUTE, regionObj);
				jsonObj.addProperty(SUCCESS_ATTRIBUTE, true);
			} else {
				jsonObj.addProperty(SUCCESS_ATTRIBUTE, false);
			}
			out.println(jsonObj.toString());
		} catch (IOException e) {
			throw new AjaxCommandException(GET_WRITER_EXCEPTION_MESSAGE, e);
		} catch (DaoException e) {
			throw new AjaxCommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
		} catch (NumberFormatException e) {
			throw new AjaxCommandException(WRONG_COUNTRY_ID_EXCEPTION_MESSAGE, e);
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

}
