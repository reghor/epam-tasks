package by.reghor.logistics.service.ajax.exception;

/**Exception class. Will be thrown when some problems happened while executing Ajax command.
 * @author Hornet
 *
 */
public class AjaxCommandException extends Exception {

	private static final long serialVersionUID = 1L;

	public AjaxCommandException() {

	}

	public AjaxCommandException(String message) {
		super(message);
	}

	public AjaxCommandException(String message, Throwable exception) {
		super(message, exception);
	}

	public AjaxCommandException(Throwable exception) {
		super(exception);
	}
}
