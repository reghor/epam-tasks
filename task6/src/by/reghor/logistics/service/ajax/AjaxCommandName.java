package by.reghor.logistics.service.ajax;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import by.reghor.logistics.entity.UserRole;

/**Enum representing names of  Ajax commands and their permissions
 * @author Hornet
 *
 */
public enum AjaxCommandName {

	GET_ALL_CUSTOMER_ORDERS_AJAX(UserRole.CUSTOMER), GET_ALL_ORDERS_AJAX(UserRole.DISPATCHER), 
	GET_COUNTRIES_AJAX(UserRole.DISPATCHER,	UserRole.CUSTOMER), GET_COUNTRY_REGIONS_AJAX(UserRole.DISPATCHER, UserRole.CUSTOMER),
	GET_REGION_CITIES_AJAX(UserRole.DISPATCHER,	UserRole.CUSTOMER);

	private final Set<UserRole> permittedUsers = new HashSet<UserRole>();

	/**Constructor for creating command name with user permissions
	 * @param permittedUserRoles array of permitted user roles. If empty - all users are allowed, if one - only this one is permitted.
	 */
	AjaxCommandName(UserRole... permittedUserRoles) {
		permittedUsers.addAll(Arrays.asList(permittedUserRoles));
	}

	/**Gets permitted user roles
	 * @return permitted user roles
	 */
	public Set<UserRole> getPermittedUserRoles() {
		return Collections.unmodifiableSet(permittedUsers);
	}
}
