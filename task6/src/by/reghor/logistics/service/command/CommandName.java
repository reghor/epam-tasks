package by.reghor.logistics.service.command;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import by.reghor.logistics.entity.UserRole;

/**Enum containing command names and permissions.
 * @author Hornet
 *
 */
public enum CommandName {
	
	ACCEPT_ORDER(UserRole.DRIVER), 
	ACCOMPLISH_ORDER(UserRole.DRIVER),
	ADD_NEW_CAR_PAGE_REFERENCE(UserRole.DISPATCHER), 
	ADD_NEW_CAR(UserRole.DISPATCHER), 
	ASSIGN_ORDER_CAR(UserRole.DISPATCHER),
	ASSIGN_ORDER_CAR_PICK(UserRole.DISPATCHER),
	ASSIGN_DRIVER_CAR(UserRole.DISPATCHER),
	CANCEL_ASSIGNMENT(UserRole.DISPATCHER), 
	CANCEL_CAR_ASSIGNMENT(UserRole.DISPATCHER),
	CREATE_NEW_ORDER(UserRole.CUSTOMER),
	CREATE_NEW_ORDER_REFERENCE(UserRole.CUSTOMER),
	CUSTOMER_MAIN_PAGE_REFERENCE(UserRole.CUSTOMER),
	DECLINE_ORDER(UserRole.DISPATCHER),
	DELETE_CAR(UserRole.DISPATCHER),
	DELETE_ORDER(UserRole.CUSTOMER),
	DISPATCHER_MAIN_PAGE_REFERENCE(UserRole.DISPATCHER),
	DRIVER_MAIN_PAGE_REFERENCE(UserRole.DRIVER),
	EDIT_ORDER(UserRole.CUSTOMER),
	EDIT_ORDER_PAGE(UserRole.CUSTOMER),
	EMPTY_COMMAND(),
	GET_ALL_CARS(UserRole.DISPATCHER),
	GET_ALL_DRIVERS(UserRole.DISPATCHER), 
	GET_FREE_CARS(UserRole.DISPATCHER),
	GET_DRIVER_CAR(UserRole.DISPATCHER, UserRole.DRIVER),
	LOCALIZATION(), 
	LOGIN(), 
	LOGIN_PAGE_REFERENCE(), 
	LOGOUT(),
	MAKE_CAR_FAULTLESS(UserRole.DRIVER), 
	MAKE_CAR_FAULTY(UserRole.DRIVER),
	REGISTER_DRIVER(UserRole.DISPATCHER), 
	REGISTER_DRIVER_PAGE_REFERENCE(UserRole.DISPATCHER),
	REGISTER_PAGE_REFERENCE(), 
	REGISTER_USER(),
	REVIEW_ORDER(UserRole.DISPATCHER, UserRole.CUSTOMER), 
	SHOW_ALL_ORDERS_CUSTOMER(UserRole.CUSTOMER), 
	SHOW_ALL_ORDERS_DISPATCHER(UserRole.DISPATCHER),
	SHOW_DRIVER_ORDER(UserRole.DRIVER),
	WELCOME_PAGE_REFERENCE();
	
	
	private final Set<UserRole> PERMITTED_USERS=new HashSet<>();
	
	/**Constructor for creating command permissions 
	 * @param permittedUserRoles permitted user roles for command. If empty - all users are allowed, if one or more- only one or all of specified are permitted.
	 */
	CommandName(UserRole... permittedUserRoles){
		PERMITTED_USERS.addAll(Arrays.asList(permittedUserRoles));
	}
	
	/**Gets permitted user roles
	 * @return gets permitted user roles
	 */
	public Set<UserRole> getPermittedUserRoles(){
		return Collections.unmodifiableSet(PERMITTED_USERS);
	}
}
