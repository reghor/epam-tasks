package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.DispatcherDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.CommandName;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.AttributeDigester;
import by.reghor.logistics.util.PageManager;

/**Allows to decline order by dispatcher. Reads order ID from request object.
 * @author Hornet
 *
 */
public class DeclineOrder implements Command {
	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";
	
	/**
	 * Reads new order parameters from request object and calls
	 * {@link by.reghor.logistics.dao.DispatcherDao#declineOrder(int)
	 * declineOrder} DAO method.
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return request url to command
	 *         {@link by.reghor.logistics.service.command.impl.ReviewOrder
	 *         ReviewOrderDispatcher} 
	 *         CreateNewOrder}
	 * @throws CommandException
	 *             when something wrong went while executing DAO method or wrong
	 *            order ID specified in the request object.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		
		String page = PageManager.getInstance().generateCommandRequestNoSuchOrder(CommandName.SHOW_ALL_ORDERS_DISPATCHER);
		try {
			if (AttributeDigester.isValidOrderId(request)){
				DispatcherDao dispatcherDao = DaoFactory.getInstance().getDispatcherDao();
				int orderId =AttributeDigester.getOrderId(request);
				dispatcherDao.declineOrder(orderId);
				page = PageManager.getInstance().generateReviewOrderRequest(orderId);
			}
		} catch (DaoException e) {
			throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
		}
		return page;
	}

}
