package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.CommandName;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.AttributeDigester;
import by.reghor.logistics.util.PageManager;

/**Changes locale of user session according to specified language name in request object.
 * @author Hornet
 *
 */
public class Localization implements Command {

	private static final String LOCAL_ATTRIBUTE = "local";
	private static final String CONTROLLER_QUERY_STRING = "/controller?";
	/**
	 * Changes locale according to specified language in request object.
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return last user url during session containing string, if no such url found- login page url request.
	 * @throws CommandException
	 *             no exception will be thrown.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        request.getSession().setAttribute(LOCAL_ATTRIBUTE, request.getParameter(LOCAL_ATTRIBUTE));
        String lastQueryString=AttributeDigester.getLastQueryString(request);
        String page=null;
        if (lastQueryString!=null){
        	 page=CONTROLLER_QUERY_STRING+lastQueryString;
        }else{
        	page=PageManager.getInstance().generateCommandRequest(CommandName.LOGIN_PAGE_REFERENCE);
        }
        return page;
	}
}
