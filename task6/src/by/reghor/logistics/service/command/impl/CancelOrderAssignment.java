package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.DispatcherDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.CommandName;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.AttributeDigester;
import by.reghor.logistics.util.PageManager;

public class CancelOrderAssignment implements Command {

	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		String page = PageManager.getInstance().generateCommandRequestNoSuchOrder(CommandName.SHOW_ALL_ORDERS_DISPATCHER);
		try {
			
			if (AttributeDigester.isValidOrderId(request)){
				DispatcherDao dispatcherDao = DaoFactory.getInstance().getDispatcherDao();
				int orderId = AttributeDigester.getOrderId(request);
				dispatcherDao.cancelOrderCarAssignment(orderId);
				page = PageManager.getInstance().generateReviewOrderRequest(orderId);
			}
			
		} catch (DaoException e) {
			throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
		}
		return page;
	}
}
