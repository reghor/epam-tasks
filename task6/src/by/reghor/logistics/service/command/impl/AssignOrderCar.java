package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.DispatcherDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.CommandName;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.AttributeDigester;
import by.reghor.logistics.util.PageManager;

/**
 * Assigns a free car with driver to a registered order. Reads car and order data.
 * 
 * @author Hornet
 *
 */
public class AssignOrderCar implements Command {

	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";

	/**
	 * Reads data about car from the request object and calls
	 * {@link by.reghor.logistics.dao.DispatcherDao#assignOrderCar(int, int)
	 * assignOrderCar} DAO method.
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return request url to command
	 *         {@link by.reghor.logistics.service.command.impl.ReviewOrder
	 *         ReviewOrderDispatcher} if assignment went ok with parameter
	 *         success set true, if not with parameter false.
	 * @throws CommandException
	 *             when something wrong went while executing DAO method .
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		String page = PageManager.getInstance().generateCommandRequestNoSuchCar(CommandName.SHOW_ALL_ORDERS_DISPATCHER);
		try {
			DispatcherDao dispatcherDao = DaoFactory.getInstance().getDispatcherDao();
			if (AttributeDigester.isValidOrderId(request)&&AttributeDigester.isValidCarId(request)){
				int carId = AttributeDigester.getCarId(request);
				int orderId = AttributeDigester.getOrderId(request);
				if (dispatcherDao.assignOrderCar(orderId, carId)) {
					page = PageManager.getInstance().generateReviewOrderRequest(orderId);
				} 
			}
			
		} catch (DaoException e) {
			throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
		} 
		return page;
	}
}