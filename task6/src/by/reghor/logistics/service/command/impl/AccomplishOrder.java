package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.DriverDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.entity.User;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.CommandName;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.AttributeDigester;
import by.reghor.logistics.util.PageManager;

/**
 * Class allows to accomplish order
 * 
 * @author Hornet
 *
 */
public class AccomplishOrder implements Command {
	private static final String USER_ATTRIBUTE = "user";
	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";


	/**
	 * Accomplishes user order, which was accepted by driver. Reads order ID to
	 * be accepted. Calls
	 * {@link by.reghor.logistics.dao.DriverDao#accomplishOrder(int, int)
	 * accomplishOrder} method to finish operation.
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return request url on
	 *         {@link by.reghor.logistics.service.command.impl.GetDriverCarDriver
	 *         GetDriverCarDriver} command
	 * @throws CommandException
	 *             when something wrong went while executing DAO method or ID of
	 *             order specified in request was wrong.
	 */

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

		try {
			User user = (User) request.getSession().getAttribute(USER_ATTRIBUTE);
			int userId = user.getId();
			if (AttributeDigester.isValidOrderId(request)) {
				int orderId = AttributeDigester.getOrderId(request);
				DriverDao driverDao = DaoFactory.getInstance().getDriverDao();
				driverDao.accomplishOrder(orderId, userId);
			}

		} catch (DaoException e) {
			throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
		} 
		return PageManager.getInstance().generateCommandRequest(CommandName.SHOW_DRIVER_ORDER);
	}
}
