package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.CustomerDao;
import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.CommandName;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.AttributeDigester;
import by.reghor.logistics.util.PageManager;

/**Deletes specified order according to read order ID. Order can be declined, registered and accomplished.
 * @author Hornet
 *
 */
public class DeleteOrder implements Command {
	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";
	/**
	 * Reads order ID from request object, deletes order by ID by calling
	 * {@link by.reghor.logistics.dao.CustomerDao.deleteOrder(int, int)
	 * deleteOrder} DAO method.
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return request url to command
	 *         {@link by.reghor.logistics.service.command.impl.GetAllCustomerOrdersPage
	 *         GetAllCustomerOrdersPage} 
	 * @throws CommandException
	 *             when something wrong went while executing DAO method or wrong
	 *            car ID was specified in the request object.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

		String page = PageManager.getInstance().generateCommandRequestNotSuccessful(CommandName.SHOW_ALL_ORDERS_CUSTOMER);
		try {
			CustomerDao customerDao = DaoFactory.getInstance().getCustomerDao();
			int userId = AttributeDigester.getCurrentUserId(request);
			if (AttributeDigester.isValidOrderId(request)){
				int orderId=AttributeDigester.getOrderId(request);
				if (customerDao.deleteOrder(userId, orderId)){
					page = PageManager.getInstance().generateCommandRequestSuccessful(CommandName.SHOW_ALL_ORDERS_CUSTOMER);
				}
			}
		} catch (DaoException e) {
			throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
		}
		return page;
	}
}
