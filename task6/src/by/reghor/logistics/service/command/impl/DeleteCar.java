package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.DispatcherDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.CommandName;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.AttributeDigester;
import by.reghor.logistics.util.PageManager;

/**
 * Deletes a car. Reads car ID from request object.
 * 
 * @author Hornet
 *
 */
public class DeleteCar implements Command {

	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";

	/**
	 * Reads car ID from request object, deletes car by ID by calling
	 * {@link by.reghor.logistics.dao#DispatcherDao.deleteCar(int) deleteCar}
	 * DAO method.
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return request url to command
	 *         {@link by.reghor.logistics.service.command.impl.GetAllCars
	 *         GetAllCars}
	 * @throws CommandException
	 *             when something wrong went while executing DAO method or wrong
	 *             car ID was specified in the request object.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		String page = PageManager.getInstance().generateCommandRequestNotSuccessful(CommandName.GET_ALL_CARS);
		try {
			if (AttributeDigester.isValidCarId(request)) {
				int carId = AttributeDigester.getCarId(request);
				DispatcherDao dispatcherDao = DaoFactory.getInstance().getDispatcherDao();
				if (dispatcherDao.deleteCar(carId)) {
					page = PageManager.getInstance().generateCommandRequestSuccessful(CommandName.GET_ALL_CARS);
				}
			}
		} catch (DaoException e) {
			throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
		}
		return page;
	}
}
