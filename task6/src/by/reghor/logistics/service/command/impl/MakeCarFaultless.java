package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.DriverDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.CommandName;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.AttributeDigester;
import by.reghor.logistics.util.PageManager;

/**
 * Marks car condition as faultless. By default cars are marked as faulty.
 * 
 * @author Hornet
 *
 */
public class MakeCarFaultless implements Command {
	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";

	/**
	 * Marks car condition as faultless. Reads car ID as attribute of request
	 * object and calls {@link
	 * by.reghor.logistics.dao.DriverDao.makeCarFaultless(int, int)
	 * makeCarFaultless} DAO method.
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return request url to command
	 *         {@link by.reghor.logistics.service.command.impl.GetDriverCarDriver
	 *         GetDriverCarDriver}
	 * @throws CommandException
	 *             when something wrong went while executing DAO method or wrong
	 *             car id was specified in the request object.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		String page = PageManager.getInstance().generateCommandRequest(CommandName.GET_DRIVER_CAR);
		try {
			if (AttributeDigester.isValidCarId(request)) {
				int carId = AttributeDigester.getCarId(request);
				DriverDao driverDao = DaoFactory.getInstance().getDriverDao();
				int driverId = AttributeDigester.getCurrentUserId(request);
				driverDao.makeCarFaultless(carId, driverId);
			}
		} catch (DaoException e) {
			throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
		}
		return page;
	}

}
