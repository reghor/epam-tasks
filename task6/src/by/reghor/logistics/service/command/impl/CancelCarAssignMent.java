package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.DispatcherDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.CommandName;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.AttributeDigester;
import by.reghor.logistics.util.PageManager;

/**
 * Cancels driver car assignment. Reads driver ID.
 * 
 * @author Hornet
 *
 */
public class CancelCarAssignment implements Command {
	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";

	/**
	 * Reads driver ID from request object and calls
	 * {@link by.reghor.logistics.dao.DispatcherDao#cancelDriverCarAssignment(int)
	 * cancelDriverCarAssignment} DAO method.
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return request url to command
	 *         {@link by.reghor.logistics.service.command.impl.GetDriverCarDriver
	 *         GetDriverCarDriver}
	 * @throws CommandException
	 *             when something wrong went while executing DAO method .
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		
		String page = PageManager.getInstance().generateCommandRequestNotSuccessful(CommandName.GET_ALL_DRIVERS);
		
		try {
			if (AttributeDigester.isValidDriverId(request)){
				DispatcherDao dispatcherDao = DaoFactory.getInstance().getDispatcherDao();
				int driverId = AttributeDigester.getDriverId(request);
				dispatcherDao.cancelDriverCarAssignment(driverId);
				page = PageManager.getInstance().generateReviewDriverCarRequest(driverId);
			}
				
		} catch (DaoException e) {
			throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
		}
		return page;
	}

}
