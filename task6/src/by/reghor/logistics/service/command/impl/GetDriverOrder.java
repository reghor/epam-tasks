package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.DriverDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.entity.Order;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.AttributeDigester;
import by.reghor.logistics.util.PageManager;

/**
 * Gets driver order according to driver ID specified in request object.
 * 
 * @author Hornet
 *
 */
public class GetDriverOrder implements Command {
	private static final String ORDER_ATTRIBUTE = "order";

	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";

	/**
	 * Reads driver ID from request object and calls
	 * {@link by.reghor.logistics.dao.DriverDao#getDriverOrder(int, String)
	 * getDriverOrder} DAO method to retrieve driver order info and sets info as
	 * request object attribute.
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return path to page for driver order reviewal.
	 * @throws CommandException
	 *             when something wrong went while executing DAO method or wrong
	 *             order ID was specified in the request object.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		String page = PageManager.getInstance().getPage(PageManager.DRIVER_REVIEW_ORDER);
		try {
				DriverDao driverDao = DaoFactory.getInstance().getDriverDao();
				int driverId = AttributeDigester.getCurrentUserId(request);
				Order order = driverDao.getDriverOrder(driverId, AttributeDigester.getCurrentLocale(request));
				request.setAttribute(ORDER_ATTRIBUTE, order);
		} catch (DaoException e) {
			throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
		}
		return page;
	}

}
