package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.CustomerDao;
import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.entity.Order;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.CommandName;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.AttributeDigester;
import by.reghor.logistics.util.PageManager;

/**
 * Creates new order according to specified parameters.
 * 
 * @author Hornet
 *
 */
public class CreateNewOrder implements Command {
	private static final String WRONG_INPUT_ATTRIBUTE = "wrong_input";
	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";

	/**
	 * Reads new order parameters from request object, validates them and calls
	 * {@link by.reghor.logistics.dao.CustomerDao#createNewOrder(int, String, int, int, int, int, int, int, int)
	 * createNewOrder} DAO method.
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return request url to command
	 *         {@link by.reghor.logistics.service.command.impl.CreateNewOrderPage
	 *         CreateNewOrderPage} with success attribute set to true if
	 *         creation was successful and if not request url to command
	 *         {@link by.reghor.logistics.service.command.impl.CreateNewOrder
	 *         CreateNewOrder}
	 * @throws CommandException
	 *             when something wrong went while executing DAO method or wrong
	 *             car parameters were specified in the request object.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

		String page = PageManager.getInstance().getPage(PageManager.CUSTOMER_CREATE_NEW_ORDER);

		try {
			CustomerDao customerDao = DaoFactory.getInstance().getCustomerDao();
			Order order = AttributeDigester.getOrder(request);
			if (order != null) {
				System.out.println(order.getArrivalPointId()+"|"+order.getDepartureCountryId());
				int userId = AttributeDigester.getCurrentUserId(request);
				customerDao.createNewOrder(userId, order.getDescription(), order.getMaxLoadHeight(), order.getMaxLoadHeight(),
						order.getMaxLoadLength(), order.getMaxLoadHeight(), order.getVolume(), order.getDeparturePointId(),
						order.getArrivalPointId());
				page = PageManager.getInstance().generateCommandRequestSuccessful(CommandName.CREATE_NEW_ORDER_REFERENCE);
			} else {
				request.setAttribute(WRONG_INPUT_ATTRIBUTE, true);
			}
		} catch (DaoException e) {
			throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
		}

		return page;
	}

}
