package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.bean.ReviewedOrder;
import by.reghor.logistics.dao.CustomerDao;
import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.entity.User;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.CommandName;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.PageManager;

public class EditOrderPage implements Command {

		private static final String USER_ATTRIBUTE = "user";
		private static final String ORDER_ID_ATTRIBUTE = "order_id";
		private static final String REVIEWED_ORDER_ATTRIBUTE = "reviewed_order";
		private static final String LOCAL_ATTRIBUTE = "local";

		private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";
		private static final String WRONG_ORDER_ID_EXCEPTION_MESSAGE = "Wrong order ID";
		/**
		 * *Gets reviewed order by customer. Reads order ID  from
		 * request object and calls
		 * {@link by.reghor.logistics.dao.CustomerDao#reviewOrder(int, int, String)
		 * reviewOrder} DAO method. Read info will be displayed on the edit order page of current customer.
		 * 
		 * @param request
		 *            {@inheritDoc}
		 * @param response
		 *            {@inheritDoc}
		 * @return  path to customer edit order page
		 * @throws CommandException
		 *             when something wrong went while executing DAO method or wrong order ID was specified
		 */
		@Override
		public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
			String page = null;
			try {
				CustomerDao customerDao = DaoFactory.getInstance().getCustomerDao();
				int orderId = Integer.valueOf(request.getParameter(ORDER_ID_ATTRIBUTE));
				User user = (User) request.getSession().getAttribute(USER_ATTRIBUTE);
				int userId = user.getId();
				ReviewedOrder reviewOrder = customerDao.reviewOrder(userId, orderId, (String) request.getSession().getAttribute(LOCAL_ATTRIBUTE));
				if (reviewOrder!=null){
					request.setAttribute(REVIEWED_ORDER_ATTRIBUTE, reviewOrder);
					page = PageManager.getInstance().getPage(PageManager.CUSTOMER_EDIT_ORDER);
				}else{
					page=PageManager.getInstance().generateCommandRequestNoSuchOrder(CommandName.SHOW_ALL_ORDERS_CUSTOMER);
				}
				
			} catch (NumberFormatException e) {
				throw new CommandException(WRONG_ORDER_ID_EXCEPTION_MESSAGE, e);
			} catch (DaoException e) {
				throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
			}

		return page;
	}
}
