package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.PageManager;

/**Returns dispatcher page for  adding new car .
 * @author Hornet
 *
 */
public class AddNewCarPage implements Command {

	/**Returns dispatcher page for adding new car
	  
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return dispatcher page for adding new car.
	 * @throws CommandException no exception will be thrown
	 *            
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		return PageManager.getInstance().getPage(PageManager.DISPATCHER_ADD_NEW_CAR);
	}
}
