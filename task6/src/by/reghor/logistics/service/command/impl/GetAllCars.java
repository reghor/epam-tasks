package by.reghor.logistics.service.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.DispatcherDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.entity.Car;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.PageManager;

/**Retrieves all cars.
 * @author Hornet
 *
 */
public class GetAllCars implements Command {

	private static final String ALL_CARS_ATTRIBUTE = "all_cars";
	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";
	/**
	 * Reads list of all cars  by calling
	 * {@link by.reghor.logistics.dao.DispatcherDao.getAllCars()
	 * getAllCars} DAO method and sets as attribute of request.
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return path to dispatcher all cars page.
	 * @throws CommandException
	 *             when something wrong went while executing DAO method.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		
		String page  = PageManager.getInstance().getPage(PageManager.DISPATCHER_ALL_CARS);
		try {
			DispatcherDao dispatcherDao = DaoFactory.getInstance().getDispatcherDao();
			List<Car> carList = dispatcherDao.getAllCars();
			request.setAttribute(ALL_CARS_ATTRIBUTE, carList);
		} catch (DaoException e) {
			throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
		}
		return page;
	}

}
