package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.PageManager;

public class WelcomePage implements Command {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		return PageManager.getInstance().getPage(PageManager.GUEST_WELCOME);
	}

}
