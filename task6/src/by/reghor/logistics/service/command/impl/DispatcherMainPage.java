package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.PageManager;

/** Gets path to dispatcher main page
 * @author Hornet
 *
 */
public class DispatcherMainPage implements Command {
	/**
	 * Gets path to dispatcher main page
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return path to dispatcher main page
	 * @throws CommandException
	 *             will never be thrown
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		return PageManager.getInstance().getPage(PageManager.DISPATCHER_MAIN);
	}
}
