package by.reghor.logistics.service.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.DispatcherDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.entity.User;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.PageManager;

/**
 * Retrieves all drivers.
 * 
 * @author Hornet
 *
 */
public class GetAllDrivers implements Command {
	private static final String ALL_DRIVERS_ATTRIBUTE = "all_drivers";

	/**
	 * Reads a list of all drivers by calling {@link
	 * by.reghor.logistics.dao.DispatcherDao.getAllCars() getAllCars} DAO method
	 * and sets as attribute of request.
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return path to dispatcher all cars page.
	 * @throws CommandException
	 *             when something wrong went while executing DAO method.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		String page = PageManager.getInstance().getPage(PageManager.DISPATCHER_ALL_DRIVERS);
		try {
			DispatcherDao dispatcherDao = DaoFactory.getInstance().getDispatcherDao();
			List<User> driverList = dispatcherDao.getAllDrivers();
			request.setAttribute(ALL_DRIVERS_ATTRIBUTE, driverList);
		} catch (DaoException e) {
			throw new CommandException(e);
		}
		return page;
	}

}
