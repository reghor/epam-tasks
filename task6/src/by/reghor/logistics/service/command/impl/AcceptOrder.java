package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.DriverDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.CommandName;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.AttributeDigester;
import by.reghor.logistics.util.PageManager;



/**
 * Class allows customer order (which was assigned by dispatcher) be accepted by
 * driver.
 * 
 *
 */
public class AcceptOrder implements Command {

	
	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";
	

	/**
	 * Accepts user order, which was accepted by driver. Calls
	 * {@link by.reghor.logistics.dao.DriverDao#acceptOrder(int, int)
	 * acceptOrder()} DAO method for finishing the operation of acception.
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return request url on {@link
	 *         by.reghor.logistics.dao.DriverDao.acceptOrder(int, int)
	 *         acceptOrder} command
	 * @throws CommandException
	 *             when something wrong went while executing DAO method or ID of
	 *             order specified in request was wrong.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		DriverDao driverDao = DaoFactory.getInstance().getDriverDao();
		int userId = AttributeDigester.getCurrentUserId(request);
		try {
			if (AttributeDigester.isValidOrderId(request)){
				int orderId = AttributeDigester.getOrderId(request);
				driverDao.acceptOrder(orderId, userId);
			}
		} catch (DaoException e) {
			throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
		} 
		return PageManager.getInstance().generateCommandRequest(CommandName.SHOW_DRIVER_ORDER);
	}
}
