package by.reghor.logistics.service.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.DispatcherDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.entity.Car;
import by.reghor.logistics.entity.Order;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.AttributeDigester;
import by.reghor.logistics.util.PageManager;

/**
 * Gets suitable cars. Reads required parameters for cars and sets car list and
 * order ID as attributes.
 * 
 * @author Hornet
 *
 */
public class GetSuitableCars implements Command {

	private static final String ORDER_ID_ATTRIBUTE = "order_id";
	private static final String SUITABLE_CARS_LIST_ATTRIBUTE = "suitable_cars_list";

	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";

	/**
	 * Gets suitable cars. Reads required parameters for cars from request
	 * object and sets car list and order ID as attributes of request object and
	 * calls {@link
	 * by.reghor.logistics.dao.DispatcherDao.getSuitableCarsForAssign(int, int,
	 * int, int, int) getSuitableCarsForAssign} DAO method.
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return path to dispatcher page for order car assignment
	 * @throws CommandException
	 *             when something wrong went while executing DAO method or wrong
	 *             order parameters were specified in the request object.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		String page = PageManager.getInstance().getPage(PageManager.DISPATCHER_ASSIGN_ORDER);
		try {
			if (AttributeDigester.isValidOrderId(request)) {
				int orderId = AttributeDigester.getOrderId(request);
				Order order = AttributeDigester.getOrderLoadInfo(request);
				if (order != null) {
					DispatcherDao dispatcherDao = DaoFactory.getInstance().getDispatcherDao();
					List<Car> carList = dispatcherDao.getSuitableCarsForAssign(order.getMaxLoadHeight(), order.getMaxLoadLength(),
							order.getMaxLoadWidth(), order.getVolume(), order.getWeight());
					request.setAttribute(SUITABLE_CARS_LIST_ATTRIBUTE, carList);
					request.setAttribute(ORDER_ID_ATTRIBUTE, orderId);
				}
			}
		} catch (DaoException e) {
			throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
		} 
		
		return page;
	}

}
