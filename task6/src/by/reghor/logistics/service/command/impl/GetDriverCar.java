package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.DispatcherDao;
import by.reghor.logistics.dao.DriverDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.entity.Car;
import by.reghor.logistics.entity.UserRole;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.AttributeDigester;
import by.reghor.logistics.util.PageManager;

/**
 * Reads driver car info during dispatcher session and sets it as attribute of
 * request.
 * 
 * @author Hornet
 *
 */
public class GetDriverCar implements Command {

	private static final String DRIVER_ID_ATTRIBUTE = "driver_id";
	private static final String DRIVER_CAR_ATTRIBUTE = "driver_car";
	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";
	private static final String CAR_ATTRIBUTE = "car";

	/**
	 * Reads driver ID from request object, driver car info during dispatcher
	 * session by calling {@link
	 * by.reghor.logistics.dao.DispatcherDao.getDriverCar(int) getDriverCar} DAO
	 * method and sets it as attribute of request.
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return path to dispatcher page about driver car.
	 * @throws CommandException
	 *             when something wrong went while executing DAO method or wrong
	 *             driver ID was specified.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		UserRole userRole = AttributeDigester.getCurrentUser(request).getRole();
		String page = null;
		switch (userRole) {
		case DISPATCHER: {
			page = PageManager.getInstance().getPage(PageManager.DISPATCHER_DRIVER_CAR);
			try {
				DispatcherDao dispatcherDao = DaoFactory.getInstance().getDispatcherDao();
				if (AttributeDigester.isValidDriverId(request)) {
					int driverId = AttributeDigester.getDriverId(request);
					request.setAttribute(DRIVER_ID_ATTRIBUTE, driverId);
					Car car = dispatcherDao.getDriverCar(driverId);
					request.setAttribute(DRIVER_CAR_ATTRIBUTE, car);
				}

			} catch (DaoException e) {
				throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
			}
			break;
		}
		case DRIVER: {
			PageManager configurationManager = PageManager.getInstance();
			page = configurationManager.getPage(PageManager.DRIVER_REVIEW_ORDER);
			try {
				DriverDao driverDao = DaoFactory.getInstance().getDriverDao();
				int driverId = AttributeDigester.getCurrentUserId(request);
				Car car = driverDao.getDriverCar(driverId);
				request.setAttribute(CAR_ATTRIBUTE, car);
				page = configurationManager.getPage(PageManager.DRIVER_REVIEW_CAR);
			} catch (DaoException e) {
				throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
			}
			break;
		}
		default: {
			break;
		}
		}
		return page;
	}
}
