package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.DispatcherDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.CommandName;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.AttributeDigester;
import by.reghor.logistics.util.PageManager;

/**
 * Assigns car to a driver. Performs reading parameters.
 * 
 * @author Hornet
 *
 */
public class AssignDriverCar implements Command {

	
	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";

	/**
	 * Reads data about car from the request object and calls
	 * {@link by.reghor.logistics.dao.DispatcherDao#assignDriverCar(int, int)
	 * assignDriverCar} DAO method.
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return request url to command
	 *         {@link by.reghor.logistics.service.command.impl.GetAllDrivers
	 *         GetAllDrivers} if assignment went ok with parameter success set
	 *         true, if not with parameter false.
	 * @throws CommandException
	 *             when something wrong went while executing DAO method.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		String page = PageManager.getInstance().generateCommandRequestNotSuccessful(CommandName.GET_ALL_DRIVERS);

		try {
			if (AttributeDigester.isValidDriverId(request) && AttributeDigester.isValidCarId(request)) {
				int driverId = AttributeDigester.getDriverId(request);
				int carId = AttributeDigester.getCarId(request);
				DispatcherDao dispatcherDao = DaoFactory.getInstance().getDispatcherDao();
				if (dispatcherDao.assignDriverCar(driverId, carId)) {
					page = PageManager.getInstance().generateCommandRequestSuccessful(CommandName.GET_ALL_DRIVERS);
				} 
			} 
		} catch (DaoException e) {
			throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
		}
		return page;
	}

}
