package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.bean.ReviewedOrder;
import by.reghor.logistics.dao.CustomerDao;
import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.DispatcherDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.CommandName;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.AttributeDigester;
import by.reghor.logistics.util.PageManager;

/**
 * Gets reviewed order by dispatcher according to provided in request object ID.
 * 
 * @author Hornet
 *
 */
public class ReviewOrder implements Command {
	private static final String REVIEWED_ORDER_ATTRIBUTE = "reviewed_order";
	private static final String WRONG_USER_ROLE_EXCEPTION_MESSAGE = "wrong user role";
	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";

	/**
	 * Gets reviewed order by dispatcher or by driver. Reads order ID from request object
	 * and calls
	 * {@link by.reghor.logistics.dao.DispatcherDao#reviewOrder(int, String)
	 * reviewOrder} or {@link by.reghor.logistics.dao.CustomerDao#reviewOrder(int, int, String)
	 * reviewOrder} DAO method.
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return path to dispatcher or driver review order page, if no such order- request to all orders page with no such order attribute
	 * @throws CommandException
	 *             when something wrong went while executing DAO method 
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		String page = null;
		try {
			switch (AttributeDigester.getCurrentUser(request).getRole()) {
			case DISPATCHER: {
				page = PageManager.getInstance().generateCommandRequestNoSuchOrder(CommandName.SHOW_ALL_ORDERS_DISPATCHER);
				DispatcherDao dispatcherDao = DaoFactory.getInstance().getDispatcherDao();
				if (AttributeDigester.isValidOrderId(request)) {
					int orderId = AttributeDigester.getOrderId(request);
					ReviewedOrder reviewOrder = dispatcherDao.reviewOrder(orderId, AttributeDigester.getCurrentLocale(request));
					if (reviewOrder != null) {
						request.setAttribute(REVIEWED_ORDER_ATTRIBUTE, reviewOrder);
						page = PageManager.getInstance().getPage(PageManager.DISPATCHER_REVIEW_ORDER);
					}
				}
				break;
			}
			case CUSTOMER: {
				page = PageManager.getInstance().generateCommandRequestNoSuchOrder(CommandName.SHOW_ALL_ORDERS_CUSTOMER);
				if (AttributeDigester.isValidOrderId(request)) {
					int orderId = AttributeDigester.getOrderId(request);
					int userId = AttributeDigester.getCurrentUserId(request);
					CustomerDao customerDao = DaoFactory.getInstance().getCustomerDao();
					ReviewedOrder reviewedOrder = customerDao.reviewOrder(userId, orderId, AttributeDigester.getCurrentLocale(request));
					if (reviewedOrder != null) {
						request.setAttribute(REVIEWED_ORDER_ATTRIBUTE, reviewedOrder);
						page = PageManager.getInstance().getPage(PageManager.CUSTOMER_REVIEW_ORDER);
					}
				}
				break;
			}
			default: {
				throw new CommandException(WRONG_USER_ROLE_EXCEPTION_MESSAGE);
			}
			}
		} catch (DaoException e) {
			throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
		}
		return page;
	}

}
