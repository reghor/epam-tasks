package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.CustomerDao;
import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.entity.Order;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.AttributeDigester;
import by.reghor.logistics.util.PageManager;

public class EditOrder implements Command {
	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";

	/**
	 * Reads new parameters of edited order from request object, validates them
	 * and applies changes by calling
	 * {@link by.reghor.logistics.dao.CustomerDao#editOrder(int, int, String, int, int, int, int, int, int, int)
	 * editOrder} DAO method.
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return request url to command {@link by.reghor.logistics
	 *         generateLastQueryStringRequest} if editing was successful, if not
	 *         path to edit order page.
	 * @throws CommandException
	 *             when something wrong went while executing DAO method.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		String page = PageManager.getInstance().getPage(PageManager.CUSTOMER_EDIT_ORDER);
		try {
			if (AttributeDigester.isValidOrderId(request)) {
				int orderId = AttributeDigester.getOrderId(request);
				Order order = AttributeDigester.getOrder(request);
				if (order != null) {
					int userId = AttributeDigester.getCurrentUserId(request);
					CustomerDao customerDao = DaoFactory.getInstance().getCustomerDao();
					customerDao.editOrder(userId, orderId, order.getDescription(), order.getMaxLoadHeight(), order.getMaxLoadHeight(), order.getMaxLoadLength(), order.getWeight(), order.getVolume(), order.getDeparturePointId(),
							order.getArrivalPointId());
					page = PageManager.getInstance().generateReviewOrderRequest(orderId);
				}
			}
		} catch (DaoException e) {
			throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
		}
		
		return page;
	}
}
