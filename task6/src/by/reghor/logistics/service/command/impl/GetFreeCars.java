package by.reghor.logistics.service.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.DispatcherDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.entity.Car;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.AttributeDigester;
import by.reghor.logistics.util.PageManager;

/**
 * Retrieves all free cars (with status 'free').
 * 
 * @author Hornet
 *
 */
public class GetFreeCars implements Command {
	private static final String FREE_CARS_ATTRIBUTE = "free_cars";
	private static final String DRIVER_ID_ATTRIBUTE = "driver_id";
	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";

	/**
	 * Reads list of all free cars (with status 'free') by calling {@link
	 * by.reghor.logistics.dao.DispatcherDao.getFreeCars() getFreeCars} DAO
	 * method and sets as attribute of request.
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return path to dispatcher free cars page.
	 * @throws CommandException
	 *             when something wrong went while executing DAO method or wrong
	 *             driver id was specified in request object.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		String page = PageManager.getInstance().getPage(PageManager.DISPATCHER_FREE_CARS);
		try {
			if (AttributeDigester.isValidDriverId(request)){
				int driverId=AttributeDigester.getDriverId(request);
				DispatcherDao dispatcherDao = DaoFactory.getInstance().getDispatcherDao();
				List<Car> carList = dispatcherDao.getFreeCars();
				request.setAttribute(FREE_CARS_ATTRIBUTE, carList);
				request.setAttribute(DRIVER_ID_ATTRIBUTE, driverId);
			}
			
			
		} catch (DaoException e) {
			throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
		} 
		return page;
	}

}
