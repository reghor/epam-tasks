package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.DispatcherDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.entity.Car;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.CommandName;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.AttributeDigester;
import by.reghor.logistics.util.PageManager;

/**
 * Adds new car . Perform reading parameters, validation.
 * 
 * @author Hornet
 *
 */
public class AddNewCar implements Command {

	private static final String SUCCESS_ATTRIBUTE = "success";
	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";

	/**
	 * Reads data about new car from the request object, validates this data and
	 * calls
	 * {@link by.reghor.logistics.dao.DispatcherDao#addNewCar(String, String, String, int, int, int, int, int, int)
	 * addNewCar()} DAO method.
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return page for adding new car path if process of adding went wrong, if
	 *         ok - request to command
	 *         {@link by.reghor.logistics.service.command.impl.AddNewCarPage
	 *         AddNewCarPage} with parameter success set true.
	 * @throws CommandException
	 *             when something wrong went while executing DAO method or wrong
	 *             car parameters were specified in request object.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

		DispatcherDao dispatcherDao = DaoFactory.getInstance().getDispatcherDao();
		String page = null;
		try {
			Car car = AttributeDigester.getCar(request);
			if (car != null
					&& dispatcherDao.addNewCar(car.getVehicleIdNumber(), car.getMakeOfCar(), car.getColour(), car.getYear(), car.getMaxHeight(),
							car.getMaxLength(), car.getMaxWidth(), car.getMaxVolume(), car.getMaxWeight())) {
				page = PageManager.getInstance().generateCommandRequestSuccessful(CommandName.ADD_NEW_CAR_PAGE_REFERENCE);
			} else {
				request.setAttribute(SUCCESS_ATTRIBUTE, false);
				page = PageManager.getInstance().getPage(PageManager.DISPATCHER_ADD_NEW_CAR);
			}
		} catch (DaoException e) {
			throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, e);
		}
		return page;
	}
}
