package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.CommandName;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.PageManager;

/**
 * Logouts current user (invalidates current session)  
 * 
 * @author Hornet
 *
 */
public class Logout implements Command {
	/**
	 * Logouts current user (invalidates current session)  
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return url request to {@link by.reghor.logistics.service.command.impl.LoginPage LoginPage} command 
	 * @throws CommandException
	 *             will never be thrown
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		request.getSession().invalidate();
		return PageManager.getInstance().generateCommandRequest(CommandName.LOGIN_PAGE_REFERENCE);
	}

}
