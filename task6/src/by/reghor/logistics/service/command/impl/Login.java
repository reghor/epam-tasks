package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.DaoFactory;
import by.reghor.logistics.dao.UserDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.entity.User;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.CommandName;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.AttributeDigester;
import by.reghor.logistics.util.PageManager;
import by.reghor.logistics.util.RegExManager;

public class Login implements Command {

	private static final String USER_ATTRIBUTE = "user";
	private static final String LOGIN_ATTRIBUTE = "login";
	private static final String PASSWORD_ATTRIBUTE = "password";
	private static final String ERROR_LOGIN_PASSWORD_ATTRIBUTE = "errorLoginPassword";
	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";
	private static final String USER_ROLE_EXCEPTION_MESSAGE = "wrong user type during login";


	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		PageManager pageManager = PageManager.getInstance();
		String page = pageManager.generateCommandRequest(CommandName.LOGIN_PAGE_REFERENCE);
		String login = request.getParameter(LOGIN_ATTRIBUTE);
		String password = request.getParameter(PASSWORD_ATTRIBUTE);
		if (AttributeDigester.validate(login, RegExManager.getValue(RegExManager.LOGIN_PATTERN))
				&& AttributeDigester.validate(password, RegExManager.getValue(RegExManager.PASSWORD_PATTERN))) {
			System.out.println("OK L");
			try {
				UserDao userDao = DaoFactory.getInstance().getUserDao();
				User user = userDao.authorizeUser(login, password);
				if (user != null) {
					System.out.println("User is ok");
					request.getSession().setAttribute(USER_ATTRIBUTE, user);
					switch (user.getRole()) {
					case DISPATCHER: {
						page = pageManager.generateCommandRequest(CommandName.DISPATCHER_MAIN_PAGE_REFERENCE);
						break;
					}
					case DRIVER: {
						page = pageManager.generateCommandRequest(CommandName.DRIVER_MAIN_PAGE_REFERENCE);
						break;
					}
					case CUSTOMER: {
						page = pageManager.generateCommandRequest(CommandName.CUSTOMER_MAIN_PAGE_REFERENCE);
						break;
					}
					default: {
						throw new CommandException(USER_ROLE_EXCEPTION_MESSAGE);
					}
					}
				} else {
					request.setAttribute(ERROR_LOGIN_PASSWORD_ATTRIBUTE, true);

				}
			} catch (DaoException e) {
				throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE,e);
			}
		}
		return page;
	}
}
