package by.reghor.logistics.service.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.dao.UserDao;
import by.reghor.logistics.dao.exception.DaoException;
import by.reghor.logistics.dao.mysql.MySqlUserDao;
import by.reghor.logistics.entity.User;
import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.AttributeDigester;
import by.reghor.logistics.util.PageManager;
/**
 * Registers driver according to entered parameters.
 * 
 * @author Hornet
 *
 */
public class RegisterDriver implements Command {

	private static final String INPUT_NOT_UNIQUE_ATTRIBUTE = "input_not_unique";

	private static final String DAO_COMMAND_EXCEPTION_MESSAGE = "Exception on executing DAO command";
	/**
	 * Registers driver according to entered parameters. Reads user input from
	 * request object, validates and calls
	 * {@link by.reghor.logistics.dao.UserDao#registerDriver(String, String, String, String, String, String)
	 * registerDriver} DAO method.
	 * 
	 * @param request
	 *            {@inheritDoc}
	 * @param response
	 *            {@inheritDoc}
	 * @return request url or path to page after command
	 *         {@link by.reghor.logistics.service.command.impl.Login
	 *         Login} execution if registration was successful, otherwise path to registration page.
	 * @throws CommandException
	 *             when something wrong went while executing DAO method
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		String page = PageManager.getInstance().getPage(PageManager.GUEST_REGISTRATION);
		boolean userInputUnique = false;
		User user = AttributeDigester.getUser(request);
		if (user != null) {
			try {
				UserDao userDao = MySqlUserDao.getInstance();
				userInputUnique = userDao.registerDriver(user.getLogin(), user.getFirstName(), user.getLastName(), user.getPassword(),
						user.getEmail(), user.getPhoneNumber());
			} catch (DaoException exception) {
				throw new CommandException(DAO_COMMAND_EXCEPTION_MESSAGE, exception);
			}
		}
		if (userInputUnique) {
			Login loginCommand = new Login();
			page = loginCommand.execute(request, response);
		} else {
			request.setAttribute(INPUT_NOT_UNIQUE_ATTRIBUTE, true);
		}
		return page;
	}

}
