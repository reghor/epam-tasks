package by.reghor.logistics.service.command.dispatcher;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.reghor.logistics.service.command.Command;
import by.reghor.logistics.service.command.CommandName;
import by.reghor.logistics.service.command.exception.CommandException;
import by.reghor.logistics.util.PageManager;

public class CommandDispatcher {
	private static final Logger LOGGER = LogManager.getLogger(CommandDispatcher.class.getName());
	private static final String JSP_FILE_EXTENSION=".jsp";
	private static final String COMMAND_EXCEPTION_MESSAGE="Exception on executing command";

	private CommandDispatcher() {
	}

	/**Launches command execution. After command execution is finished redirects or forwards user to a page. 
	 * @param command user's command for further execution
	 * @param request request object
	 * @param response response object
	 * @throws IOException when RequestDispatcher can not forward to some page.
	 * @throws ServletException when RequestDispatcher can not forward or redirect to some page.
	 */
	public static void forward(Command command, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String page = null;
		try {
			page = command.execute(request, response);
		} catch (CommandException e) {
			LOGGER.error(COMMAND_EXCEPTION_MESSAGE, e);
		}
		RequestDispatcher requestDispatcher = request.getRequestDispatcher(page);
		if (requestDispatcher != null) {
			if (page.endsWith(JSP_FILE_EXTENSION)) {
				requestDispatcher.forward(request, response);
			} else {
				System.out.println(request.getContextPath()+ page);

				response.sendRedirect(request.getContextPath() + page);
			}
		} else {
			request.getSession().invalidate();
			response.sendRedirect(request.getContextPath()+PageManager.getInstance().generateCommandRequest(CommandName.LOGIN_PAGE_REFERENCE));
		
		}
	}
}
