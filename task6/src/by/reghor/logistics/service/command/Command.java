package by.reghor.logistics.service.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.service.command.exception.CommandException;

/**Interface describing methods of usual commands.
 * @author Hornet
 *
 */
public interface Command {

	/**Executes a command and returns page or request paths after execution.
	 * @param request request object
	 * @param response response object
	 * @return page or request path to which user view will be redirected or forwarded
	 * @throws CommandException when something during execution
	 */
	String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException;

}
