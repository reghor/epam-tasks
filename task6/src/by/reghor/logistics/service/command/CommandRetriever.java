package by.reghor.logistics.service.command;

import java.util.HashMap;
import java.util.Map;

import by.reghor.logistics.service.command.impl.AcceptOrder;
import by.reghor.logistics.service.command.impl.AccomplishOrder;
import by.reghor.logistics.service.command.impl.AddNewCar;
import by.reghor.logistics.service.command.impl.AddNewCarPage;
import by.reghor.logistics.service.command.impl.AssignDriverCar;
import by.reghor.logistics.service.command.impl.AssignOrderCar;
import by.reghor.logistics.service.command.impl.CancelCarAssignment;
import by.reghor.logistics.service.command.impl.CancelOrderAssignment;
import by.reghor.logistics.service.command.impl.CreateNewOrder;
import by.reghor.logistics.service.command.impl.CreateNewOrderPage;
import by.reghor.logistics.service.command.impl.CustomerMainPage;
import by.reghor.logistics.service.command.impl.DeclineOrder;
import by.reghor.logistics.service.command.impl.DeleteCar;
import by.reghor.logistics.service.command.impl.DeleteOrder;
import by.reghor.logistics.service.command.impl.DispatcherMainPage;
import by.reghor.logistics.service.command.impl.DriverMainPage;
import by.reghor.logistics.service.command.impl.EditOrder;
import by.reghor.logistics.service.command.impl.EditOrderPage;
import by.reghor.logistics.service.command.impl.GetAllCars;
import by.reghor.logistics.service.command.impl.GetAllCustomerOrdersPage;
import by.reghor.logistics.service.command.impl.GetAllDrivers;
import by.reghor.logistics.service.command.impl.GetAllOrdersDispatcherPage;
import by.reghor.logistics.service.command.impl.GetDriverCar;
import by.reghor.logistics.service.command.impl.GetDriverOrder;
import by.reghor.logistics.service.command.impl.GetFreeCars;
import by.reghor.logistics.service.command.impl.GetSuitableCars;
import by.reghor.logistics.service.command.impl.Localization;
import by.reghor.logistics.service.command.impl.Login;
import by.reghor.logistics.service.command.impl.LoginPage;
import by.reghor.logistics.service.command.impl.Logout;
import by.reghor.logistics.service.command.impl.MakeCarFaultless;
import by.reghor.logistics.service.command.impl.MakeCarFaulty;
import by.reghor.logistics.service.command.impl.RegisterCustomer;
import by.reghor.logistics.service.command.impl.RegisterCustomerPage;
import by.reghor.logistics.service.command.impl.RegisterDriver;
import by.reghor.logistics.service.command.impl.RegisterDriverPage;
import by.reghor.logistics.service.command.impl.ReviewOrder;
import by.reghor.logistics.service.command.impl.WelcomePage;

/**Class for keeping command objects with their name, retrieving command objects according to their names.
 * @author Hornet
 *
 */
public class CommandRetriever {

	private static final CommandRetriever instance = new CommandRetriever();

	private final Map<CommandName, Command> commands = new HashMap<CommandName, Command>();

	private CommandRetriever() {
		commands.put(CommandName.LOGIN, new Login());
		commands.put(CommandName.ADD_NEW_CAR, new AddNewCar());
		commands.put(CommandName.ADD_NEW_CAR_PAGE_REFERENCE, new AddNewCarPage());
		commands.put(CommandName.SHOW_ALL_ORDERS_DISPATCHER, new GetAllOrdersDispatcherPage());
		commands.put(CommandName.SHOW_ALL_ORDERS_CUSTOMER, new GetAllCustomerOrdersPage());
		commands.put(CommandName.REGISTER_PAGE_REFERENCE, new RegisterCustomerPage());
		commands.put(CommandName.REGISTER_USER, new RegisterCustomer());
		commands.put(CommandName.CREATE_NEW_ORDER_REFERENCE, new CreateNewOrderPage());
		commands.put(CommandName.CREATE_NEW_ORDER, new CreateNewOrder());
		commands.put(CommandName.REVIEW_ORDER, new ReviewOrder());
		commands.put(CommandName.ASSIGN_ORDER_CAR, new GetSuitableCars());
		commands.put(CommandName.ASSIGN_ORDER_CAR_PICK, new AssignOrderCar());
		commands.put(CommandName.EDIT_ORDER, new EditOrder());
		commands.put(CommandName.CANCEL_ASSIGNMENT, new CancelOrderAssignment());
		commands.put(CommandName.DELETE_ORDER, new DeleteOrder());
		commands.put(CommandName.REGISTER_DRIVER, new RegisterDriver());
		commands.put(CommandName.REGISTER_DRIVER_PAGE_REFERENCE, new RegisterDriverPage());
		commands.put(CommandName.DELETE_CAR, new DeleteCar());
		commands.put(CommandName.LOGOUT, new Logout());
		commands.put(CommandName.GET_ALL_CARS, new GetAllCars());
		commands.put(CommandName.GET_ALL_DRIVERS, new GetAllDrivers());
		commands.put(CommandName.LOCALIZATION, new Localization());
		commands.put(CommandName.LOGIN_PAGE_REFERENCE, new LoginPage());
		commands.put(CommandName.DISPATCHER_MAIN_PAGE_REFERENCE, new DispatcherMainPage());
		commands.put(CommandName.CUSTOMER_MAIN_PAGE_REFERENCE, new CustomerMainPage());
		commands.put(CommandName.DRIVER_MAIN_PAGE_REFERENCE, new DriverMainPage());
		commands.put(CommandName.SHOW_DRIVER_ORDER, new GetDriverOrder());
		commands.put(CommandName.GET_DRIVER_CAR, new GetDriverCar());
		commands.put(CommandName.ACCEPT_ORDER, new AcceptOrder());
		commands.put(CommandName.ACCOMPLISH_ORDER, new AccomplishOrder());
		commands.put(CommandName.MAKE_CAR_FAULTLESS, new MakeCarFaultless());
		commands.put(CommandName.MAKE_CAR_FAULTY, new MakeCarFaulty());
		commands.put(CommandName.GET_DRIVER_CAR, new GetDriverCar());
		commands.put(CommandName.ASSIGN_DRIVER_CAR, new AssignDriverCar());
		commands.put(CommandName.GET_FREE_CARS, new GetFreeCars());
		commands.put(CommandName.CANCEL_CAR_ASSIGNMENT, new CancelCarAssignment());
		commands.put(CommandName.DECLINE_ORDER, new DeclineOrder());
		commands.put(CommandName.EDIT_ORDER_PAGE, new EditOrderPage());
		commands.put(CommandName.WELCOME_PAGE_REFERENCE, new WelcomePage());
	}

	/**Return instance of the CommandRetriever class.
	 * @return
	 */
	public static CommandRetriever getInstance() {
		return instance;
	}

	/**Retrieves command object according to the specified name.
	 * @param commandName command name.
	 * @return command object according to the specified name.
	 */
	public Command getCommand(String commandName) {
		CommandName name = CommandName.valueOf(commandName.toUpperCase());
		Command command = commands.get(name);
		return command;
	}

}
