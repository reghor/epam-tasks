package by.reghor.logistics.service.command.exception;


/** Exception class - will be thrown when something went wrong during command execution.
 * @author Hornet
 *
 */
public class CommandException extends Exception {

	private static final long serialVersionUID = 1L;

	public CommandException() {
		
    }

    public CommandException(String message) {
        super(message);
    }

    public CommandException(String message, Throwable exception) {
        super(message, exception);
    }

    public CommandException(Throwable exception) {
        super(exception);
    }
}
