package by.reghor.logistics.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import by.reghor.logistics.entity.Car;
import by.reghor.logistics.entity.CarCondition;
import by.reghor.logistics.entity.CarStatus;
import by.reghor.logistics.entity.Order;
import by.reghor.logistics.entity.OrderStatus;
import by.reghor.logistics.entity.User;
import by.reghor.logistics.entity.UserRole;

/**Class helps to retrieve data, form entities and java bean objects  from sql resultsets with retrieved data.
 * @author Hornet
 *
 */
public final class MySqlHelper {
	private static final String ID_ATTRIBUTE = "id";
	private static final String FIRST_NAME_ATTRIBUTE = "first_name";
	private static final String LAST_NAME_ATTRIBUTE = "last_name";
	private static final String LOGIN_ATTRIBUTE = "login";
	private static final String PHONE_NUMBER_ATTRIBUTE = "phone_number";
	private static final String EMAIL_ATTRIBUTE = "email";
	private static final String ROLE_ATTRIBUTE = "role";
	
	private static final String CAR_NUMBER_ATTRIBUTE = "vehicle_id_number";
	private static final String CAR_MAKE_OF_CAR_ATTRIBUTE = "make_of_car";
	private static final String CAR_YEAR_ATTRIBUTE = "year";
	private static final String CAR_MAX_LOAD_HEIGHT_ATTRIBUTE = "max_height";
	private static final String CAR_MAX_LOAD_LENGTH_ATTRIBUTE = "max_length";
	private static final String CAR_MAX_LOAD_WIDTH_ATTRIBUTE = "max_width";
	private static final String CAR_MAX_WEIGHT_ATTRIBUTE = "max_weight";
	private static final String CAR_MAX_VOLUME_ATTRIBUTE = "max_volume";
	private static final String CAR_COLOUR_ATTRIBUTE = "colour";
	private static final String CAR_STATUS_ATTRIBUTE = "status";
	private static final String CAR_CONDITION_ATTRIBUTE = "car_condition";
	
	private static final String DESCRIPTION_ATTRIBUTE = "description";
	private static final String MAX_LOAD_HEIGHT_ATTRIBUTE = "max_load_height";
	private static final String MAX_LOAD_LENGTH_ATTRIBUTE = "max_load_length";
	private static final String MAX_LOAD_WIDTH_ATTRIBUTE = "max_load_width";
	private static final String WEIGHT_ATTRIBUTE = "weight";
	private static final String VOLUME_ATTRIBUTE = "volume";
	private static final String ORDER_STATUS_ATTRIBUTE = "status";
	private static final String CITY_ARRIVAL_ID_ATTRIBUTE = "arrival_point_id";
	private static final String CITY_DEPARTURE_ID_ATTRIBUTE = "departure_point_id";
	private static final String CITY_ARRIVAL_NAME_ATTRIBUTE = "arrival_point_name";
	private static final String CITY_DEPARTURE_NAME_ATTRIBUTE = "departure_point_name";
	private static final String REGION_ARRIVAL_ID_ATTRIBUTE = "arrival_region_id";
	private static final String REGION_DEPARTURE_ID_ATTRIBUTE = "departure_region_id";
	private static final String REGION_ARRIVAL_NAME_ATTRIBUTE = "arrival_region_name";
	private static final String REGION_DEPARTURE_NAME_ATTRIBUTE = "departure_region_name";
	private static final String COUNTRY_ARRIVAL_ID_ATTRIBUTE = "arrival_country_id";
	private static final String COUNTRY_DEPARTURE_ID_ATTRIBUTE = "departure_country_id";
	private static final String COUNTRY_ARRIVAL_NAME_ATTRIBUTE = "arrival_country_name";
	private static final String COUNTRY_DEPARTURE_NAME_ATTRIBUTE = "departure_country_name";
	
	
	private MySqlHelper() {

	}

	/**Retrieves car data from resultset and returns car object keeping this data.
	 * @param result SQL resultset
	 * @return Car object with needed data
	 * @throws SQLException -when there are problems  while retrieving data
	 */
	public static Car getCarFromResultSet(ResultSet result) throws SQLException {
		Car car = new Car();
		car.setId(result.getInt(ID_ATTRIBUTE));
		car.setVehicleIdNumber(result.getString(CAR_NUMBER_ATTRIBUTE));
		car.setMaxHeight(result.getInt(CAR_MAX_LOAD_HEIGHT_ATTRIBUTE));
		car.setMaxLength(result.getInt(CAR_MAX_LOAD_LENGTH_ATTRIBUTE));
		car.setMaxWidth(result.getInt(CAR_MAX_LOAD_WIDTH_ATTRIBUTE));
		car.setMaxVolume(result.getInt(CAR_MAX_VOLUME_ATTRIBUTE));
		car.setMaxWeight(result.getInt(CAR_MAX_WEIGHT_ATTRIBUTE));
		car.setMakeOfCar(result.getString(CAR_MAKE_OF_CAR_ATTRIBUTE));
		car.setColour(result.getString(CAR_COLOUR_ATTRIBUTE));
		car.setYear(result.getInt(CAR_YEAR_ATTRIBUTE));
		car.setStatus(CarStatus.valueOf(result.getString(CAR_STATUS_ATTRIBUTE).toUpperCase()));
		String condition = result.getString(CAR_CONDITION_ATTRIBUTE).toUpperCase();
		CarCondition carCondition = CarCondition.valueOf(condition);
		car.setCarCondition(carCondition);
		return car;
	}
	/**Retrieves car data from resultset without car status and returns car object keeping this data.
	 * @param result SQL resultset
	 * @return Car object with needed data without car status
	 * @throws SQLException -when there are problems  while retrieving data
	 */
	public static Car getCarNoStatusFromResultSet(ResultSet result) throws SQLException {
		Car car = new Car();
		car.setId(result.getInt(ID_ATTRIBUTE));
		car.setVehicleIdNumber(result.getString(CAR_NUMBER_ATTRIBUTE));
		car.setMaxHeight(result.getInt(CAR_MAX_LOAD_HEIGHT_ATTRIBUTE));
		car.setMaxLength(result.getInt(CAR_MAX_LOAD_LENGTH_ATTRIBUTE));
		car.setMaxWidth(result.getInt(CAR_MAX_LOAD_WIDTH_ATTRIBUTE));
		car.setMaxVolume(result.getInt(CAR_MAX_VOLUME_ATTRIBUTE));
		car.setMaxWeight(result.getInt(CAR_MAX_WEIGHT_ATTRIBUTE));
		car.setMakeOfCar(result.getString(CAR_MAKE_OF_CAR_ATTRIBUTE));
		car.setColour(result.getString(CAR_COLOUR_ATTRIBUTE));
		car.setYear(result.getInt(CAR_YEAR_ATTRIBUTE));
		String condition = result.getString(CAR_CONDITION_ATTRIBUTE).toUpperCase();
		CarCondition carCondition = CarCondition.valueOf(condition);
		car.setCarCondition(carCondition);
		return car;
	}
	/**Retrieves driver (user) data from resultset and returns user object keeping this data.
	 * @param result SQL resultset
	 * @return User object with needed data
	 * @throws SQLException -when there are problems  while retrieving data
	 */
	public static User getDriverFromResultSet(ResultSet result) throws SQLException {
		User user = new User();
		user.setId(result.getInt(ID_ATTRIBUTE));
		user.setFirstName(result.getString(FIRST_NAME_ATTRIBUTE));
		user.setLastName(result.getString(LAST_NAME_ATTRIBUTE));
		user.setPhoneNumber(result.getString(PHONE_NUMBER_ATTRIBUTE));
		user.setEmail(result.getString(EMAIL_ATTRIBUTE));
		return user;
	}
	/**Retrieves order data from resultset and returns order object keeping this data.
	 * @param result SQL resultset
	 * @return Order object with needed data
	 * @throws SQLException -when there are problems  while retrieving data
	 */
	public static Order getOrderFromResultSet(ResultSet result) throws SQLException {
		Order order = new Order();
		order.setId(result.getInt(ID_ATTRIBUTE));
		order.setDescription(result.getString(DESCRIPTION_ATTRIBUTE));
		order.setMaxLoadHeight(result.getInt(MAX_LOAD_HEIGHT_ATTRIBUTE));
		order.setMaxLoadWidth(result.getInt(MAX_LOAD_WIDTH_ATTRIBUTE));
		order.setMaxLoadLength(result.getInt(MAX_LOAD_LENGTH_ATTRIBUTE));
		order.setWeight(result.getInt(WEIGHT_ATTRIBUTE));
		order.setVolume(result.getInt(VOLUME_ATTRIBUTE));
		order.setArrivalPoint(result.getString(CITY_ARRIVAL_NAME_ATTRIBUTE));
		order.setDeparturePoint(result.getString(CITY_DEPARTURE_NAME_ATTRIBUTE));
		order.setStatus(OrderStatus.valueOf(result.getString(ORDER_STATUS_ATTRIBUTE).toUpperCase()));
		return order;
	}
	/**Retrieves order data from resultset with complete details about departure and arrival and returns order object keeping this data.
	 * @param result SQL resultset
	 * @return Order object with needed data
	 * @throws SQLException -when there are problems  while retrieving data
	 */
	public static Order getOrderFromResultSetDetailed(ResultSet result) throws SQLException {
		Order order = new Order();
		order.setId(result.getInt(ID_ATTRIBUTE));
		order.setDescription(result.getString(DESCRIPTION_ATTRIBUTE));
		order.setMaxLoadHeight(result.getInt(MAX_LOAD_HEIGHT_ATTRIBUTE));
		order.setMaxLoadWidth(result.getInt(MAX_LOAD_WIDTH_ATTRIBUTE));
		order.setMaxLoadLength(result.getInt(MAX_LOAD_LENGTH_ATTRIBUTE));
		order.setWeight(result.getInt(WEIGHT_ATTRIBUTE));
		order.setVolume(result.getInt(VOLUME_ATTRIBUTE));
		order.setArrivalPoint(result.getString(CITY_ARRIVAL_NAME_ATTRIBUTE));
		order.setArrivalPointId(result.getInt(CITY_ARRIVAL_ID_ATTRIBUTE));
		order.setDeparturePoint(result.getString(CITY_DEPARTURE_NAME_ATTRIBUTE));
		order.setDeparturePointId(result.getInt(CITY_DEPARTURE_ID_ATTRIBUTE));
		order.setArrivalRegion(result.getString(REGION_ARRIVAL_NAME_ATTRIBUTE));
		order.setArrivalRegionId(result.getInt(REGION_ARRIVAL_ID_ATTRIBUTE));
		order.setDepartureRegion(result.getString(REGION_DEPARTURE_NAME_ATTRIBUTE));
		order.setDepartureRegionId(result.getInt(REGION_DEPARTURE_ID_ATTRIBUTE));
		order.setArrivalCountry(result.getString(COUNTRY_ARRIVAL_NAME_ATTRIBUTE));
		order.setArrivalCountryId(result.getInt(COUNTRY_ARRIVAL_ID_ATTRIBUTE));
		order.setDepartureCountry(result.getString(COUNTRY_DEPARTURE_NAME_ATTRIBUTE));
		order.setDepartureCountryId(result.getInt(COUNTRY_DEPARTURE_ID_ATTRIBUTE));
		order.setStatus(OrderStatus.valueOf(result.getString(ORDER_STATUS_ATTRIBUTE).toUpperCase()));
		return order;
	}
	/**Retrieves  data about user from resultset and returns user object keeping this data.
	 * @param result SQL resultset
	 * @return User object with needed data
	 * @throws SQLException -when there are problems  while retrieving data
	 */
	public static User getUserFromResultSet(ResultSet result) throws SQLException {
		User user = new User();
		user.setId(result.getInt(ID_ATTRIBUTE));
		user.setFirstName(result.getString(LOGIN_ATTRIBUTE));
		user.setFirstName(result.getString(FIRST_NAME_ATTRIBUTE));
		user.setLastName(result.getString(LAST_NAME_ATTRIBUTE));
		user.setPhoneNumber(result.getString(PHONE_NUMBER_ATTRIBUTE));
		user.setEmail(result.getString(EMAIL_ATTRIBUTE));
		user.setRole(UserRole.valueOf(result.getString(ROLE_ATTRIBUTE).toUpperCase()));
		return user;
	}
}
