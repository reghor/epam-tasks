package by.reghor.logistics.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**Class for generating  password hash.
 * @author Hornet
 *
 */
public final class PasswordHasher {
	private static final String HASHING_ALGORITHM_NAME="SHA-256";
	private static final String ENCODING_NAME="UTF-8";
	private static final String ZERO="0";
	
	private  PasswordHasher() {
	}
	/**Generates password hash according to sha256 algorithm 
	 * @param base password String
	 * @return password hash
	 * @throws NoSuchAlgorithmException when can not find hashing algorithm
	 * @throws UnsupportedEncodingException when password String encoding is not supported 
	 */
	public static String sha256(String base) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		StringBuffer hexString=null;
	        MessageDigest digest = MessageDigest.getInstance(HASHING_ALGORITHM_NAME);
	        byte[] hash = digest.digest(base.getBytes(ENCODING_NAME));
	         hexString = new StringBuffer();
	        for (int i = 0; i < hash.length; i++) {
	            String hex = Integer.toHexString(0xff & hash[i]);
	            if(hex.length() == 1) hexString.append(ZERO);
	            hexString.append(hex);
	        }
	        return hexString.toString();
	}
	public static void main(String args[]) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		System.out.println(sha256("aA1%aa"));
	}
}
