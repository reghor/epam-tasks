package by.reghor.logistics.util;

import javax.servlet.http.HttpServletRequest;

import by.reghor.logistics.entity.Car;
import by.reghor.logistics.entity.Order;
import by.reghor.logistics.entity.User;

/**
 * Class for validating user input in the form of String object using regular
 * expressions.
 * 
 * @author Hornet
 *
 */
public class AttributeDigester {

	private static final String MAX_INTEGER_STRING = String.valueOf(Integer.MAX_VALUE);
	private static final String ORDER_ID_ATTRIBUTE = "order_id";
	private static final String DRIVER_ID_ATTRIBUTE = "driver_id";
	private static final String CAR_ID_ATTRIBUTE = "car_id";
	private static final String USER_ATTRIBUTE = "user";
	private static final String LOCALE_ATTRIBUTE = "local";
	private static final String LAST_QUERY_STRING_ATTRIBUTE = "lastQueryString";

	private static final String CAR_NUMBER_ATTRIBUTE = "car_vehicle_id_number";
	private static final String CAR_MAKE_OF_CAR_ATTRIBUTE = "make_of_car";
	private static final String CAR_YEAR_ATTRIBUTE = "car_year";
	private static final String CAR_MAX_LOAD_HEIGHT_ATTRIBUTE = "car_max_height";
	private static final String CAR_MAX_LOAD_LENGTH_ATTRIBUTE = "car_max_length";
	private static final String CAR_MAX_LOAD_WIDTH_ATTRIBUTE = "car_max_width";
	private static final String CAR_MAX_WEIGHT_ATTRIBUTE = "car_max_weight";
	private static final String CAR_MAX_VOLUME_ATTRIBUTE = "car_max_volume";
	private static final String CAR_COLOUR_ATTRIBUTE = "car_colour";

	private static final String ORDER_DESCRIPTION_ATTRIBUTE = "description";
	private static final String ORDER_MAX_LOAD_HEIGHT_ATTRIBUTE = "max_load_height";
	private static final String ORDER_MAX_LOAD_LENGTH_ATTRIBUTE = "max_load_length";
	private static final String ORDER_MAX_LOAD_WIDTH_ATTRIBUTE = "max_load_width";
	private static final String ORDER_WEIGHT_ATTRIBUTE = "weight";
	private static final String ORDER_VOLUME_ATTRIBUTE = "volume";
	private static final String ORDER_CITY_ARRIVAL_ATTRIBUTE = "cities_arrival";
	private static final String ORDER_CITY_DEPARTURE_ATTRIBUTE = "cities_departure";
	
	private static final String FIRST_NAME_ATTRIBUTE = "first_name";
	private static final String LAST_NAME_ATTRIBUTE = "last_name";
	private static final String LOGIN_ATTRIBUTE = "login";
	private static final String PHONE_NUMBER_ATTRIBUTE = "phone_number";
	private static final String EMAIL_ATTRIBUTE = "email";
	private static final String PASSWORD_ATTRIBUTE = "password";
	private static final String CONFIRM_PASSWORD_ATTRIBUTE = "confirm_password";

	private static final String WRONG_FIRST_NAME_ATTRIBUTE = "wrong_first_name";
	private static final String WRONG_LAST_NAME_ATTRIBUTE = "wrong_last_name";
	private static final String WRONG_LOGIN_ATTRIBUTE = "wrong_login";
	private static final String WRONG_PHONE_NUMBER_ATTRIBUTE = "wrong_phone_number";
	private static final String WRONG_EMAIL_ATTRIBUTE = "wrong_email";
	private static final String WRONG_PASSWORD_ATTRIBUTE = "wrong_password";
	private static final String PASSWORD_MATCH_ERROR_ATTRIBUTE = "password_match_error";
	
	

	private AttributeDigester() {
	}

	/**
	 * Checks does user input match regular expression
	 * 
	 * @param userInput
	 *            - user input String
	 * @param regEx
	 *            - regular expression
	 * @return true if user input matches pattern and if not or value is null
	 *         false
	 */
	public static boolean validate(String userInput, String regEx) {
		if (userInput != null) {
			return userInput.matches(regEx);
		}
		return false;

	}

	/**
	 * Checks is string positive integer or not (from 0 to max value of integer
	 * type)
	 * 
	 * @param numberStr
	 *            string for test
	 * @return true if string is not null and in range 0 to max value of
	 *         integer, otherwise - false
	 */
	public static boolean isPositiveInteger(String numberStr) {
		if (numberStr == null) {
			return false;
		}
		int numberStrLength = numberStr.length();
		int maxIntegerStrLength = MAX_INTEGER_STRING.length();
		if (numberStrLength == 0 || numberStrLength > maxIntegerStrLength) {
			return false;
		}
		if (numberStrLength == 10) {
			for (int i = 0; i < numberStrLength; i++) {
				char c = numberStr.charAt(i);
				if (c < '0' || c > MAX_INTEGER_STRING.charAt(i)) {
					return false;
				}
			}
		} else {
			for (int i = 0; i < numberStrLength; i++) {
				char c = numberStr.charAt(i);
				if (c < '0' || c > '9') {
					return false;
				}
			}
		}
		return true;
	}

	public static boolean isValidOrderId(HttpServletRequest request) {
		
		String orderIdString = request.getParameter(ORDER_ID_ATTRIBUTE);
		return isPositiveInteger(orderIdString);
	}

	public static boolean isValidCarId(HttpServletRequest request) {
		String orderIdString = request.getParameter(CAR_ID_ATTRIBUTE);
		return isPositiveInteger(orderIdString);
	}

	public static boolean isValidDriverId(HttpServletRequest request) {
		String orderIdString = request.getParameter(DRIVER_ID_ATTRIBUTE);
		return isPositiveInteger(orderIdString);
	}

	public static int getOrderId(HttpServletRequest request) {
		return Integer.parseInt(request.getParameter(ORDER_ID_ATTRIBUTE));
	}

	public static int getCarId(HttpServletRequest request) {
		return Integer.parseInt(request.getParameter(CAR_ID_ATTRIBUTE));
	}

	public static int getDriverId(HttpServletRequest request) {
		return Integer.parseInt(request.getParameter(DRIVER_ID_ATTRIBUTE));
	}

	public static int getCurrentUserId(HttpServletRequest request) {
		return ((User) request.getSession().getAttribute(USER_ATTRIBUTE)).getId();
	}

	public static Car getCar(HttpServletRequest request) {
		String carNumber = request.getParameter(CAR_NUMBER_ATTRIBUTE);
		String makeOfCar = request.getParameter(CAR_MAKE_OF_CAR_ATTRIBUTE);
		String carColour = request.getParameter(CAR_COLOUR_ATTRIBUTE);
		String carYearString = request.getParameter(CAR_YEAR_ATTRIBUTE);
		String carMaxHeightString = request.getParameter(CAR_MAX_LOAD_HEIGHT_ATTRIBUTE);
		String carMaxWidthString = request.getParameter(CAR_MAX_LOAD_WIDTH_ATTRIBUTE);
		String carMaxLengthString = request.getParameter(CAR_MAX_LOAD_LENGTH_ATTRIBUTE);
		String carMaxVolumeString = request.getParameter(CAR_MAX_VOLUME_ATTRIBUTE);
		String carMaxWeightString = request.getParameter(CAR_MAX_WEIGHT_ATTRIBUTE);

		Car car = null;

		boolean wrongInputFlag = false;

		if (!AttributeDigester.validate(carNumber, RegExManager.getValue(RegExManager.CAR_NUMBER_PATTERN))) {
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(makeOfCar, RegExManager.getValue(RegExManager.MAKE_OF_CAR_PATTERN))) {
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(carColour, RegExManager.getValue(RegExManager.CAR_COLOUR_PATTERN))) {
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(carYearString, RegExManager.getValue(RegExManager.CAR_YEAR_PATTERN))) {
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(carMaxHeightString, RegExManager.getValue(RegExManager.ORDER_NUMERIC_FIELD_PATTERN))) {
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(carMaxWidthString, RegExManager.getValue(RegExManager.ORDER_NUMERIC_FIELD_PATTERN))) {
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(carMaxLengthString, RegExManager.getValue(RegExManager.ORDER_NUMERIC_FIELD_PATTERN))) {
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(carMaxVolumeString, RegExManager.getValue(RegExManager.ORDER_NUMERIC_FIELD_PATTERN))) {
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(carMaxWeightString, RegExManager.getValue(RegExManager.ORDER_NUMERIC_FIELD_PATTERN))) {
			wrongInputFlag = true;
		}
		System.out.println("CAR IS: "+wrongInputFlag);
		if (!wrongInputFlag) {
			int minCarYear = Integer.parseInt(RegExManager.getValue(RegExManager.MIN_CAR_YEAR));
			int maxCarYear = Integer.parseInt(RegExManager.getValue(RegExManager.MAX_CAR_YEAR));
			int carYear = Integer.parseInt(carYearString);
			if (carYear >= minCarYear && carYear <= maxCarYear) {
				System.out.println("CAR OK");
				car = new Car();
				int carMaxHeight = Integer.parseInt(carMaxHeightString);
				int carMaxLength = Integer.parseInt(carMaxLengthString);
				int carMaxWidth = Integer.parseInt(carMaxWidthString);
				int carMaxVolume = Integer.parseInt(carMaxVolumeString);
				int carMaxWeight = Integer.parseInt(carMaxWeightString);
				car.setVehicleIdNumber(carNumber);
				car.setMakeOfCar(makeOfCar);
				car.setColour(carColour);
				car.setYear(carYear);
				car.setMaxHeight(carMaxHeight);
				car.setMaxWidth(carMaxWidth);
				car.setMaxVolume(carMaxVolume);
				car.setMaxLength(carMaxLength);
				car.setMaxWeight(carMaxWeight);
			}
		}
		return car;
	}

	public static Order getOrder(HttpServletRequest request) {
		Order order = null;
		String description = request.getParameter(ORDER_DESCRIPTION_ATTRIBUTE);
		String maxLoadHeightString = request.getParameter(ORDER_MAX_LOAD_HEIGHT_ATTRIBUTE);
		String maxLoadWidthString = request.getParameter(ORDER_MAX_LOAD_WIDTH_ATTRIBUTE);
		String maxLoadLengthString = request.getParameter(ORDER_MAX_LOAD_LENGTH_ATTRIBUTE);
		String weightString = request.getParameter(ORDER_WEIGHT_ATTRIBUTE);
		String volumeString = request.getParameter(ORDER_VOLUME_ATTRIBUTE);
		String arrivalPointIdString = request.getParameter(ORDER_CITY_ARRIVAL_ATTRIBUTE);
		String departurePointIdString = request.getParameter(ORDER_CITY_DEPARTURE_ATTRIBUTE);
		System.out.println("DEPid"+departurePointIdString);
		boolean wrongInputFlag = false;

		if (!AttributeDigester.validate(description, RegExManager.getValue(RegExManager.ORDER_DESCRIPTION_PATTERN))) {
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(maxLoadHeightString, RegExManager.getValue(RegExManager.ORDER_NUMERIC_FIELD_PATTERN))) {
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(maxLoadWidthString, RegExManager.getValue(RegExManager.ORDER_NUMERIC_FIELD_PATTERN))) {
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(maxLoadLengthString, RegExManager.getValue(RegExManager.ORDER_NUMERIC_FIELD_PATTERN))) {
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(weightString, RegExManager.getValue(RegExManager.ORDER_NUMERIC_FIELD_PATTERN))) {
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(volumeString, RegExManager.getValue(RegExManager.ORDER_NUMERIC_FIELD_PATTERN))) {
			wrongInputFlag = true;
		} else if (!AttributeDigester.isPositiveInteger(arrivalPointIdString)) {
			wrongInputFlag = true;
		} else if (!AttributeDigester.isPositiveInteger(departurePointIdString)) {
			wrongInputFlag = true;
		}

		if (!wrongInputFlag) {
			order = new Order();
			int maxLoadHeight = Integer.parseInt(maxLoadHeightString);
			int maxLoadLength = Integer.parseInt(maxLoadLengthString);
			int maxLoadWidth = Integer.parseInt(maxLoadWidthString);
			int weight = Integer.parseInt(weightString);
			int volume = Integer.parseInt(volumeString);
			int arrivalPointId = Integer.parseInt(arrivalPointIdString);
			int departurePointId = Integer.parseInt(departurePointIdString);
			order.setDescription(description);
			order.setMaxLoadHeight(maxLoadHeight);
			order.setMaxLoadLength(maxLoadLength);
			order.setMaxLoadWidth(maxLoadWidth);
			order.setWeight(weight);
			order.setVolume(volume);
			order.setArrivalPointId(arrivalPointId);
			order.setDeparturePointId(departurePointId);

		}
		return order;
	}
	
	public static Order getOrderLoadInfo(HttpServletRequest request) {
		Order order = null;
		String maxLoadHeightString = request.getParameter(ORDER_MAX_LOAD_HEIGHT_ATTRIBUTE);
		String maxLoadWidthString = request.getParameter(ORDER_MAX_LOAD_WIDTH_ATTRIBUTE);
		String maxLoadLengthString = request.getParameter(ORDER_MAX_LOAD_LENGTH_ATTRIBUTE);
		String weightString = request.getParameter(ORDER_WEIGHT_ATTRIBUTE);
		String volumeString = request.getParameter(ORDER_VOLUME_ATTRIBUTE);
		boolean wrongInputFlag = false;

		if (!AttributeDigester.validate(maxLoadHeightString, RegExManager.getValue(RegExManager.ORDER_NUMERIC_FIELD_PATTERN))) {
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(maxLoadWidthString, RegExManager.getValue(RegExManager.ORDER_NUMERIC_FIELD_PATTERN))) {
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(maxLoadLengthString, RegExManager.getValue(RegExManager.ORDER_NUMERIC_FIELD_PATTERN))) {
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(weightString, RegExManager.getValue(RegExManager.ORDER_NUMERIC_FIELD_PATTERN))) {
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(volumeString, RegExManager.getValue(RegExManager.ORDER_NUMERIC_FIELD_PATTERN))) {
			wrongInputFlag = true;
		} 

		if (!wrongInputFlag) {
			order = new Order();
			int maxLoadHeight = Integer.parseInt(maxLoadHeightString);
			int maxLoadLength = Integer.parseInt(maxLoadLengthString);
			int maxLoadWidth = Integer.parseInt(maxLoadWidthString);
			int weight = Integer.parseInt(weightString);
			int volume = Integer.parseInt(volumeString);
			order.setMaxLoadHeight(maxLoadHeight);
			order.setMaxLoadLength(maxLoadLength);
			order.setMaxLoadWidth(maxLoadWidth);
			order.setWeight(weight);
			order.setVolume(volume);
		}
		return order;
	}
	
	public static User getUser(HttpServletRequest request){
		String firstName = request.getParameter(FIRST_NAME_ATTRIBUTE);
		String lastName = request.getParameter(LAST_NAME_ATTRIBUTE);
		String login = request.getParameter(LOGIN_ATTRIBUTE);
		String phoneNumber = request.getParameter(PHONE_NUMBER_ATTRIBUTE);
		String email = request.getParameter(EMAIL_ATTRIBUTE);
		String password = request.getParameter(PASSWORD_ATTRIBUTE);
		String confirmationPassword = request.getParameter(CONFIRM_PASSWORD_ATTRIBUTE);
		boolean wrongInputFlag = false;
		User user=null;

		if (!AttributeDigester.validate(firstName, RegExManager.getValue(RegExManager.FIRST_NAME_PATTERN))) {
			request.setAttribute(WRONG_FIRST_NAME_ATTRIBUTE, true);
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(lastName, RegExManager.getValue(RegExManager.LAST_NAME_PATTERN))) {
			request.setAttribute(WRONG_LAST_NAME_ATTRIBUTE, true);
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(login, RegExManager.getValue(RegExManager.LOGIN_PATTERN))) {
			request.setAttribute(WRONG_LOGIN_ATTRIBUTE, true);
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(phoneNumber, RegExManager.getValue(RegExManager.PHONE_NUMBER_PATTERN))) {
			request.setAttribute(WRONG_PHONE_NUMBER_ATTRIBUTE, true);
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(email, RegExManager.getValue(RegExManager.EMAIL_PATTERN))) {
			request.setAttribute(WRONG_EMAIL_ATTRIBUTE, true);
			wrongInputFlag = true;
		} else if (!AttributeDigester.validate(password, RegExManager.getValue(RegExManager.PASSWORD_PATTERN))) {
			request.setAttribute(WRONG_PASSWORD_ATTRIBUTE, true);
			wrongInputFlag = true;
		} else if (!password.equals(confirmationPassword)) {
			request.setAttribute(PASSWORD_MATCH_ERROR_ATTRIBUTE, true);
			wrongInputFlag = true;
		}
		
		if (!wrongInputFlag){
			user=new User();
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setLogin(login);
			user.setPhoneNumber(phoneNumber);
			user.setEmail(email);
			user.setPassword(password);
		}
		return user;

	}
	
	public static User getCurrentUser(HttpServletRequest request){
		return (User)request.getSession().getAttribute(USER_ATTRIBUTE);
	}
	
	public static String getCurrentLocale(HttpServletRequest request){
		return (String)request.getSession().getAttribute(LOCALE_ATTRIBUTE);
	}
	
	public static String getLastQueryString(HttpServletRequest request){
		return (String)request.getSession().getAttribute(LAST_QUERY_STRING_ATTRIBUTE);
	}
}
