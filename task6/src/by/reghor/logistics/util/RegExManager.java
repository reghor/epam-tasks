package by.reghor.logistics.util;

import java.util.ResourceBundle;

/**Class for working with regular expressions
 * @author Hornet
 *
 */
public final class RegExManager {
	public static final String EMAIL_PATTERN = "email";
	public static final String FIRST_NAME_PATTERN = "name.first";
	public static final String LAST_NAME_PATTERN = "name.last";
	public static final String LOGIN_PATTERN = "login";
	public static final String ORDER_DESCRIPTION_PATTERN = "order.description";
	public static final String ORDER_NUMERIC_FIELD_PATTERN = "order.field.number";
	public static final String PASSWORD_PATTERN = "password";
	public static final String PHONE_NUMBER_PATTERN = "phone.number";
	public static final String POSITIVE_NUMBER_PATTERN = "positive.number";
	public static final String CAR_NUMBER_PATTERN = "car.id";
	public static final String CAR_COLOUR_PATTERN = "car.colour";
	public static final String MAKE_OF_CAR_PATTERN = "make.of.car";
	public static final String CAR_YEAR_PATTERN = "car.year";
	public static final String MIN_CAR_YEAR="min.year";
	public static final String MAX_CAR_YEAR="max.year";
	public static final String REGEX_BUNDLE_NAME = "regex";

	private static final ResourceBundle resourceBundle = ResourceBundle.getBundle(REGEX_BUNDLE_NAME);

	private RegExManager() {

	}

	/**Retrieves regular expression from file according to its name
	 * @param regExName name of the required regular expression
	 * @return regular expression
	 */
	public static String getValue(String regExName) {
		return resourceBundle.getString(regExName);
	}
}
