package by.reghor.logistics.util;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import by.reghor.logistics.service.command.CommandName;

/**Class for generating page paths and URLs with command requests and parameters.
 * @author Hornet
 *
 */
public final class PageManager {
	public static final String CUSTOMER_ALL_ORDERS="customer.order.all";
	public static final String CUSTOMER_CREATE_NEW_ORDER="customer.order.create";
	public static final String CUSTOMER_EDIT_ORDER="customer.order.edit";
	public static final String CUSTOMER_MAIN_PAGE= "customer.main";
	public static final String CUSTOMER_REVIEW_ORDER="customer.order.review";
	
	public static final String DISPATCHER_MAIN="dispatcher.main";
	public static final String DISPATCHER_ADD_NEW_CAR="dispatcher.car.add";
	public static final String DISPATCHER_ADD_DRIVER="dispatcher.add.driver";
	public static final String DISPATCHER_ADD_NEW_ORDER="dispatcher.order.new";
	public static final String DISPATCHER_ALL_CARS="dispatcher.car.all";
	public static final String DISPATCHER_ALL_DRIVERS="dispatcher.driver.all";
	public static final String DISPATCHER_ALL_ORDERS_PAGE="dispatcher.order.all";
	public static final String DISPATCHER_ASSIGN_ORDER="dispatcher.order.assign";
	public static final String DISPATCHER_CREATE_NEW_ORDER="dispatcher.order.create";
	public static final String DISPATCHER_DRIVER_CAR="dispatcher.driver.car";
	public static final String DISPATCHER_REGISTER_DRIVER="dispatcher.driver.register";
	public static final String DISPATCHER_REVIEW_ORDER="dispatcher.order.review";
	public static final String DISPATCHER_FREE_CARS="dispatcher.car.free";
	
	public static final String DRIVER_MAIN = "driver.main";
	public static final String DRIVER_REVIEW_CAR="driver.car.review";
	public static final String DRIVER_REVIEW_ORDER="driver.order.review";
	
	public static final String GUEST_INDEX = "common.index";
	public static final String GUEST_LOGIN = "common.login";
	public static final String GUEST_REGISTRATION="common.registration";
	public static final String GUEST_WELCOME="common.welcome";
	
	private static final String PAGE_BUNDLE_NAME="page_name";
	private static final String CONTROLLER_COMMAND_REQUEST="/controller?command=";
	private static final String CONTROLLER="/controller?";
	private static final String LAST_QUERY_STRING="lastQueryString";
	private static final String SUCCESSFUL_ATTRIBUTE="&success=true";
	private static final String NOT_SUCCESSFUL_ATTRIBUTE="&success=false";
	private static final String NO_SUCH_ORDER_ATTRIBUTE="&no_such_order=true";
	private static final String NO_SUCH_CAR_ATTRIBUTE="&no_such_car=true";
	private static final String NO_SUCH_DRIVER_ATTRIBUTE="&no_such_driver=true";
	private static final String ORDER_ID_ATTRIBUTE="&order_id=";
	private static final String DRIVER_ID_ATTRIBUTE="&driver_id=";

	private final ResourceBundle bundle = ResourceBundle.getBundle(PAGE_BUNDLE_NAME);
	
	private static final PageManager instance=new PageManager();

	private PageManager(){
	}
	
	/**Instantiates PageManager
	 * @return instance of PageManager
	 */
	public static PageManager getInstance() {
		
		return instance;
	}
	
	/**Generates URL containing command request according to command name ("/controller?command=command_name")
	 * @param commandName command name
	 * @return URL containing command request
	 */
	public String generateCommandRequest(CommandName commandName){
		return CONTROLLER_COMMAND_REQUEST+commandName.toString().toLowerCase();
	}
	/**Generates URL containing command request according to command name with success parameter which equals true ("/controller?command=command_name&success=true")
	 * @param commandName command name
	 * @return url containing command request with parameter success set to true
	 */
	public String generateCommandRequestSuccessful(CommandName commandName){
		StringBuilder sb=new StringBuilder();
		sb.append(CONTROLLER_COMMAND_REQUEST).append(commandName.toString().toLowerCase()).append(SUCCESSFUL_ATTRIBUTE);
		return sb.toString();
	}
	/**Generates url containing command request according to command name with no_such_order parameter which equals true ("/controller?command=command_name&no_such_order=true")
	 * @param commandName command name
	 * @return url containing command request with parameter no_such_order set to true
	 */
	public String generateCommandRequestNoSuchOrder(CommandName commandName){
		StringBuilder sb=new StringBuilder();
		sb.append(CONTROLLER_COMMAND_REQUEST).append(commandName.toString().toLowerCase()).append(NO_SUCH_ORDER_ATTRIBUTE);
		return sb.toString();
	}
	/**Generates url containing command request according to command name with no_such_car parameter which equals true ("/controller?command=command_name&no_such_car=true")
	 * @param commandName command name
	 * @return url containing command request with parameter no_such_car set to true
	 */
	public String generateCommandRequestNoSuchCar(CommandName commandName){
		StringBuilder sb=new StringBuilder();
		sb.append(CONTROLLER_COMMAND_REQUEST).append(commandName.toString().toLowerCase()).append(NO_SUCH_CAR_ATTRIBUTE);
		return sb.toString();
	}
	/**Generates url containing command request according to command name with no_such_driver parameter which equals false ("/controller?command=command_name&no_such_driver=true")
	 * @param commandName command name
	 * @return url containing command request with parameter no_such_driver set to true
	 */
	public String generateCommandRequestNoSuchDriver(CommandName commandName){
		StringBuilder sb=new StringBuilder();
		sb.append(CONTROLLER_COMMAND_REQUEST).append(commandName.toString().toLowerCase()).append(NO_SUCH_DRIVER_ATTRIBUTE);
		return sb.toString();
	}
	/**Generates url containing command request according to command name with success parameter which equals false ("/controller?command=command_name&success=false")
	 * @param commandName command name
	 * @return url containing command request with parameter success set to false
	 */
	public String generateCommandRequestNotSuccessful(CommandName commandName){
		StringBuilder sb=new StringBuilder();
		sb.append(CONTROLLER_COMMAND_REQUEST).append(commandName.toString().toLowerCase()).append(NOT_SUCCESSFUL_ATTRIBUTE);
		return sb.toString();
	}
	/**Generates last query string, which user used during his session 
	 * @param request request object
	 * @return url containing last query string, which user used during his session 
	 */
	public String generateLastQueryStringRequest( HttpServletRequest request){
		return CONTROLLER+request.getSession().getAttribute(LAST_QUERY_STRING);
	}
	/**Generates query on order review  according to order ID
	 * @param orderId order ID for review
	 * @return url containing  query string on reviewal of order
	 */
	public String generateReviewOrderRequest(int orderId){
		StringBuilder sb=new StringBuilder(CONTROLLER_COMMAND_REQUEST);
		sb.append(CommandName.REVIEW_ORDER.toString().toLowerCase()).append(ORDER_ID_ATTRIBUTE).append(orderId);
		return sb.toString();
	}
	/**Generates query on  driver review according to order ID
	 * @param driverId driver ID for review
	 * @return url containing last query string on reviewal of driver
	 */
	public String generateReviewDriverCarRequest(int driverId){
		StringBuilder sb=new StringBuilder(CONTROLLER_COMMAND_REQUEST);
		sb.append(CommandName.GET_DRIVER_CAR.toString().toLowerCase()).append(DRIVER_ID_ATTRIBUTE).append(driverId);
		return sb.toString();
	}

	/**Gets page path according to the String key provided
	 * @param key page key
	 * @return page path according to the String key provided
	 */
	public String getPage(String key) {
		return bundle.getString(key);
	}
}
