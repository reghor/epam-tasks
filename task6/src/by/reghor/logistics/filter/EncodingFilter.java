package by.reghor.logistics.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class EncodingFilter implements Filter {

	private String defaultEncoding;
	private String ENCODING_ATTRIBUTE = "encoding";

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException,
			ServletException {
		String codeRequest = servletRequest.getCharacterEncoding();
		if (defaultEncoding != null && !defaultEncoding.equalsIgnoreCase(codeRequest)) {
			servletRequest.setCharacterEncoding(defaultEncoding);
			servletResponse.setCharacterEncoding(defaultEncoding);
		}
		filterChain.doFilter(servletRequest, servletResponse);
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		defaultEncoding = filterConfig.getInitParameter(ENCODING_ATTRIBUTE);
	}

	@Override
	public void destroy() {
	}
}
