package by.reghor.logistics.filter;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.reghor.logistics.entity.User;
import by.reghor.logistics.entity.UserRole;
import by.reghor.logistics.service.ajax.AjaxCommandName;
import by.reghor.logistics.service.command.CommandName;
import by.reghor.logistics.util.PageManager;

public class SecurityFilter implements Filter {

	private static final Set<String> ajaxCommands = new HashSet<>();
	private static final Set<String> commands = new HashSet<>();
	private static final String COMMAND_PARAMETER_NAME = "command";
	private static final String AJAX_ENDING = "_ajax";
	private static final String USER_ATTRIBUTE_NAME = "user";

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		for (AjaxCommandName ajaxCommandName : AjaxCommandName.values()) {
			ajaxCommands.add(ajaxCommandName.toString().toLowerCase());
		}
		for (CommandName CommandName : CommandName.values()) {
			commands.add(CommandName.toString().toLowerCase());
		}
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException,
			ServletException {
		System.out.println("IN FILTER");
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		String commandName = request.getParameter(COMMAND_PARAMETER_NAME);
		boolean correctCommandName = false;
		if (commandName.endsWith(AJAX_ENDING) && ajaxCommands.contains(commandName)
				&& isAjaxCommandPermitted(AjaxCommandName.valueOf(commandName.toUpperCase()), request)) {
			correctCommandName = true;
		} else if (commands.contains(commandName) && isCommandPermitted(CommandName.valueOf(commandName.toUpperCase()), request)) {
			correctCommandName = true;
		}
		if (!correctCommandName) {
			String page = PageManager.getInstance().generateCommandRequest(CommandName.LOGIN_PAGE_REFERENCE);
			response.sendRedirect(request.getContextPath() + page);
			System.out.println("WRONG COMMAND");
			return;
		}
		System.out.println("OK COMMAND");
		filterChain.doFilter(request, response);
	}

	@Override
	public void destroy() {

	}

	private boolean isCommandPermitted(CommandName command, HttpServletRequest request) {
		boolean commandPermitted = false;
		Set<UserRole> permittedUserRoles = command.getPermittedUserRoles();
		if (permittedUserRoles.isEmpty()) {
			commandPermitted = true;
		} else {
			User user = (User) request.getSession().getAttribute(USER_ATTRIBUTE_NAME);
			UserRole userRole = user.getRole();
			if (permittedUserRoles.contains(userRole)) {
				commandPermitted = true;
			}
		}
		return commandPermitted;
	}

	private boolean isAjaxCommandPermitted(AjaxCommandName command, HttpServletRequest request) {
		boolean commandPermitted = false;
		Set<UserRole> permittedUserRoles = command.getPermittedUserRoles();
		if (permittedUserRoles.isEmpty()) {
			commandPermitted = true;
		} else {
			User user = (User) request.getSession().getAttribute(USER_ATTRIBUTE_NAME);
			UserRole userRole = user.getRole();
			if (permittedUserRoles.contains(userRole)) {
				commandPermitted = true;
			}
		}
		return commandPermitted;
	}
}
