package by.reghor.logistics.entity;

/**Enum representing possible car conditions
 * @author Hornet
 *
 */
public enum CarCondition {
	FAULTLESS, FAULTY;

	private String lowerCaseValue = this.toString().toLowerCase();

	/**Gets String lowercase representation of car condition
	 * @return String lowercase representation of car condition
	 */
	public String getLowerCaseValue() {
		return lowerCaseValue;
	}

}
