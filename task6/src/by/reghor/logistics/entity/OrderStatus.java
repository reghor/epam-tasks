package by.reghor.logistics.entity;

/**Enum representing order statuses
 * @author Hornet
 *
 */
public enum OrderStatus {
	ACCEPTED, ACCOMPLISHED, ASSIGNED, REGISTERED, DECLINED;

	private String lowerCaseValue = this.toString().toLowerCase();

	/**Gets String representation of order status
	 * @return String representation of order status
	 */
	public String getLowerCaseValue() {
		return lowerCaseValue;
	}

}
