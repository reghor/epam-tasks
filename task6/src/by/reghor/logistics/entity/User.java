package by.reghor.logistics.entity;

import java.io.Serializable;

/**
 * @author Hornet
 *
 */
public class User implements Serializable {
	private static final long serialVersionUID = 1L;
	private String email;
	private String firstName;
	private int id;
	private String lastName;
	private String login;
	private String password;
	private String phoneNumber;
	private UserRole role;

	/**
	 * User Entity Class constructor
	 */
	public User() {
	}

	/**
	 * Gets user email
	 * 
	 * @return user email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Gets user first name
	 * 
	 * @return user first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Gets user ID
	 * 
	 * @return user ID
	 */
	public int getId() {
		return id;
	}

	/**
	 * Gets user last name
	 * 
	 * @return user last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Gets user login
	 * 
	 * @return use login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * Gets user password
	 * 
	 * @return use password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Gets user phone number
	 * 
	 * @return user phone number
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * Gets user role
	 * 
	 * @return user role
	 */
	public UserRole getRole() {
		return role;
	}

	/**
	 * Sets user email
	 * 
	 * @param email
	 *            use email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Sets user first name
	 * 
	 * @param firstName
	 *            user first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Sets user ID
	 * 
	 * @param id
	 *            user ID
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Sets user last name
	 * 
	 * @param lastName
	 *            user last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Sets user login
	 * 
	 * @param login
	 *            user login
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * sets user password
	 * 
	 * @param password
	 *            user password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Sets user phone number
	 * 
	 * @param phoneNumber
	 *            user phone number
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Sets user role
	 * 
	 * @param role
	 *            user role
	 */
	public void setRole(UserRole role) {
		this.role = role;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + id;
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id != other.id)
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (role != other.role)
			return false;
		return true;
	}

}
