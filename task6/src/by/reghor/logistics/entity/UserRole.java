package by.reghor.logistics.entity;

/**
 * Enum representing user roles
 * 
 * @author Hornet
 *
 */
public enum UserRole {

	CUSTOMER, DISPATCHER, DRIVER, GUEST;

}
