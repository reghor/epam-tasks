package by.reghor.logistics.entity;

/**Enum representing possible car statuses.
 * @author Hornet
 *
 */
public enum CarStatus {
	BUSY, FREE;

	private String lowerCaseValue = this.toString().toLowerCase();

	/**Gets String lowercase representation of car status
	 * @return String lowercase car status
	 */
	public String getLowerCaseValue() {
		return lowerCaseValue;
	}

}
