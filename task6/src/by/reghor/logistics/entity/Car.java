package by.reghor.logistics.entity;

import java.io.Serializable;

/**
 * Car class is an entity class for storing information about car.
 * 
 * @author Hornet
 * 
 * 
 */
public class Car implements Serializable {
	private static final long serialVersionUID = 1L;
	private CarCondition carCondition;
	private CarStatus status;
	private String carConditionRepr;
	private String colour;
	private int id;
	private String makeOfCar;
	private int maxHeight;
	private int maxLength;
	private int maxVolume;
	private int maxWeight;
	private int maxWidth;
	private String vehicleIdNumber;
	private int year;

	public Car() {
	}

	/**
	 * Gets car condition
	 * 
	 * @return car condtion
	 */
	public CarCondition getCarCondition() {
		return carCondition;
	}

	/**
	 * Get string car condition representation
	 * 
	 * @return car condtion string representation
	 */
	public String getCarConditionRepr() {
		return carConditionRepr;
	}

	/**
	 * Gets car colour
	 * 
	 * @return car colour
	 */
	public String getColour() {
		return colour;
	}

	/**
	 * car ID according to database
	 * 
	 * @return car ID
	 */
	public int getId() {
		return id;
	}

	/**
	 * Gets make of car
	 * 
	 * @return make of car
	 */
	public String getMakeOfCar() {
		return makeOfCar;
	}

	/**
	 * Gets max height of load can be stored inside car
	 * 
	 * @return max load height
	 */
	public int getMaxHeight() {
		return maxHeight;
	}

	/**
	 * Gets max length of load can be stored inside car
	 * 
	 * @return max load length
	 */
	public int getMaxLength() {
		return maxLength;
	}

	/**
	 * Gets max total volume of load can be stored inside car
	 * 
	 * @return max load total volume
	 */
	public int getMaxVolume() {
		return maxVolume;
	}

	/**
	 * Gets max total weight of load can be stored inside car
	 * 
	 * @return max load total weight
	 */
	public int getMaxWeight() {
		return maxWeight;
	}

	/**
	 * Gets max width of load can be stored inside car
	 * 
	 * @return max load width
	 */
	public int getMaxWidth() {
		return maxWidth;
	}

	/**
	 * Gets car number
	 * 
	 * @return car number
	 */

	public String getVehicleIdNumber() {
		return vehicleIdNumber;
	}

	/**
	 * Gets car year
	 * 
	 * @return car year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * Sets car condition
	 * 
	 * @param carCondition
	 *            car condition
	 */
	public void setCarCondition(CarCondition carCondition) {
		this.carCondition = carCondition;
		setCarConditionRepr(carCondition.toString().toLowerCase());
	}

	/**
	 * Sets car condition String representation
	 * 
	 * @param carConditionRepr
	 *            String car condtion representation
	 */
	public void setCarConditionRepr(String carConditionRepr) {
		this.carConditionRepr = carConditionRepr;
	}

	/**
	 * Sets car colour
	 * 
	 * @param colour
	 *            car colur
	 */
	public void setColour(String colour) {
		this.colour = colour;
	}

	/**
	 * Sets car ID
	 * 
	 * @param id
	 *            car ID
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Sets make of car
	 * 
	 * @param makeOfCar
	 *            make of car
	 */
	public void setMakeOfCar(String makeOfCar) {
		this.makeOfCar = makeOfCar;
	}

	/**
	 * Sets max height of load can be stored in car
	 * 
	 * @param maxHeight
	 *            max height of load
	 */
	public void setMaxHeight(int maxHeight) {
		this.maxHeight = maxHeight;
	}

	/**
	 * Sets max length of load can be stored in car
	 * 
	 * @param maxHeight
	 *            max length of load
	 */
	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	/**
	 * Sets max total volume of load can be stored in car
	 * 
	 * @param maxHeight
	 *            max total volume of load
	 */
	public void setMaxVolume(int maxVolume) {
		this.maxVolume = maxVolume;
	}

	/**
	 * Sets max total weight of load can be stored in car
	 * 
	 * @param maxHeight
	 *            max total weight of load
	 */
	public void setMaxWeight(int maxWeight) {
		this.maxWeight = maxWeight;
	}

	/**
	 * Sets max width of load can be stored in car
	 * 
	 * @param maxHeight
	 *            max width of load
	 */
	public void setMaxWidth(int maxWidth) {
		this.maxWidth = maxWidth;
	}

	/**
	 * Sets car number
	 * 
	 * @param maxHeight
	 *            car number
	 */
	public void setVehicleIdNumber(String vehicleIdNumber) {
		this.vehicleIdNumber = vehicleIdNumber;
	}

	/**
	 * Sets car year
	 * 
	 * @param year
	 *            car year
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * Gets car status
	 * 
	 * @return car status
	 */
	public CarStatus getStatus() {
		return status;
	}

	/**
	 * Sets car status
	 * 
	 * @param status
	 *            car status
	 */
	public void setStatus(CarStatus status) {
		this.status = status;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Car other = (Car) obj;
		if (colour == null) {
			if (other.colour != null)
				return false;
		} else if (!colour.equals(other.colour))
			return false;
		if (makeOfCar == null) {
			if (other.makeOfCar != null)
				return false;
		} else if (!makeOfCar.equals(other.makeOfCar))
			return false;
		if (maxHeight != other.maxHeight)
			return false;
		if (maxLength != other.maxLength)
			return false;
		if (maxVolume != other.maxVolume)
			return false;
		if (maxWeight != other.maxWeight)
			return false;
		if (maxWidth != other.maxWidth)
			return false;
		if (carCondition != other.carCondition)
			return false;
		if (id != other.id)
			return false;
		if (vehicleIdNumber == null) {
			if (other.vehicleIdNumber != null)
				return false;
		} else if (!vehicleIdNumber.equals(other.vehicleIdNumber))
			return false;
		if (year != other.year)
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((colour == null) ? 0 : colour.hashCode());
		result = prime * result + ((makeOfCar == null) ? 0 : makeOfCar.hashCode());
		result = prime * result + maxHeight;
		result = prime * result + maxLength;
		result = prime * result + maxVolume;
		result = prime * result + maxWeight;
		result = prime * result + maxWidth;
		result = prime * result + ((carCondition == null) ? 0 : carCondition.hashCode());
		result = prime * result + id;
		result = prime * result + ((vehicleIdNumber == null) ? 0 : vehicleIdNumber.hashCode());
		result = prime * result + year;
		return result;
	}
}
