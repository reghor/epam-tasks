package by.reghor.logistics.entity;

import java.io.Serializable;

/**
 * Entity class for representing orders
 * 
 * @author Hornet
 *
 */
public class Order implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String arrivalCountry;
	private int arrivalCountryId;
	private String arrivalPoint;
	private int arrivalPointId;
	private String arrivalRegion;
	private int arrivalRegionId;
	private String departureCountry;
	private int departureCountryId;
	private String departurePoint;
	private int departurePointId;
	private String departureRegion;
	private int departureRegionId;
	private String description;
	private int id;
	private int maxLoadHeight;
	private int maxLoadLength;
	private int maxLoadWidth;
	private OrderStatus status;
	private String statusRepr;
	private int volume;
	private int weight;

	/**
	 * Gets arrival country name
	 * 
	 * @return arrival country name
	 */
	public String getArrivalCountry() {
		return arrivalCountry;
	}

	/**
	 * Gets arrival country ID
	 * 
	 * @return arrival country ID
	 */
	public int getArrivalCountryId() {
		return arrivalCountryId;
	}

	/**
	 * Gets arrival city name
	 * 
	 * @return arrival city name
	 */
	public String getArrivalPoint() {
		return arrivalPoint;
	}

	/**
	 * Gets arrival city ID
	 * 
	 * @return arrival city ID
	 */
	public int getArrivalPointId() {
		return arrivalPointId;
	}

	/**
	 * Gets arrival region name
	 * 
	 * @return arrival region name
	 */
	public String getArrivalRegion() {
		return arrivalRegion;
	}

	/**
	 * Gets arrival region id
	 * 
	 * @return arrival region id
	 */
	public int getArrivalRegionId() {
		return arrivalRegionId;
	}

	/**
	 * Gets departure country name
	 * 
	 * @return departure country name
	 */
	public String getDepartureCountry() {
		return departureCountry;
	}

	/**
	 * Gets departure country ID
	 * 
	 * @return departure country ID
	 */
	public int getDepartureCountryId() {
		return departureCountryId;
	}

	/**
	 * Gets departure city name
	 * 
	 * @return departure city name
	 */
	public String getDeparturePoint() {
		return departurePoint;
	}

	/**
	 * Gets departure city ID
	 * 
	 * @return departure city ID
	 */
	public int getDeparturePointId() {
		return departurePointId;
	}

	/**
	 * Gets departure region name
	 * 
	 * @return departure region name
	 */
	public String getDepartureRegion() {
		return departureRegion;
	}

	/**
	 * Gets departure region ID
	 * 
	 * @return departure region ID
	 */
	public int getDepartureRegionId() {
		return departureRegionId;
	}

	/**
	 * Gets order description
	 * 
	 * @return order description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Gets order ID
	 * 
	 * @return order ID
	 */
	public int getId() {
		return id;
	}

	/**
	 * Gets max load height
	 * 
	 * @return max load height
	 */
	public int getMaxLoadHeight() {
		return maxLoadHeight;
	}

	/**
	 * Gets max load length
	 * 
	 * @return max load length
	 */
	public int getMaxLoadLength() {
		return maxLoadLength;
	}

	/**
	 * Gets max load width
	 * 
	 * @return max load width
	 */
	public int getMaxLoadWidth() {
		return maxLoadWidth;
	}

	/**
	 * Gets order status
	 * 
	 * @return order status
	 */
	public OrderStatus getStatus() {
		return status;
	}

	/**
	 * Gets order status String representation
	 * 
	 * @return order status String representation
	 */
	public String getStatusRepr() {
		return statusRepr;
	}

	/**
	 * Gets order load total volume
	 * 
	 * @return order load total volume
	 */
	public int getVolume() {
		return volume;
	}

	/**
	 * Gets order load total weight
	 * 
	 * @return order load total weight
	 */
	public int getWeight() {
		return weight;
	}

	/**
	 * Sets arrival country name
	 * 
	 * @param arrivalCountry
	 *            arrival country name
	 */
	public void setArrivalCountry(String arrivalCountry) {
		this.arrivalCountry = arrivalCountry;
	}

	/**
	 * Sets arrival country ID
	 * 
	 * @param arrivalCountryId
	 *            arrival country ID
	 */
	public void setArrivalCountryId(int arrivalCountryId) {
		this.arrivalCountryId = arrivalCountryId;
	}

	/**
	 * Sets arrival city name
	 * 
	 * @param arrivalPoint
	 *            arrival city name
	 */
	public void setArrivalPoint(String arrivalPoint) {
		this.arrivalPoint = arrivalPoint;
	}

	/**
	 * Sets arrival city ID
	 * 
	 * @param arrivalPointId
	 *            arrival city ID
	 */
	public void setArrivalPointId(int arrivalPointId) {
		this.arrivalPointId = arrivalPointId;
	}

	/**
	 * Sets arrival region name
	 * 
	 * @param arrivalRegion
	 *            arrival region name
	 */
	public void setArrivalRegion(String arrivalRegion) {
		this.arrivalRegion = arrivalRegion;
	}

	/**
	 * Sets arrival region ID
	 * 
	 * @param arrivalRegionId
	 *            arrival region ID
	 */
	public void setArrivalRegionId(int arrivalRegionId) {
		this.arrivalRegionId = arrivalRegionId;
	}

	/**
	 * Sets departure country name
	 * 
	 * @param departureCountry
	 *            departure country name
	 */
	public void setDepartureCountry(String departureCountry) {
		this.departureCountry = departureCountry;
	}

	/**
	 * Sets departure country ID
	 * 
	 * @param departureCountryId
	 *            departure country ID
	 */
	public void setDepartureCountryId(int departureCountryId) {
		this.departureCountryId = departureCountryId;
	}

	/**
	 * Sets departure city name
	 * 
	 * @param departurePoint
	 *            departure city name
	 */
	public void setDeparturePoint(String departurePoint) {
		this.departurePoint = departurePoint;
	}

	/**
	 * Sets departure city ID
	 * 
	 * @param departurePointId
	 *            departure city ID
	 */
	public void setDeparturePointId(int departurePointId) {
		this.departurePointId = departurePointId;
	}

	/**
	 * Sets departure region name
	 * 
	 * @param departureRegion
	 *            departure region name
	 */
	public void setDepartureRegion(String departureRegion) {
		this.departureRegion = departureRegion;
	}

	/**
	 * Sets departure region ID
	 * 
	 * @param departureRegionId
	 *            region ID
	 */
	public void setDepartureRegionId(int departureRegionId) {
		this.departureRegionId = departureRegionId;
	}

	/**
	 * Sets description
	 * 
	 * @param description
	 *            description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Sets order id
	 * 
	 * @param id
	 *            order id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Sets max order load height
	 * 
	 * @param maxLoadHeight
	 *            max load height
	 */
	public void setMaxLoadHeight(int maxLoadHeight) {
		this.maxLoadHeight = maxLoadHeight;
	}

	/**
	 * Sets max order load length
	 * 
	 * @param maxLoadLength
	 */
	public void setMaxLoadLength(int maxLoadLength) {
		this.maxLoadLength = maxLoadLength;
	}

	/**
	 * Sets max order load width
	 * 
	 * @param maxLoadWidth
	 *            max order load width
	 */
	public void setMaxLoadWidth(int maxLoadWidth) {
		this.maxLoadWidth = maxLoadWidth;
	}

	/**
	 * Sets order status
	 * 
	 * @param status
	 *            order status
	 */
	public void setStatus(OrderStatus status) {
		this.status = status;
		statusRepr = status.getLowerCaseValue();
	}

	/**
	 * Sets String order status representation
	 * 
	 * @param statusRepr
	 *            String order status representation
	 */
	public void setStatusRepr(String statusRepr) {
		this.statusRepr = statusRepr;
	}

	/**
	 * Sets order load volume
	 * 
	 * @param volume
	 *            order load volume
	 */
	public void setVolume(int volume) {
		this.volume = volume;
	}

	/**
	 * Sets order load weight
	 * 
	 * @param weight
	 *            order load weight
	 */
	public void setWeight(int weight) {
		this.weight = weight;
	}

}
