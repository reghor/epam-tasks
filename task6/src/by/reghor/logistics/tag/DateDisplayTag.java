package by.reghor.logistics.tag;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class DateDisplayTag extends TagSupport {

	private static final long serialVersionUID = 1L;
	private static final String DATE_FORMAT="dd.MM.yy ";

	@Override
	public int doStartTag() throws JspTagException {
		try {
			Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat(DATE_FORMAT);
			String date = "<h1 class='header-of-fields'>" + ft.format(dNow) + "</h1>";
			JspWriter out = pageContext.getOut();
			out.write(date);
		} catch (IOException e) {
			throw new JspTagException(e);
		}
		return SKIP_BODY;
	}
	
	@Override
	public int doEndTag() throws JspTagException {

		return EVAL_PAGE;
	}

}
