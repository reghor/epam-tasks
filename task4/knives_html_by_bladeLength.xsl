<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:k="http://www.epam.by.reghor/knives">
	<xsl:output method="html"/>
	<xsl:output omit-xml-declaration="yes" encoding="UTF-8"/>
	<xsl:template match="/k:Knife">
		<html>
			<HEAD>
				<TITLE>
					Холодное оружие по длине клинка
				</TITLE>
			</HEAD>
			<body>
				<h2>Холодное оружие сортированное по длине клинка</h2>
				<table border="1">
					<tr bgcolor="#9acd32">
						<th>ID</th>
						<th>Тип</th>
						<th bgcolor="#FF0000">Длина клинка, см</th>
					</tr>
					<xsl:for-each select="k:knife">
						 <xsl:sort select="k:Visual/k:bladeDimensions/k:bladeLength"/>
						<tr>
							<td>
								<xsl:value-of select="@ID" />
							</td>
							<td>
								<xsl:value-of select="k:Type" />
							</td>
							<td>
								<xsl:value-of select="k:Visual/k:bladeDimensions/k:bladeLength" />
							</td>
						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>